
import {

  Routes,
  Route,


  HashRouter,
} from "react-router-dom";
// import "./App.css"

import SearchAppBar from "./new_components/Search_field";

import LoginPage from "./new_components/LoginPage";
import LogsComponent from "./new_components/LogsComponent";

import SettingsPage from "./new_components/SettingsPage";
import UserSettings from "./new_components/Dash";
import DepartmentSettings from "./new_components/DepartmentSettings";

import Nurses from "./new_components/Pages/Nurses";

//import Management from "./new_components/Pages/Management";
import Analytics from "./new_components/Pages/Analytics";
import IntermidietPage from "./new_components/Intermediate/IntermidietPage";
import BasicPage from "./new_components/Basic/BasicPage";

import { useState } from "react";
import Users from "./new_components/Pages/Users";
import ApiList from "./new_components/Pages/Management";
import NursesInter from "./new_components/Intermediate/NursesInter";
import Tickets from "./new_components/Pages/Tickets";
import { ThemeProvider, createTheme } from "@mui/material";
import { AppProvider } from "./new_components/Pages/MyContext";

function App() {

  const theme = createTheme({
    typography: {
      fontFamily: ["Play"],
    },
  });
  return (
    <>
      <ThemeProvider theme={theme}>
        <HashRouter>
          {/* <ResponsiveAppBar /> */}
          {/* <NavigationBar/> */}
          <AppProvider>
            <Routes>
              {/* <Route path="/" element={<Sidenav />} /> */}
              <Route index element={<LoginPage />} />
              {/* <Route path="home" element={<SearchAppBar />} /> */}

              <Route path="home" element={<SearchAppBar />} />

              <Route path="inter" element={<IntermidietPage />} />
              <Route path="basic" element={<BasicPage />} />
              <Route path="logs" element={<LogsComponent />} />
              <Route path="settings" element={<SettingsPage />} />
              <Route path="dash" element={<UserSettings />} />
              <Route path="tickets" element={<Tickets />} />

              <Route path="department" element={<DepartmentSettings />} />
              <Route path="nurses" element={<Nurses />} />
              <Route path="management" element={<ApiList />} />
              <Route path="analytics" element={<Analytics />} />
              <Route path="userspage" element={<Users />} />
              <Route path="internurse" element={<NursesInter />} />
            </Routes>
          </AppProvider>
        </HashRouter>
      </ThemeProvider>
    </>
  );
}
export default App;
