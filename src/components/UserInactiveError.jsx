import React from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

const UserInactiveError = ({ open, onClose, autoHideDuration }) => {
  return (
    <Snackbar
      open={open}
      autoHideDuration={autoHideDuration}
      onClose={onClose}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
    >
      <Alert onClose={onClose} severity="error" sx={{ width: "100%" }}>
  Current user INACTIVE, Contact Admin. 
      </Alert>
    </Snackbar>
  );
};

export default UserInactiveError;
