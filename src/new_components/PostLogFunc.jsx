// api.js

import axios from 'axios';

export const postLogs = async (user_name,role, activity) => {
 // console.log("data to be sent", data);

  const logObject = {
    username: user_name,
    activity: activity,
    user_role: role
  };

  try {
    const response = await axios.post(
       "https://nurse-backend.onrender.com/create-log",
     // "http://localhost:8080/create-log",

      logObject
    );

    // Handle the response from the backend if needed
    console.log("Response from the backend:", response.data);
    // setOpenSuccessAlert(true);
  } catch (error) {
    // Handle any errors that occur during the request
    console.error("Error sending data to the backend:", error);
  }
  // alert("nurse saved successfully...");

  // handleCloseAddNurse();
};
