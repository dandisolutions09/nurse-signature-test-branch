import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import {
  AppBar,
  Box,
  Button,
  Modal,
  TextField,
  TextareaAutosize,
  Toolbar,
  Typography,
} from "@mui/material";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import * as Yup from "yup";
import NavigationBar from "../NavigationBar";
import { Textarea } from "@mui/joy";
import Ticketsuccess from "../../components/TicketsAlerts/Ticketsuccess";
import { Formik, Form, useFormik } from "formik";
import { values } from "lodash";
import axios from "axios";
import { Dropdown } from "antd";

const validationSchema = Yup.object({
  issue: Yup.string().required("Input subject of the issue"),
  description: Yup.string().required("description is Required"),
});

const columns = [
  { id: "created_at", label: "TIME AND DATE SENT", minWidth: 100 },
  // { id: "username", label: "USER", minWidth: 100 },
  { id: "issue", label: "ISSUE", minWidth: 100 },
  { id: "description", label: "Description", minWidth: 100 },
  { id: "status", label: "STATUS", minWidth: 100 },
];

export default function Tickets() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
   const [selectedRowData, setSelectedRowData] = React.useState("");
  

  const [incomingLogs, setIncomingLogs] = React.useState([]);
  const items = [
    {
      key: "1",
      label: "View Ticket ",
      // icon: <EditIcon sx={{ fontSize: 32 }} />,
      onClick: () => handle_open_Viewticket(selectedRowData),
    },

    {
      key: "3",
      label: "Resolved",
      // icon: <VisibilityOff />,
      // onClick: () => handleOpenEdit(selectedRowData),
    },
  ];

  function handleGetAllTickets() {
    console.log("handle get all nurses called!");
    fetch("https://nurse-backend.onrender.com/tickets")
      .then((response) => response.json())
      .then((json) => {
        console.log("rec logs", json);
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

        setRows(json);
      });
  }

  const postData = async (data) => {
    console.log("data to be sent", data);
    try {
      const response = await axios.post(
        "https://nurse-backend.onrender.com/add-ticket",
        data
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    }
    // alert("nurse saved successfully...");

    // navigate("/");
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  React.useEffect(() => {
    // This code will run when the component mounts
    //setSearchInput(""); // Set searchInput to an empty string
    //console.log("nurseData", nurseData);
    // handleGetAllNurses();

    handleGetAllTickets();
  }, []);

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const [openAddTicket, setOpenAddTicket] = React.useState(false);
   const [openViewTicket, setOpenViewTicket] = React.useState(false);
  const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);

 const handle_open_Viewticket = () => {
   setOpenViewTicket(true);
 };
  const handle_close_Viewticket = () => {
    setOpenViewTicket(false);
  };

  const handle_open_ticket = () => {
    setOpenAddTicket(true);
  };
  const handle_close_ticket = () => {
    setOpenAddTicket(false);
  };

  const handleSubmit = (inc_val) => {
    console.log("incoming ", inc_val);
    postData(inc_val);
    setOpenAddTicket(false);

    setOpenSuccessAlert(true);
  };

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 6,
    p: 4,
    borderRadius: 2,
  };

  const formik = useFormik({
    initialValues: {
      issue: "",
      description: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log(" add form data:-", values);
      handleSubmit(values);
      handle_close_ticket();
    },
  });

  return (
    <>
      <Ticketsuccess
        open={openSuccessAlert}
        autoHideDuration={3000}
        onClose={() => setOpenSuccessAlert(false)}
      />
      <Modal
        open={openViewTicket}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4 m-4">
              <Typography>
                <p style={text}>{rows.ticket_number} Ticket Details</p>
              </Typography>
              <TextField
                id="issue"
                name="issue"
                label="Subject Issue:"
                placeholder="Issue"
                maxRows={4}
                variant="standard"
                value={formik.values.issue}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.issue && Boolean(formik.errors.issue)}
                helperText={formik.touched.issue && formik.errors.issue}
              />
              <Textarea
                id="description"
                name="description"
                label="Description:"
                placeholder="description..."
                minRows={3}
                value={formik.values.description}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.description &&
                  Boolean(formik.errors.description)
                }
                helperText={
                  formik.touched.description && formik.errors.description
                }
              />
            </div>

            <div className="flex m-2 gap-4">
              <Button variant="contained" color="success" type="submit">
                Submit
              </Button>
              <Button
                onClick={handle_close_Viewticket}
                variant="contained"
                color="error"
              >
                cancel
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openAddTicket}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4 m-4">
              <Typography>
                <p style={text}> New Ticket</p>
              </Typography>
              <TextField
                id="issue"
                name="issue"
                label="Subject Issue:"
                placeholder="Issue"
                maxRows={4}
                variant="standard"
                value={formik.values.issue}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.issue && Boolean(formik.errors.issue)}
                helperText={formik.touched.issue && formik.errors.issue}
              />
              <Textarea
                id="description"
                name="description"
                label="Description:"
                placeholder="description..."
                minRows={3}
                value={formik.values.description}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.description &&
                  Boolean(formik.errors.description)
                }
                helperText={
                  formik.touched.description && formik.errors.description
                }
              />
            </div>

            <div className="flex m-2 gap-4">
              <Button variant="contained" color="success" type="submit">
                Submit
              </Button>
              <Button
                onClick={handle_close_ticket}
                variant="contained"
                color="error"
              >
                cancel
              </Button>
            </div>
          </form>
        </Box>
      </Modal>

      <NavigationBar />
      <Box className="flex  justify-between">
        <Typography
          sx={{
            fontSize: "26px",
            margin: "20px",

            color: "rgb(31 41 55)",
          }}
        >
          Ticket
        </Typography>
        <Box className="m-3">
          <Button
            variant="contained"
            color="success"
            onClick={handle_open_ticket}
          >
            Add Ticket
          </Button>
        </Box>
      </Box>

      <Paper
        sx={{
          width: "100%",
          overflow: "hidden",
          padding: "30px",
          margin: "20px",
        }}
      >
        <TableContainer sx={{ maxHeight: "100%" }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                    sx={{ fontWeight: "600", fontSize: "16px" }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <Dropdown
                      key={row._id}
                      menu={{ items }}
                      trigger={["contextMenu"]}
                      placement="bottomLeft"
                    >
                      <TableRow
                        role="checkbox"
                        tabIndex={-1}
                        key={row._id}
                        sx={{
                          "&:hover": { backgroundColor: "rgb(212 212 212)" },
                          cursor: "pointer",
                        }}
                        onMouseEnter={() => {
                          setSelectedRowData(row);
                        }}
                      >
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {column.format && typeof value === "number"
                                ? column.format(value)
                                : value}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    </Dropdown>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
const btn = {
  backgroundColor: "red",
  color: "white",
  borderRadius: "4px",
};
const text = {
  fontWeight: "bold",
  fontSize: "18px",
};
