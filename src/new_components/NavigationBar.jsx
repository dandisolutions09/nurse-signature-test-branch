import {
  AppBar,
  Badge,
  Box,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Modal,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import React from "react";
// import Tooltip, { TooltipProps, tooltipClasses } from "@mui/material/Tooltip";
// import { styled, alpha } from "@mui/material/styles";
import ListIcon from "@mui/icons-material/List";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import NotificationsIcon from "@mui/icons-material/Notifications";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Sidenav from "./Sidenav";
import AccoutToggle from "../components/AccoutToggle";
import MailIcon from "@mui/icons-material/Mail";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MoreIcon from "@mui/icons-material/MoreVert";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import axios from "axios";
import { postLogs } from "./PostLogFunc";
import * as Yup from "yup";
import { useFormik } from "formik";
import MultipleSelectCheckmarks from "./MultiSelect";
import ReactSignatureCanvas, { SignatureCanvas } from "react-signature-canvas";
import Analytics from "./Pages/Analytics";
import LightTooltip from "../components/LightTool";
import { AppContext } from "./Pages/MyContext";
import Notification from "../components/notification/Notification";

const validationSchema = Yup.object({
  name: Yup.string().required("name is required"),
  department: Yup.string().required("department is required"),
});

export default function NavigationBar() {
  const [isSideNavOpen, setIsSideNavOpen] = React.useState(false);
  const [openNotification, setOpenNotification] = React.useState(false);
  const navigate = useNavigate();

  const handle_notification = () =>{
    setOpenNotification(!openNotification);
  }
  const handleToggleSideNav = () => {
    console.log("opened");
    setIsSideNavOpen(!isSideNavOpen);
  };
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const [openAccountInfo, setOpenAccountInfo] = React.useState(false);

  //const { userData } = React.useContext(AppContext);
  const storeduserData = localStorage.getItem("userName_storage");
  const userData = JSON.parse(storeduserData);


  //console.log("context user name:->", userData.username);

  const handleNavigation_analy = () =>{
    navigate("/analytics");
  }

  const handleOpenAccountInfo = () => {
    setOpenAccountInfo(true);
  };
  const handleCloseAccountInfo = () => {
    setOpenAccountInfo(false);
  };

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
      {isSideNavOpen && <AccoutToggle />}
    </Menu>
  );
  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton size="large" aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="error">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem>
        <IconButton
          size="large"
          aria-label="show 17 new notifications"
          color="inherit"
        >
          <Badge badgeContent={17} color="error">
            <NotificationsIcon onClick={handle_notification} />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
      {isSideNavOpen && <AccoutToggle />}
    </Menu>
  );
  const location = useLocation();
  const incoming_usr_data = location.state;

  const sigCanvasRef = React.useRef(null);
  const clearSignature = () => {
    sigCanvasRef.current.clear();
  };

  const signupInformationRef = React.useRef({
    name: "",
    // last_name: "",
    position: "",
    // email: "",
    // disabled: false,
    department: "",
    // role: "Admin",
    // hashed_pwd: "",
    wards: [],
    signatureImageData: "",
  });

  const [personName, setPersonName] = React.useState([]);

  const handlePersonNameChange = (newPersonName) => {
    setPersonName(newPersonName);
  };

  const postData = async (data) => {
    console.log("data to be sent", data);
    try {
      const response = await axios.post(
        // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
        "https://nurse-backend.onrender.com/add-nurse",
        // "https://nurse-backend.onrender.com/create",
        data
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
      // postLogs(
      //   incoming_username_data,
      //   incoming_usr_data.usr_role,
      //   `ADDED NURSE:- ${data.name}`
      // );

      // postLogs(data.name, )
      // setOpenSuccessAlert(true);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    }
    // alert("nurse saved successfully...");

    handleCloseAddNurse();
  };
  const formik = useFormik({
    initialValues: {
      name: "",
      department: "",
      position: "",
      wards: [],
      signatureImageData: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("form data:-", values);

      const signatureImageURL = sigCanvasRef.current.toDataURL();
      signupInformationRef.current.name = values.name;
      signupInformationRef.current.position =
        values.position !== undefined ? values.position : "empty";
      signupInformationRef.current.department =
        values.department !== undefined ? values.department : "empty";
      signupInformationRef.current.wards = personName; // Add selected wards to signupInformationRef
      signupInformationRef.current.signatureImageData = signatureImageURL;
      console.log("sign up info:-", signupInformationRef.current);

      postData(signupInformationRef.current);

      // setAuthAdd(false);

      // setTimeout(postData(signupInformationRef.current), 3000)

      // sendMessage("facility_data", values);
      // navigate("/facilities");
    },
  });

  const departments = [
    {
      value: "North",
      label: "North",
    },
    {
      value: "West",
      label: "West",
    },
  ];
  const [positions, setPositions] = React.useState([
    {
      value: "Chief",
      label: "Chief",
    },
    {
      value: "AsstChief",
      label: "AsstChief",
    },

    {
      value: "ASST",
      label: "ASST",
    },
  ]);
  const [openaddNurseModal, setAddNurse] = React.useState(false);
  const [openAnalytics, setOpenAnalytics] = React.useState(false);
  function handleOpenAddNurse() {
    setAddNurse(true);
  }
  function handleCloseAddNurse() {
    setAddNurse(false);
  }
  const handleNavigate_management = () => {
    navigate("/management");
  };
  const handleNavigate_logs = () => {
    navigate("/logs");
  };
  const handleNavigate_nurses = () => {
    navigate("/nurses");
  };
  const handleOpenAnalytics = () => {
    setOpenAnalytics(true);
  };
  const handleCloseAnalytics = () => {
    setOpenAnalytics(false);
  };

  const handleNavigate_home = () => {
    navigate("/home");
  };
  // const LightTooltip = styled(({ className, ...props }) => (
  //   <Tooltip {...props} classes={{ popper: className }} />
  // ))(({ theme }) => ({
  //   [`& .${tooltipClasses.tooltip}`]: {
  //     backgroundColor: theme.palette.common.white,
  //     color: "rgba(0, 0, 0, 0.87)",
  //     boxShadow: theme.shadows[1],
  //     fontSize: 11,
  //   },
  // }));

  return (
    <>
      <Modal open={openAnalytics} onClose={handleCloseAnalytics}>
        <Box sx={style}>
          <Analytics />
        </Box>
      </Modal>
      <Modal
        open={openaddNurseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add New Nurse
          </Typography>

          <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4 m-4">
              <div className="flex flex-row gap-3">
                <TextField
                  id="name"
                  name="name"
                  label="Name:"
                  placeholder="Name"
                  maxRows={4}
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.name && Boolean(formik.errors.name)}
                  helperText={formik.touched.name && formik.errors.name}
                />

                <TextField
                  id="department"
                  select
                  name="department"
                  label="Department"
                  defaultValue="Chief"
                  helperText="Please select Department"
                  value={formik.values.department}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  {departments.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </div>
              <div className="flex flex-row justify-between">
                <TextField
                  id="position"
                  select
                  name="position"
                  label="Position"
                  // defaultValue="Chief"
                  helperText="Please select Department"
                  value={formik.values.position}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  {positions.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                <MultipleSelectCheckmarks
                  personName={personName}
                  onPersonNameChange={handlePersonNameChange}
                />{" "}
                {personName.length}
              </div>

              <div className="border border-gray-600 rounded-md">
                <ReactSignatureCanvas
                  ref={sigCanvasRef}
                  penColor="blue"
                  canvasProps={{
                    className: "w-full h-auto",
                  }}
                />
              </div>
            </div>

            <div>
              <Button onClick={clearSignature} variant="outlined" color="error">
                Clear Signature
              </Button>
            </div>

            <div className="flex m-2 gap-4">
              <Button variant="contained" color="success" type="submit">
                Submit
              </Button>
              <Button
                onClick={handleCloseAddNurse}
                variant="contained"
                color="error"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <LightTooltip title="Home">
              <div className="mr-6">
                <IconButton
                  size="small"
                  edge="end"
                  aria-label="account of current user"
                  aria-haspopup="true"
                  onClick={handleNavigate_home}
                  color="inherit"
                >
                  <HomeOutlinedIcon sx={{ fontSize: 30 }} />
                </IconButton>
              </div>
            </LightTooltip>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{
                flexGrow: 1,
                fontWeight: "bold",
                fontSize: { xs: "16px", sm: "18px", md: "20px" },
                cursor: "pointer", // Customize font sizes based on your needs
              }}
              onClick={handleNavigate_home}
            >
              Nurses CD Signature System
            </Typography>
            <Box sx={{ flexGrow: 1 }} />
            <Box sx={{ flexGrow: 1 }} />
            <Box className="flex " sx={{ marginLeft: "90px" }}>
              <Box className="flex flex-row" sx={{ gap: "30px" }}>
                <LightTooltip title="All Nurses">
                  <Typography
                    sx={{
                      transition: "border-bottom-width 0.3s ease",
                      "&:hover": {
                        borderBottomWidth: "2px",
                      },
                      cursor: "pointer",
                    }}
                    onClick={handleNavigate_nurses}
                  >
                    Nurses
                  </Typography>
                </LightTooltip>
                <LightTooltip title="Analytics" placement="bottom">
                  <Typography
                    sx={{
                      transition: "border-bottom-width 0.3s ease",
                      "&:hover": {
                        borderBottomWidth: "2px",
                      },
                      cursor: "pointer",
                    }}
                    onClick={handleNavigation_analy}
                  >
                    Analytics
                  </Typography>
                </LightTooltip>
                <LightTooltip title="Users Logs" placement="bottom">
                  <Typography
                    sx={{
                      transition: "border-bottom-width 0.3s ease",
                      "&:hover": {
                        borderBottomWidth: "2px",
                      },
                      cursor: "pointer",
                    }}
                    onClick={handleNavigate_logs}
                  >
                    Logs
                  </Typography>
                </LightTooltip>
              </Box>
            </Box>

            <Box sx={{ flexGrow: 1 }} />

            <Box sx={{ display: { xs: "none", md: "flex" } }}>
              {/* <Button
                variant="contained"
                color="success"
                onClick={handleOpenAddNurse}
              >
                New Nurse +
              </Button> */}
              <LightTooltip title="view notifications">
                <IconButton
                  size="large"
                  aria-label="show 4 new mails"
                  color="inherit"
                >
                  <Badge badgeContent={14} color="error">
                    <NotificationsIcon onClick={handle_notification} />
                  </Badge>
                </IconButton>
              </LightTooltip>

              {openNotification && <Notification />}
              <LightTooltip title="Account" placement="bottom">
                <div
                  className="cursor-pointer mt-2 ml-8 flex flex-row text-center gap-4 py-4"
                  onClick={handleToggleSideNav}
                >
                  <AccountCircleIcon sx={{ fontSize: 35 }} />
                  {/* <AccountCircleIcon /> {incoming_username_data} */}
                  <div className="flex flex-row mt-2 gap-6">
                    <p>{userData.username}</p>
                    <SettingsOutlinedIcon />
                  </div>
                </div>
              </LightTooltip>
              {isSideNavOpen && <AccoutToggle />}
            </Box>

            <Box sx={{ display: { xs: "flex", md: "none" } }}>
              <IconButton onClick={handleToggleSideNav}>
                <LightTooltip title="more" placement="bottom">
                  <MoreIcon onClick={handleToggleSideNav} />
                </LightTooltip>
              </IconButton>
              {isSideNavOpen && <AccoutToggle />}
            </Box>
          </Toolbar>
        </AppBar>
        {renderMobileMenu}
        {renderMenu}
      </Box>
    </>
  );
}
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
  border: "none",
  outline: "none",
};
