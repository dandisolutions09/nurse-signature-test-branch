import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { AppBar, Box, Button, LinearProgress, Toolbar, Typography } from "@mui/material";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import IconButton from "@mui/material/IconButton";
import NavigationBar from "./NavigationBar";
import { AppContext } from "./Pages/MyContext";
import DownloadIcon from "@mui/icons-material/Download";
import LightTooltip from "../components/LightTool";

const columns = [
  { id: "created_at", label: "TIMESTAMP (LONDON TIME)", minWidth: 100 },
  { id: "username", label: "USER", minWidth: 100 },
  { id: "user_role", label: "ROLE", minWidth: 100 },
  { id: "activity", label: "ACTIVITY", minWidth: 100 },
];

function createData(name, code, population, size) {
  const density = population / size;
  return { name, code, population, size, density };
}

export default function LogsComponent() {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);

  const [incomingLogs, setIncomingLogs] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  // const { userData } = React.useContext(AppContext);

  // console.log("context user name:->", userData.username);

  function handleGetAllLogs() {
    setLoading(true);
    console.log("handle get all nurses called!");
    fetch("https://nurse-backend.onrender.com/get-logs")
      .then((response) => response.json())
      .then((json) => {
        console.log("rec logs", json);
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

        setRows(json);

        //   console.log("incoming", incoming_nursesData);
        // console.log("filtere:->", filterNursesByWard(json, "Ward A"));
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  React.useEffect(() => {
    // This code will run when the component mounts
    //setSearchInput(""); // Set searchInput to an empty string
    //console.log("nurseData", nurseData);
    // handleGetAllNurses();

    handleGetAllLogs();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <>
      <NavigationBar />
      <div>
        {loading && <LinearProgress />}

        {/* Your existing component content */}
      </div>
      {/* <Typography
        sx={{
          fontSize: "26px",
          margin: "20px",
        
          color: "rgb(31 41 55)",
        }}
      >
        Logs
      </Typography> */}
      <Box className="flex  justify-between">
        <Typography
          sx={{
            fontSize: "26px",
            margin: "20px",

            color: "rgb(31 41 55)",
          }}
        >
          Logs
        </Typography>

        <Box className="m-3">
          <LightTooltip title="Download csv of the logs">
            <IconButton

            // onClick={handle_open_ticket}
            >
              <DownloadIcon color="primary" fontSize="large" />
            </IconButton>
          </LightTooltip>
        </Box>
      </Box>

      <Paper
        sx={{
          width: "100%",
          overflow: "hidden",
          padding: "30px",
          margin: "20px",
        }}
      >
        <TableContainer sx={{ maxHeight: "100%" }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                    sx={{ fontWeight: "600", fontSize: "16px" }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      role="checkbox"
                      tabIndex={-1}
                      key={row._id}
                      sx={{
                        "&:hover": { backgroundColor: "rgb(212 212 212)" },
                        cursor: "pointer",
                      }}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
