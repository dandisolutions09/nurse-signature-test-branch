import React, { useEffect, useState } from "react";
import axios from "axios";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { AppBar, Box, Button, LinearProgress, Toolbar, Typography } from "@mui/material";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import IconButton from "@mui/material/IconButton";
import NavigationBar from "../NavigationBar";
import { Dropdown } from "antd";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import AccountCircle from "@mui/icons-material/AccountCircle";
import InterNavbar from "./InterNavbar";


export default function NursesInter() {
  const [data, setData] = useState(null);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);

  const storeduserData = localStorage.getItem("userName_storage");
  const userData = JSON.parse(storeduserData);


  //console.log("context user name:->", userData.username);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const items = [
    {
      key: "1",
      label: "View Nuser",
      icon: <AccountCircle sx={{ fontSize: 32 }} />,
      // onClick: () => handleOpenViewUser(selectedRowData),
    },

    {
      key: "2",
      label: "Remove Nurse",
      icon: <DeleteForeverIcon />,
      // onClick: () => handleOpenDelete(selectedRowData),
    },
    // {
    //   key: "3",
    //   label: "Reset User Password",
    //   icon: <DeleteForeverIcon />,
    //   // onClick: () => handleOpenEdit(selectedRowData),
    // },
  ];
  const columns = [
    { id: "name", label: "NURSE NAME", minWidth: 100 },
    { id: "department", label: "DEPT", minWidth: 100 },
    { id: "position", label: "POSITION", minWidth: 100 },
    { id: "wards", label: "WARDS", minWidth: 100 },
    { id: "duration_days", label: "Duration(days)", minWidth: 100 },
    { id: "duration", label: "Duration(hrs)", minWidth: 100 },
    // { id: "color", label: "color", minWidth: 100 },
    { id: "created_at", label: "CREATED", minWidth: 100 },
  ];
    const [loading, setLoading] = React.useState(false);

    const fetchData = async () => {
      setLoading(true);
      try {
        const response = await axios.get(
          "https://nurse-backend.onrender.com/get-nurses"
        );
        const json = response.data;

        const now = new Date();

        json.forEach((item) => {
          const durationMs = now - new Date(item.created_at);
          const durationMinutes = durationMs / (1000 * 60);
          const durationDays = durationMinutes / (24 * 60);
          const durationHours = durationMs / (1000 * 60 * 60);

          item.duration = durationHours;
          item.duration_days = durationDays;

          const differenceTo60 = Math.abs(durationDays - 60);

          if (durationDays > 60) {
            item.color = "red";
            // update nurse information to inactive
          } else if (differenceTo60 < 10) {
            item.color = "yellow"; // Amber
          } else {
            item.color = "green";
          }
        });

        if (json && Array.isArray(json) && json.length > 0) {
          json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
          setRows(json);
        } else {
          console.warn("Received null, undefined, or empty response.");
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      } finally {
        setLoading(false); // Set loading to false after the request is complete (success or error)
      }
    };
  const [selectedRowData, setSelectedRowData] = React.useState("");

  useEffect(() => {
    fetchData();
    
  }, []);

  return (
    <>
      <InterNavbar />
      <div>{loading && <LinearProgress />}</div>
      <Typography
        sx={{
          fontSize: "26px",
          margin: "20px",

          color: "rgb(31 41 55)",
        }}
      >
        Nurses
      </Typography>

      <Paper
        sx={{
          width: "100%",
          overflow: "hidden",
          padding: "30px",
          margin: "20px",
        }}
      >
        <TableContainer sx={{ maxHeight: "100%" }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                    sx={{ fontWeight: "600", fontSize: "16px" }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <Dropdown
                      key={row._id}
                      menu={{ items }}
                      trigger={["contextMenu"]}
                      placement="bottomLeft"
                    >
                      <TableRow
                        role="checkbox"
                        tabIndex={-1}
                        key={row._id}
                        sx={{
                          transition: "background-color 0.4s ease",
                          "&:hover": { backgroundColor: "rgb(101 163 13)" },
                          cursor: "pointer",
                          backgroundColor: row.color,
                        }}
                        onMouseEnter={() => {
                          setSelectedRowData(row);
                        }}
                      >
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {column.format && typeof value === "number"
                                ? column.format(value)
                                : value}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    </Dropdown>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
