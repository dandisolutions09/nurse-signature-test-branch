import SettingsNav from "./SettingsNav";
import TestingTable from "./TestingTable";
import DepartmentTable from "./DepartmentTable";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import { useLocation, useNavigate } from "react-router-dom";


const Dash = () => {
   const navigate = useNavigate();
    const handleNavigate1 = () => {
      navigate("/home");
    };
     const style = {
       position: "absolute",
       top: "50%",
       left: "50%",
       transform: "translate(-50%, -50%)",
       bgcolor: "background.paper",
       boxShadow: 6,
       p: 4,
       borderRadius: 2,
     };
  return (
    <>
      <div style={header}>
        <div>
          <SettingsNav />
        </div>

        <div>
          <div className="flex flex-row gap-2 p-2">
            <p className="cursor-pointer text-[#2196f3] underline decoration-sky-600 hover:decoration-blue-400">
              Department Management
            </p>
            {"/"}
            <p className="cursor-pointer text-[#2196f3]">User Management</p>
          </div>
          <TestingTable />
        </div>
      </div>
    </>
  );
};
const header = {
  display: "flex",
  flexDirection: "row",

};



export default Dash;
