// import * as React from "react";
// import { styled, alpha } from "@mui/material/styles";
// import AppBar from "@mui/material/AppBar";
// import Box from "@mui/material/Box";
// import Toolbar from "@mui/material/Toolbar";
// import Menu from "@mui/material/Menu";
// import Typography from "@mui/material/Typography";
// import InputBase from "@mui/material/InputBase";
// import MenuIcon from "@mui/icons-material/Menu";
// import SearchIcon from "@mui/icons-material/Search";
// import CloseIcon from "@mui/icons-material/Close";
// import * as Yup from "yup";
// import MoreIcon from "@mui/icons-material/MoreVert";

// import { useFormik } from "formik";
// // import Select from "@mui/material/Select";
// import health_logo from "../../assets/logo4.png";
// import LOGO from "../../assets/logo4.png";
// import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
// import LOGO2 from "../../assets/logo56.png";
// import MenuItem from "@mui/material/MenuItem";
// import AccountCircleIcon from "@mui/icons-material/AccountCircle";
// import FormControl from "@mui/material/FormControl";
// import InputLabel from "@mui/material/InputLabel";
// import Badge from "@mui/material/Badge";
// import IconButton from "@mui/material/IconButton";
// import AccountCircle from "@mui/icons-material/AccountCircle";
// import MailIcon from "@mui/icons-material/Mail";
// import NotificationsIcon from "@mui/icons-material/Notifications";
// import ListIcon from "@mui/icons-material/List";
// import Tooltip, { tooltipClasses } from "@mui/material/Tooltip";
// import LoginIcon from "@mui/icons-material/Login";
// // const _ = require("lodash");
// import _ from "lodash";
// import {
//   Alert,
//   AlertTitle,
//   Button,
//   ButtonBase,
//   Checkbox,
//   FormControlLabel,
//   FormGroup,
//   FormHelperText,
//   FormLabel,
//   LinearProgress,
//   ListItemText,
//   Modal,
//   OutlinedInput,
//   Paper,
//   Radio,
//   RadioGroup,
//   Snackbar,
// } from "@mui/material";
// import TextField from "@mui/material/TextField";
// import useGetNurseByName from "../../hooks/SearchNurseHook";
// import ReactSignatureCanvas, { SignatureCanvas } from "react-signature-canvas";
// import axios from "axios";
// import { NavLink, useLocation, useNavigate } from "react-router-dom";
// import { Margin, Refresh } from "@mui/icons-material";
// import { SearchBar } from "../SearchBar";
// import { SearchResultsList } from "../SearchResultList";
// // import constructWithOptions from "styled-components/dist/constructors/constructWithOptions";
// import { IoCloseSharp } from "react-icons/io5";
// import ErrorSnackbar from "../ErrorSnackbar";
// import SuccessSnackbar from "../SuccessSnackbar";
// import { m } from "framer-motion";
// import ErrorSnackbar_SearchType from "../ErrorSearchTypeSnackBar";
// import { postLogs } from "../PostLogFunc";
// import Sidenav from "../Sidenav";
// import SettingsIcon from "@mui/icons-material/Settings";
// import MultipleSelectCheckmarks from "../MultiSelect";
// import NavigationBar from "../NavigationBar";
// import AccoutToggle from "../../components/AccoutToggle";
// import { AppContext } from "../Pages/MyContext";

// import BasicNavbar from "./BasicNavbar";
// //import  MyContext  from "./Pages/MyContext";

// // import { Formik, Form } from "formik";

// const validationSchema = Yup.object({
//   name: Yup.string().required("Name is Required"),
//   department: Yup.string().required("department is required"),
//   position: Yup.string().required("Position is Required"),
//   wards: Yup.string().required("Wards is Required"),
// });

// const Search = styled("div")(({ theme }) => ({
//   position: "relative",
//   borderRadius: theme.shape.borderRadius,
//   backgroundColor: alpha(theme.palette.common.white, 0.15),
//   "&:hover": {
//     backgroundColor: alpha(theme.palette.common.white, 0.25),
//   },
//   marginLeft: 0,
//   width: "100%",
//   [theme.breakpoints.up("sm")]: {
//     marginLeft: theme.spacing(1),
//     width: "auto",
//   },
// }));

// const SearchIconWrapper = styled("div")(({ theme }) => ({
//   padding: theme.spacing(0, 2),
//   height: "100%",
//   position: "absolute",
//   pointerEvents: "none",
//   display: "flex",
//   alignItems: "center",
//   justifyContent: "center",
// }));

// const StyledInputBase = styled(InputBase)(({ theme }) => ({
//   color: "inherit",
//   "& .MuiInputBase-input": {
//     padding: theme.spacing(1, 1, 1, 0),
//     // vertical padding + font size from searchIcon
//     paddingLeft: `calc(1em + ${theme.spacing(4)})`,
//     transition: theme.transitions.create("width"),
//     width: "100%",
//     [theme.breakpoints.up("sm")]: {
//       width: "12ch",
//       "&:focus": {
//         width: "20ch",
//       },
//     },
//   },
// }));

// const options = [
//   { value: "chocolate", label: "Chocolate" },
//   { value: "strawberry", label: "Strawberry" },
//   { value: "vanilla", label: "Vanilla" },
// ];

// const ITEM_HEIGHT = 48;
// const ITEM_PADDING_TOP = 8;
// const MenuProps = {
//   PaperProps: {
//     style: {
//       maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
//       width: 250,
//     },
//   },
// };

// const names = [
//   "Oliver Hansen",
//   "Van Henry",
//   "April Tucker",
//   "Ralph Hubbard",
//   "Omar Alexander",
//   "Carlos Abbott",
//   "Miriam Wagner",
//   "Bradley Wilkerson",
//   "Virginia Andrews",
//   "Kelly Snyder",
// ];

// const style_Paper = {
//   position: "absolute",
//   top: "18%",
//   left: "94%",
//   transform: "translate(-50%, -50%)",
//   bgcolor: "background.paper",
//   boxShadow: 0,
//   p: 4,
//   borderRadius: 2,
// };

// // const style_picture = {
// //   position: "absolute",
// //   top: "50%",
// //   left: "50%",
// //   transform: "translate(-50%, -50%)",
// //   bgcolor: "background.paper",
// //   boxShadow: 6,
// //   p: 4,
// //   borderRadius: 2,
// //   overflow: "auto",
// //   maxHeight: "80vh",
// // };

// export default function BasicPage() {
//   const [anchorEl, setAnchorEl] = React.useState(null);
//   const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
//   const [openAccountInfo, setOpenAccountInfo] = React.useState(false);
//   const [openConfirmDelete, setOPenCornfirmDelete] = React.useState(false);

//   const handlOpenConfirm = () => {
//     console.log("delete clicked!");
//     setOPenCornfirmDelete(true);
//   };
//   const handlCloseConfirm = () => {
//     setOPenCornfirmDelete(false);
//   };

//   const handleOpenAccountInfo = () => {
//     setOpenAccountInfo(true);
//   };
//   const handleCloseAccountInfo = () => {
//     setOpenAccountInfo(false);
//   };

//   const isMenuOpen = Boolean(anchorEl);
//   const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

//   const handleProfileMenuOpen = (event) => {
//     setAnchorEl(event.currentTarget);
//   };

//   const handleMobileMenuClose = () => {
//     setMobileMoreAnchorEl(null);
//   };

//   const handleMenuClose = () => {
//     setAnchorEl(null);
//     handleMobileMenuClose();
//   };

//   const handleMobileMenuOpen = (event) => {
//     setMobileMoreAnchorEl(event.currentTarget);
//   };

//   const menuId = "primary-search-account-menu";
//   const renderMenu = (
//     <Menu
//       anchorEl={anchorEl}
//       anchorOrigin={{
//         vertical: "top",
//         horizontal: "right",
//       }}
//       id={menuId}
//       keepMounted
//       transformOrigin={{
//         vertical: "top",
//         horizontal: "right",
//       }}
//       open={isMenuOpen}
//       onClose={handleMenuClose}
//     >
//       <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
//       <MenuItem onClick={handleMenuClose}>My account</MenuItem>
//     </Menu>
//   );
//   const mobileMenuId = "primary-search-account-menu-mobile";
//   const renderMobileMenu = (
//     <Menu
//       anchorEl={mobileMoreAnchorEl}
//       anchorOrigin={{
//         vertical: "top",
//         horizontal: "right",
//       }}
//       id={mobileMenuId}
//       keepMounted
//       transformOrigin={{
//         vertical: "top",
//         horizontal: "right",
//       }}
//       open={isMobileMenuOpen}
//       onClose={handleMobileMenuClose}
//     >
//       <MenuItem>
//         <IconButton size="large" aria-label="show 4 new mails" color="inherit">
//           <Badge badgeContent={4} color="error">
//             <MailIcon />
//           </Badge>
//         </IconButton>
//         <p>Messages</p>
//       </MenuItem>
//       <MenuItem>
//         <IconButton
//           size="large"
//           aria-label="show 17 new notifications"
//           color="inherit"
//         >
//           <Badge badgeContent={17} color="error">
//             <NotificationsIcon />
//           </Badge>
//         </IconButton>
//         <p>Notifications</p>
//       </MenuItem>
//       <MenuItem onClick={handleProfileMenuOpen}>
//         <IconButton
//           size="large"
//           aria-label="account of current user"
//           aria-controls="primary-search-account-menu"
//           aria-haspopup="true"
//           color="inherit"
//         >
//           <AccountCircle />
//         </IconButton>
//         <p>Profile</p>
//       </MenuItem>
//     </Menu>
//   );
//   const location = useLocation();
//   // const incoming_username_data= "= location.state";

//   const incoming_usr_data = "";

//   const incoming_username_data = "incoming_usr_data.usr_name";
//   // if (inc != null) {
//   //   console.log("navigated username", inc);

//   //   const incoming_username_data = inc.usr_name;
//   // }

//   // const incoming_username_data = "null";
//   // setUsername(incoming_username_data)

//   // const departments = [
//   //   {
//   //     value: "North",
//   //     label: "North",
//   //   },
//   //   {
//   //     value: "West",
//   //     label: "West",
//   //   },
//   // ];

//   // const positions = [
//   //   {
//   //     value: "Chief",
//   //     label: "Chief",
//   //   },
//   //   {
//   //     value: "AsstChief",
//   //     label: "AsstChief",
//   //   },
//   // ];

//   const options = [
//     { value: "A", label: "A" },
//     { value: "B", label: "B" },
//     { value: "C", label: "C" },
//     // { value: "option3", label: "D" },
//   ];
//   const [openSearchModal, setOpenSearchModal] = React.useState(false);
//   const [searchInput, setSearchInput] = React.useState("");
//   const [openaddNurseModal, setAddNurse] = React.useState(false);

//   const [authEdit, setAuthEdit] = React.useState(false);

//   const [authDelete, setAuthDelete] = React.useState(false);
//   const [openEditNurse, setEditNurse] = React.useState(false);

//   const [authAddNewNurse, setAuthAdd] = React.useState(false);

//   const [selectedRowData, setSelectedRowData] = React.useState(5);

//   const { nurseData, error, getNurseByName } = useGetNurseByName();
//   const [pos, setPos] = React.useState("");
//   const [dept, setDept] = React.useState("");

//   const [nurseId, setNurseId] = React.useState(""); // Nurse ObjectId
//   // const [name, setName] = React.useState("");
//   // const [departmentT, setDepartment] = React.useState("");
//   const [wards, setWards] = React.useState([]);

//   const [results, setResults] = React.useState([]);

//   const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);

//   const [openIncorrectPassword, setIncorrectPassword] = React.useState(false);

//   const [signatureImageData, setSignatureImageData] = React.useState("");

//   const [openWrongPwdError, setOpenWrongPwdError] = React.useState(false);
//   const [openEmptySearchTypeError, setOpenEmptySearchTypeError] =
//     React.useState(false);

//   const [incoming_nursesData, setIncomingNursesData] = React.useState([]);

//   const [searchType, setSearchType] = React.useState("");

//   // const [username, setUsername] = React.useState("");

//   const [positions, setPositions] = React.useState([
//     // {
//     //   value: "Chief",
//     //   label: "Chief",
//     // },
//     // {
//     //   value: "AsstChief",
//     //   label: "AsstChief",
//     // },
//     // {
//     //   value: "ASST",
//     //   label: "ASST",
//     // },
//   ]);

//   //const { user, login } = React.useContext(AppContext);
//   // const { username, logout } = React.useContext(AppContext);

//   // const { userData } = React.useContext(AppContext);

//   // console.log("context user name:->", userData.username);

//   const storeduserData = localStorage.getItem("userName_storage");
//   const userData = JSON.parse(storeduserData);

//   // const [ward_options, setWardOptions] = React.useState([
//   //   { value: "A", label: "A" },
//   //   { value: "B", label: "B" },
//   //   { value: "C", label: "C" },
//   //   { value: "D", label: "D" },
//   // ]);

//   const [departments, setDepartments] = React.useState([]);

//   const [ward_options, setWardOptions] = React.useState([]);

//   const [selectedOption, setSelectedOption] = React.useState(null);

//   //const contextData = React.useContext(MyContext);

//   const [state, setState] = React.useState({
//     gilad: true,
//     jason: false,
//     antoine: false,
//   });

//   const { gilad, jason, antoine } = state;

//   const handleChangeCheckbox = (event) => {
//     setState({
//       ...state,
//       [event.target.name]: event.target.checked,
//     });
//   };

//   // const [signatureImageData, setSignatureImageData] = React.useState(
//   // nurseData.signatureImageData
//   // );

//   const [searchResult, setSearchResult] = React.useState("");

//   const [input, setInput] = React.useState("");
//   const [default_search_name, setDefault_search_name] = React.useState(true);

//   // const [personName, setPersonName] = React.useState([]);

//   const [personName, setPersonName] = React.useState([]);

//   // const { text, setText } = React.useContext(MyContext);

//   const handlePersonNameChange = (newPersonName) => {
//     setPersonName(newPersonName);
//   };

//   const fetchData = (value) => {
//       setLoading(true);
//     console.log("fetch data is called!");
//     fetch("https://nurse-backend.onrender.com/get-nurses")
//       .then((response) => response.json())
//       .then((json) => {
    
//         handleGetAllNurses();
//         const results = json.filter((user) => {
//           return (
//             value &&
//             user &&
//             user.name &&
//             user.name.toLowerCase().includes(value.toLowerCase())
//           );
//         });

//         setResults(results);
//       })
//       .finally(() => {
//         setLoading(false);
//       });
//   };

//   const hasEmptyProperty = (obj) => {
//     for (const key in obj) {
//       if (obj.hasOwnProperty(key)) {
//         if (obj[key] === undefined || obj[key] === null || obj[key] === "") {
//           return true; 
//         }
//       }
//     }
//     return false; // No empty property found
//   };

//   const handleChange = (value) => {
//     setInput(value);

//     if (searchType === "search_name" || default_search_name) {
//       fetchData(value);
//     } else if (searchType === "search_ward") {
//       console.log("search by wards");
//       console.log("incoming nues dara", incoming_nursesData);
//       console.log("filtered", filterNursesByWard(incoming_nursesData, value));

//       setResults(filterNursesByWard(incoming_nursesData, value));
//     }
//   };

//   function handleSearchResultItem(result) {
//     console.log("search item clicked!!!", result);
//     setSearchResult(result);

//     //      postLogs(incoming_username_data, inc.usr_role, `ADDED NURSE:- ${data.name}`);

//     postLogs(userData.username, userData.role, `SEARCHED: ${result.name}`);

//     setOpenSearchModal(true);
//     setInput("");
//     results.length = 0;
//     console.log("search results:->", results);
//   }


//   function filterNursesByWard(nurseData, targetWard) {
//     console.log("receive nurse data:", nurseData);
//     console.log("receive target ward:", targetWard);

//     const desiredWard = targetWard.toLowerCase();

//     const resultss = nurseData.filter((user_) => {
//       //const valuez = "Oliver Hansen"; // Replace "your_search_value" with the actual value you want to search for
//       const valuez = targetWard;
//       return (
//         valuez &&
//         user_ &&
//         user_.wards &&
//         user_.wards.some((role) =>
//           role.toLowerCase().includes(valuez.toLowerCase())
//         )
//       );
//     });

//     // console.log("results search by name", results);
//     console.log("results2", resultss);

//     const filteredNurses = nurseData.filter((nurse) =>
//       nurse.wards.includes(desiredWard)
//     );

//     const filteredNames = nurseData
//       .filter((user) => user.wards.includes(targetWard))
//       .map((user) => user.name);

//     const nurseNames = filteredNurses.map((nurse) => nurse.name);
//     console.log("filtered by ward", filteredNames);

//     return resultss;
//   }

 

//     const [loading, setLoading] = React.useState(false);

//   function handleGetAllNurses() {
//        setLoading(true);
//     // console.log("handle get all nurses called!");
//     fetch("https://nurse-backend.onrender.com/get-nurses")
//       .then((response) => response.json())
//       .then((json) => {
//         console.log("rec", json);
//         setIncomingNursesData(json);

//         filterNurses(json);

//         //   console.log("incoming", incoming_nursesData);

//         //  console.log("filtere:->", filteredNurseNames(json, "Ward A"));
//       })  .finally(() => {
//         setLoading(false); // Set loading to false after the request is complete (success or error)
//       });
//   }

//   const handleRadioChange = (event) => {
//     setSearchType(event.target.value);
//     console.log("Selected search type:", event.target.value);

//     if (event.target.value === "search_ward") {
//       setDefault_search_name(false);
//     } else if (event.target.value === "search_name") {
//       setDefault_search_name(true);
//       setSearchType("search_name");
//     }
//   };

//   const filterNurses = (nurseList) => {
//     const currentDateTime = new Date();

//     const filteredNurses = nurseList.filter((nurse) =>
//       isOldNurse(nurse, currentDateTime)
//     );

//     // Do something with filteredNurses, e.g., update state, display in UI, etc.
//     // console.log("old nurses:->", filteredNurses);
//   };

//   const isOldNurse = (nurse, currentDateTime) => {
//     const createdDateTime = new Date(nurse.created_at);
//     const sixtyDaysAgo = new Date(currentDateTime);
//     sixtyDaysAgo.setDate(currentDateTime.getDate() - 60);

//     return createdDateTime <= sixtyDaysAgo;
//   };

//   //setUsername(incoming_username_data)

//   const fetchSettings = async () => {
//     try {
//       const response = await axios.get(
//         "https://nurse-backend.onrender.com/get-settings"
//       );

//       let positionsArray = [];
//       let wardsArray = [];
//       let departmentsArray = [];

//       const wards_state_arr = [];
//       const positions_arr = [];
//       const dept_arr = [];

//       response.data.forEach((item) => {
//         // console.log("settings", item);
//         // setSettingsId(item._id);
//         positionsArray = positionsArray.concat(item.position_options);
//         departmentsArray = departmentsArray.concat(item.department_options);
//         wardsArray = wardsArray.concat(item.ward_options);
//       });

//       wardsArray.forEach((ward_item) => {
//         wards_state_arr.push(ward_item);
//       });

//       positionsArray.forEach((ward_item) => {
//         positions_arr.push({ value: ward_item, label: ward_item });
//       });

//       departmentsArray.forEach((dept_item) => {
//         // depet_arr.push(dept_item);
//         dept_arr.push({ value: dept_item, label: dept_item });
//       });

//       //console.log("constructed wards array:", wards_state_arr);

//       setWardOptions(wards_state_arr);
//       setPositions(positions_arr);

//       // setPositions(positionsArray);
//       setDepartments(dept_arr);
//       // setWards(wardsArray);
//     } catch (error) {
//       console.error("Error fetching data:", error);
//     }
//   };

//   React.useEffect(() => {
//     // This code will run when the component mounts
//     setSearchInput(""); // Set searchInput to an empty string
//     //console.log("nurseData", nurseData);
//     //console.log("incoming context:", contextData);

//     handleGetAllNurses();

//     fetchSettings();

//     //filterNurses();
//   }, [openSearchModal]); // The empty dependency array ensures the effect runs only once when the component mounts

//   const navigate = useNavigate();

//   const handleNavigate1 = () => {
//     navigate("/");
//   };

//   const sigCanvasRef = React.useRef(null);
//   const sigCanvasRef2 = React.useRef(null);
//   const sigCanvasRef3 = React.useRef(null);
//   const sigCanvasRefEdit = React.useRef(null);
//   const sigCanvasRefEdit2 = React.useRef(null);
//   const sigCanvasRefEdit3 = React.useRef(null);

//   const clearSignature = () => {
//     sigCanvasRef.current.clear();
//     sigCanvasRef2.current.clear();
//     sigCanvasRef3.current.clear();
//   };

//   const postData = async (data_) => {
//     console.log("data to be sent", data_);

//     const data_sending = {
//       name: data_.name,
//       department: data_.department,
//       wards: data_.wards,
//       position: data_.position,
//       signatureImageData_1: data_.signatureImageData,
//       signatureImageData_2: data_.signatureImageData2,
//       signatureImageData_3: data_.signatureImageData3,
//     };
//     try {
//       const response = await axios.post(
//         // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
//         "https://nurse-backend.onrender.com/add-nurse",
//         // "https://nurse-backend.onrender.com/create",
//         //"http://localhost:8080/add-nurse",
//         data_sending
//       );

//       // Handle the response from the backend if needed
//       console.log("Response from the backend:", response.data);
//       postLogs(userData.username, userData.role, `ADDED NURSE:- ${data_.name}`);

//       // postLogs(data.name, )
//       setOpenSuccessAlert(true);
//     } catch (error) {
//       // Handle any errors that occur during the request
//       console.error("Error sending data to the backend:", error);
//     }
//     // alert("nurse saved successfully...");

//     handleCloseAddNurse();
//   };

//   const signupInformationRef = React.useRef({
//     name: "",
//     // last_name: "",
//     position: "",
//     // email: "",
//     // disabled: false,
//     department: "",
//     // role: "Admin",
//     // hashed_pwd: "",
//     wards: [],
//     signatureImageData: "",
//     signatureImageData2: "",
//     signatureImageData3: "",
//   });

//   const editValues = {
//     name: "jesse",
//   };

//   const formik = useFormik({
//     initialValues: {
//       name: "",
//       department: "",
//       position: "",
//       wards: [],
//       signatureImageData: "",
//       signatureImageData2: "",
//       signatureImageData3: "",
//     },
//     // validationSchema: validationSchema,
//     onSubmit: (values) => {
//       // Handle form submission here
//       console.log(" add form data:-", values);
//       if (values.name == "" || undefined || null) {
//         console.log("name field is empty");
//       } else if (values.department == "" || undefined || null) {
//         console.log("department is empty");
//       } else if (values.position == "" || undefined || null) {
//         console.log("position is empty");
//       } else {
//         const signatureImageURL = sigCanvasRef.current.toDataURL();
//         const signatureImageURL2 = sigCanvasRef2.current.toDataURL();
//         const signatureImageURL3 = sigCanvasRef3.current.toDataURL();
//         signupInformationRef.current.name = values.name;
//         signupInformationRef.current.position =
//           values.position !== undefined ? values.position : "empty";
//         signupInformationRef.current.department =
//           values.department !== undefined ? values.department : "empty";
//         signupInformationRef.current.wards = personName; // Add selected wards to signupInformationRef
//         signupInformationRef.current.signatureImageData = signatureImageURL;
//         signupInformationRef.current.signatureImageData2 = signatureImageURL2;
//         signupInformationRef.current.signatureImageData3 = signatureImageURL3;

//         console.log("sign up info:-", signupInformationRef.current);

//         postData(signupInformationRef.current, incoming_username_data);

//         // setAuthAdd(false);
//       }

//       // setTimeout(postData(signupInformationRef.current), 3000)

//       // sendMessage("facility_data", values);
//       // navigate("/facilities");
//     },
//   });

//   const formikEdit = useFormik({
//     initialValues: {
//       name: "",
//       department: "",
//       position: "",
//       wards: [],
//       signatureImageData: "",
//     },
//     //validationSchema: validationSchema,
//     onSubmit: (values) => {
//       // Handle form submission here
//       // console.log("form data for edit:-", values);
//       console.log("passed id", searchResult.ID);
//       console.log("on submit edit", sigCanvasRefEdit.current.toDataURL());
//       const newEdit = sigCanvasRefEdit.current.toDataURL();
//       const newEdit2 = sigCanvasRefEdit2.current.toDataURL();
//       const newEdit3 = sigCanvasRefEdit3.current.toDataURL();

//       // handleEditNurse(values, newEdit, searchResult.ID);
//       handleEditNurse(values, newEdit, newEdit2, newEdit3, searchResult.ID);
//     },
//   });


//   const style = {
//     position: "absolute",
//     top: "50%",
//     left: "50%",
//     transform: "translate(-50%, -50%)",
//     bgcolor: "background.paper",
//     boxShadow: 6,
//     p: 4,
//     borderRadius: 2,
//     overflow: "auto",
//     maxHeight: "80vh",
//   };
 






//   function aboutToSearch() {
//     //console.log("about to search...", searchType, typeof default_search_name);

//     if (!default_search_name) {
//       console.log("-->set to true!!!!!");
//     }

//     if (searchType.length === 0 && !default_search_name) {
//       console.log("search type is empty!");
//       setOpenWrongPwdError(true);
//       setOpenEmptySearchTypeError(true);
//     }
//   }

//   const handleClose = () => {
//     setOpenSearchModal(false);
//     console.log("search modal deactivated...");
//   };

//   const handleCloseErrorModal = () => {
//     setOpenWrongPwdError(false);
//     console.log("search modal deactivated...");
//   };

//   function handleSearch() {
//     console.log("Search submitted..", searchInput);
//     getNurseByName(searchInput);

//     // setNurseId(nurseData._id)
//     // setName(nurseData.name)
//   }

//   function handleAddNurse() {
//     console.log("handle Add nurse....");
//     //setAuthAdd(true);
//     setAddNurse(true);
//   }

//   function handleOpenEditModal() {
//     console.log("nurseEditData", searchResult);
//     formikEdit.setValues({
//       id: searchResult._id,
//       name: searchResult.name || "", // Set default value to an empty string if nurseData.name is undefined
//       department: searchResult.department || "Department", // Set a default department value or adjust as needed
//       position: searchResult.position,

//       wards: searchResult.wards || [], // Set default wards value to an empty array if nurseData.wards is undefined
//       // ... other form fields
//     });
//     setEditNurse(true);
//     setOpenSearchModal(false);
//   }



//   async function handleOpenDeleteModal(search_res) {
//     console.log("nurseDeleteData", search_res);

//     try {
//       const response = await axios.delete(
//         `https://nurse-backend.onrender.com/delete-nurse/${search_res._id}`,
//         {
//           headers: {
//             "Content-Type": "application/json",
//           },
//         }
//       );

//       // Handle success
//       console.log("deleted successfully");
//       postLogs(incoming_username_data, `DELETED:${search_res.name}`);
//       setTimeout(window.location.reload(), 4000);

//       setOpenSuccessAlert(true);

//       //window.location.reload();
//       console.log("should be navigatiing...");
//     } catch (error) {
//       // Handle error
//       // toast({
//       // title: "Cannot be deleted",
//       // position: "top",
//       // isClosable: true,
//       // duration: 3000,
//       // status: "error",
//       // });
//     }
//   }

//   function handleOpenAuthEdit() {
//     // setAuthEdit(true);
//     handleOpenEditModal();
//   }

//   function handleOpenAuthDelete() {
//     // setAuthDelete(true);

//     handleOpenDeleteModal();
//   }

//   function handleCloseEditModal() {
//     setEditNurse(false);
//   }

//   function handleCloseDeleteAuth() {
//     setAuthDelete(false);
//   }

//   function handleCloseAddAuth() {
//     setAuthAdd(false);
//   }

//   function handleCloseAddNurse() {
//     setAddNurse(false);
//   }

//   function handleCloseEditAuth() {
//     setAuthEdit(false);
//   }

//   const handleChange_dept = (event) => {
//     setDept(event.target.value);
//   };

//   const handleChange_pos = (event) => {
//     setPos(event.target.value);
//   };

//   function handleSubmit_Edit_form() {}

//   function handleCloseFacility() {}

//   function handleNavigateNursePage() {
//     console.log("nurse clicked");
//     navigate("/nurses");
//   }

//   function handleNavigateSettingsPage() {
//     console.log("setings clicked");
//     navigate("/nurses");
//   }

//   const handlePUTrequest = async (nurseId, updated_nurse_info) => {
//     console.log("updated nurse info:", updated_nurse_info);
//     try {
//       const response = await fetch(
//         `https://nurse-backend.onrender.com/update-nurse${updated_nurse_info.id}`,
//         {
//           method: "PUT",
//           headers: {
//             "Content-Type": "application/json",
//           },
//           body: JSON.stringify({
//             name: updated_nurse_info.Name,
//             department: updated_nurse_info.department,
//             wards: updated_nurse_info.wards,
//             position: updated_nurse_info.position,
//             signatureImageData: signatureImageData,
//           }),
//         }
//       );

//       if (response.ok) {
//         console.log("Nurse information updated successfully");
//         postLogs(incoming_username_data, `edited: ${updated_nurse_info.name}`);
//         setOpenSuccessAlert(true);
//         // Handle further actions or UI updates
//         //console.log("body", signatureImageData);
//       } else {
//         console.error("Failed to update nurse information");
//       }
//     } catch (error) {
//       console.error("An error occurred:", error);
//     }
//   };

//   const handlePUTrequest_for_edited_signature = async (
//     nurseId,
//     new_sig,
//     new_sig2,
//     new_sig3,
//     updated_nurse_info,
//     ward
//   ) => {
//     console.log("new nurse infor", updated_nurse_info);

//     if (hasEmptyProperty(updated_nurse_info)) {
//       console.log("Object has at least one empty property.");
//     } else {
//       console.log("Object does not have any empty properties.");

//       // console.log("new signature being updated", updated_nurse_info);
//       try {
//         const response = await fetch(
//           `https://nurse-backend.onrender.com/edit-nurse/${updated_nurse_info.id}`,
//           // `http://localhost:8080/edit-nurse/${updated_nurse_info.id}`,
//           {
//             method: "PUT",
//             headers: {
//               "Content-Type": "application/json",
//             },
//             body: JSON.stringify({
//               name: updated_nurse_info.name,
//               department: updated_nurse_info.department,
//               wards: ward,
//               position: updated_nurse_info.position,
//               signatureImageData_1: new_sig,
//               signatureImageData_2: new_sig2,
//               signatureImageData_3: new_sig3,
//             }),
//           }
//         );

//         if (response.ok) {
//           console.log("Nurse information updated successfully");
//           postLogs(
//             incoming_username_data,
//             `edited: ${updated_nurse_info.name}`
//           );
//           setOpenSuccessAlert(true);

//           setEditNurse(false);
//           // Handle further actions or UI updates
//           //console.log("body", signatureImageData);
//         } else {
//           console.error("Failed to update nurse information");
//         }
//       } catch (error) {
//         console.error("An error occurred:", error);
//       }
//     }

 
//   };

//   const handleEditNurse = async (values, sig, sig2, sig3, id) => {
//     console.log("incoming id", id);
//     console.log("edited value passed", values);

//     // console.log("image url:", sigCanvasRef )

//     // const signatureImageURL = sigCanvasRef.current.toDataURL();
//     //console.log("image url", sigCanvasRef )
//     //const signatureImageURL2 = sigCanvasRef2.current.toDataURL();
//     // const signatureImageURL3 = sigCanvasRef3.current.toDataURL();
//     //
//     //console.log("signature image uRl:", signatureImageURL);
//     const binaryImageData = atob(sig.split(",")[1]);
//     // const binaryImageData2 = atob(signatureImageURL2.split(",")[1]);
//     // const binaryImageData3 = atob(signatureImageURL3.split(",")[1]);

//     const imageSizeBytes = binaryImageData.length;
//     // const imageSizeBytes2 = binaryImageData2.length;
//     //const imageSizeBytes3 = binaryImageData3.length;

//     console.log("signature size:", imageSizeBytes);

//     if (
//       imageSizeBytes < 3000
//       // imageSizeBytes2 < 3000 &&
//       // imageSizeBytes3 < 3000
//     ) {
//       console.log("image size bytes less than 3000");
//       //signatureImageURL = signatureImageData
//       setSignatureImageData(nurseData.signatureImageData_1);
//       handlePUTrequest(nurseId, values);

//       //console.log("less than 3000", nurseData.signatureImageData);
//     } else if (
//       imageSizeBytes > 3000
//       // imageSizeBytes2 > 3000 &&
//       //imageSizeBytes3 > 3000
//     ) {
//       console.log("image size bytes greater than 3000");
//       //console.log("edited signature of the nurse");
//       //setSignatureImageData(toString(signatureImageURL));
//       // console.log("type", typeof signatureImageURL);
//       handlePUTrequest_for_edited_signature(
//         id,
//         sig,
//         sig2,
//         sig3,
//         values,
//         personName
//       );
//     }

//     <Alert severity="success" color="info">
//       Nurse information edited successfully...
//     </Alert>;
//   };

//   const handleDelete = async (nurse_data) => {
//     handleOpenAuthDelete();
//   };

//   const [isSideNavOpen, setIsSideNavOpen] = React.useState(false);

//   const handleToggleSideNav = () => {
//     console.log("opened");
//     setIsSideNavOpen(!isSideNavOpen);
//   };

//   const LightTooltip = styled(({ className, ...props }) => (
//     <Tooltip {...props} classes={{ popper: className }} />
//   ))(({ theme }) => ({
//     [`& .${tooltipClasses.tooltip}`]: {
//       backgroundColor: theme.palette.common.white,
//       color: "rgba(0, 0, 0, 0.87)",
//       boxShadow: theme.shadows[1],
//       fontSize: 11,
//     },
//   }));

//   return (
//     <>
//       <BasicNavbar />
//       <div>{loading && <LinearProgress />}</div>

//       <ErrorSnackbar
//         open={openWrongPwdError}
//         autoHideDuration={3000}
//         onClose={handleCloseErrorModal}
//       />

//       <ErrorSnackbar_SearchType
//         open={openEmptySearchTypeError}
//         autoHideDuration={6000}
//         onClose={() => setOpenEmptySearchTypeError(false)}
//       />

//       <SuccessSnackbar
//         open={openSuccessAlert}
//         autoHideDuration={3000}
//         onClose={() => setOpenSuccessAlert(false)}
//       />

//       {searchResult && (
//         <Modal
//           open={openSearchModal}
//           aria-labelledby="modal-modal-title"
//           aria-describedby="modal-modal-description"
//         >
//           <Box sx={style}>
//             <div className="flex flex-row justify-between">
//               <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
//                 View Nurse Details
//               </Typography>

//               <Button onClick={handleClose} color="error" variant="contained">
//                 <CloseIcon fontSize="40" />
//               </Button>
//             </div>

//             <div className="flex flex-col gap-4 m-3">
//               <div className="flex flex-row gap-3">
//                 <TextField
//                   label="Name"
//                   value={searchResult.name}
//                   InputProps={{
//                     readOnly: true,
//                     style: { fontWeight: "bold", fontSize: "18px" },
//                   }}
//                 />
//                 <TextField
//                   label="Department"
//                   value={searchResult.department}
//                   InputProps={{
//                     readOnly: true,
//                   }}
//                 />
//               </div>
//               <div className="flex flex-row gap-3">
//                 <TextField
//                   label="Position"
//                   value={searchResult.position}
//                   InputProps={{
//                     readOnly: true,
//                   }}
//                 />
//                 <TextField
//                   label="Wards"
//                   value={searchResult.wards}
//                   InputProps={{
//                     readOnly: true,
//                   }}
//                 />
//               </div>
//             </div>

//             <Box className="border border-1 border-gray-400 p-2  m-4 rounded-md">
//               <Typography id="modal-modal-title" sx={{ m: 2 }}>
//                 Signature 1:
//               </Typography>
//               <img
//                 src={searchResult.signatureImageData_1}
//                 // src={LOGO}
//                 alt="NOT FOUND"
//                 style={{ width: "300px", height: "auto" }}
//               />
//             </Box>

//             <Box className="border border-1 border-gray-400 p-2  m-4 rounded-md">
//               <Typography id="modal-modal-title" sx={{ m: 2 }}>
//                 Signature 2:
//               </Typography>
//               <img
//                 src={searchResult.signatureImageData_2}
//                 // src={LOGO}
//                 alt="NOT FOUND"
//                 style={{ width: "300px", height: "auto" }}
//               />
//             </Box>

//             <Box className="border border-1 border-gray-400 p-2  m-4 rounded-md">
//               <Typography id="modal-modal-title" sx={{ m: 2 }}>
//                 Signature 3:
//               </Typography>
//               <img
//                 src={searchResult.signatureImageData_3}
//                 // src={LOGO}
//                 alt="NOT FOUND"
//                 style={{ width: "300px", height: "auto" }}
//               />
//             </Box>
//             <div className="flex flex-row gap-6 items-center justify-center mt-2"></div>
//           </Box>
//         </Modal>
//       )}

//       <Box className="flex justify-center text-center w-full mt-[12%]">
//         <img
//           src={LOGO2}
//           alt="NOT FOUND"
//           className="w-[200px] rounded-full shadow-xl"
//         />
//       </Box>
//       <div className="mt-12">
//         <div className="flex text-center justify-center ">
//           <Box className="flex flex-row bg-gray-100 w-[550px]  rounded-lg shadow-xl gap-[300px]">
//             <Search>
//               <SearchIconWrapper>
//                 <SearchIcon />
//               </SearchIconWrapper>
//               <StyledInputBase
//                 //onClick={handleSearchModal}
//                 onClick={aboutToSearch}
//                 //placeholder="Search…"
//                 placeholder={searchType || "Search Name..."}
//                 inputProps={{ "aria-label": "search" }}
//                 value={input}
//                 onChange={(e) => handleChange(e.target.value)}
//               />
//             </Search>
//           </Box>
//         </div>

//         <div
//           className="border rounded-md shadow-lg mx-auto max-w-[350px] md:max-w-[400px] lg:max-w-[550px]"
//           style={scroll}
//         >
//           {results.map((result, id) => {
//             // return <SearchResult result={result.name} key={id} />;
//             return (
//               <div
//                 className="flex flex-row gap-2 hover:bg-blue-400 p-2 cursor-pointer"
//                 key={id}
//                 onClick={() => handleSearchResultItem(result)}
//               >
//                 {result.name}
//               </div>
//             );
//           })}
//         </div>

//         <div className="flex text-center justify-center">
//           <div className="flex flex-col">
//             <div className="flex flex-row gap-2">
//               <FormControl>
//                 <RadioGroup
//                   row
//                   aria-labelledby="demo-row-radio-buttons-group-label"
//                   name="row-radio-buttons-group"
//                 >
//                   <LightTooltip title="search by name" placement="bottom">
//                     <FormControlLabel
//                       value="search_name"
//                       checked={default_search_name}
//                       control={<Radio />}
//                       label="Search Name"
//                       onChange={handleRadioChange}
//                       //    onChange={setSearch_by_name(true)}
//                       style={{ color: "#1976D2", fontWeight: "bold" }}
//                     />
//                   </LightTooltip>
//                   <LightTooltip title="search by ward" placement="bottom">
//                     <FormControlLabel
//                       value="search_ward"
//                       control={<Radio />}
//                       label="Search Name By Ward"
//                       onChange={handleRadioChange}
//                       //   onChange={setSearch_by_ward(true)}
//                       style={{ color: "#1976D2", fontWeight: "bold" }}
//                     />
//                   </LightTooltip>
//                 </RadioGroup>
//               </FormControl>
//             </div>
//             <div className="mt-8"></div>
//           </div>
//         </div>
//         {renderMobileMenu}
//         {renderMenu}
//       </div>

//       {/* {results && results.length > 0 && <SearchResultsList results={results} />} */}
//     </>
//   );
// }
// const text = {
//   fontSize: "14px",
//   padding: "5px",
//   borderRadius: "6px",
// };
// const scroll = {
//   overflowY: " auto",
//   maxHeight: "40vh",
// };



import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Menu from "@mui/material/Menu";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import CloseIcon from "@mui/icons-material/Close";
import * as Yup from "yup";
import MoreIcon from "@mui/icons-material/MoreVert";

import { useFormik } from "formik";
// import Select from "@mui/material/Select";
import health_logo from "../../assets/logo4.png";
import LOGO from "../../assets/logo4.png";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import LOGO2 from "../../assets/logo56.png";
import MenuItem from "@mui/material/MenuItem";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Badge from "@mui/material/Badge";
import IconButton from "@mui/material/IconButton";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MailIcon from "@mui/icons-material/Mail";
import NotificationsIcon from "@mui/icons-material/Notifications";
import ListIcon from "@mui/icons-material/List";
import Tooltip from "@mui/material/Tooltip";
import LoginIcon from "@mui/icons-material/Login";
// const _ = require("lodash");
import _ from "lodash";
import {
  Alert,
  AlertTitle,
  Button,
  ButtonBase,
  Checkbox,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  FormLabel,
  LinearProgress,
  ListItemText,
  Modal,
  OutlinedInput,
  Paper,
  Radio,
  RadioGroup,
  Snackbar,
} from "@mui/material";
import TextField from "@mui/material/TextField";
// import useGetNurseByName from "../hooks/SearchNurseHook";
import ReactSignatureCanvas, { SignatureCanvas } from "react-signature-canvas";
import axios from "axios";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
import { Margin, Refresh } from "@mui/icons-material";
import { SearchBar } from "../SearchBar";
import { SearchResultsList } from "../SearchResultList";
// import constructWithOptions from "styled-components/dist/constructors/constructWithOptions";
import { IoCloseSharp } from "react-icons/io5";
import ErrorSnackbar from "../ErrorSnackbar";
import SuccessSnackbar from "../SuccessSnackbar";
import { m } from "framer-motion";
import ErrorSnackbar_SearchType from "../ErrorSearchTypeSnackBar";
import { postLogs } from "../PostLogFunc";

import SettingsIcon from "@mui/icons-material/Settings";
import MultipleSelectCheckmarks from "../MultiSelect";

import NurseAddSucces from "../../components/NursesAlerts/NurseAddSucces";
import NurseDeactivate from "../../components/NursesAlerts/NurseDeactivate";

import { Formik, Form } from "formik";
import LightTooltip from "../../components/LightTool";
import ValidationFailed from "../../components/ValidationFailedSnack";

import BasicNavbar from "./BasicNavbar";

const validationSchema = Yup.object({
  name: Yup.string().required("Name is Required"),
  department: Yup.string().required("department is required"),
  position: Yup.string().required("Position is Required"),
  //  wards: Yup.array().required("Wards is Required"),
});

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = [
  "Oliver Hansen",
  "Van Henry",
  "April Tucker",
  "Ralph Hubbard",
  "Omar Alexander",
  "Carlos Abbott",
  "Miriam Wagner",
  "Bradley Wilkerson",
  "Virginia Andrews",
  "Kelly Snyder",
];

export default function BasicPage() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const [openAccountInfo, setOpenAccountInfo] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleOpenAccountInfo = () => {
    setOpenAccountInfo(true);
  };
  const handleCloseAccountInfo = () => {
    setOpenAccountInfo(false);
  };

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );
  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton size="large" aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="error">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem>
        <IconButton
          size="large"
          aria-label="show 17 new notifications"
          color="inherit"
        >
          <Badge badgeContent={17} color="error">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );
  const location = useLocation();
  // const incoming_username_data= "= location.state";

  const incoming_usr_data = "";

  const incoming_username_data = "incoming_usr_data.usr_name";
  // if (inc != null) {
  //   console.log("navigated username", inc);

  //   const incoming_username_data = inc.usr_name;
  // }

  // const incoming_username_data = "null";
  // setUsername(incoming_username_data)

  // const departments = [
  //   {
  //     value: "North",
  //     label: "North",
  //   },
  //   {
  //     value: "West",
  //     label: "West",
  //   },
  // ];

  // const positions = [
  //   {
  //     value: "Chief",
  //     label: "Chief",
  //   },
  //   {
  //     value: "AsstChief",
  //     label: "AsstChief",
  //   },
  // ];

  const options = [
    { value: "A", label: "A" },
    { value: "B", label: "B" },
    { value: "C", label: "C" },
    // { value: "option3", label: "D" },
  ];
  const [openSearchModal, setOpenSearchModal] = React.useState(false);
  const [searchInput, setSearchInput] = React.useState("");
  const [openaddNurseModal, setAddNurse] = React.useState(false);

  const [authEdit, setAuthEdit] = React.useState(false);

  const [authDelete, setAuthDelete] = React.useState(false);
  const [openEditNurse, setEditNurse] = React.useState(false);

  const [authAddNewNurse, setAuthAdd] = React.useState(false);

  const [selectedRowData, setSelectedRowData] = React.useState(5);

  // const { nurseData, loading, error, getNurseByName } = useGetNurseByName();
  const [pos, setPos] = React.useState("");
  const [dept, setDept] = React.useState("");

  const [nurseId, setNurseId] = React.useState(""); // Nurse ObjectId
  // const [name, setName] = React.useState("");
  // const [departmentT, setDepartment] = React.useState("");
  const [wards, setWards] = React.useState([]);

  const [results, setResults] = React.useState([]);

  const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);
  const [openAddNurseSuccess, setOpenAddNurseSuccess] = React.useState(false);
  const [openDeactivateNurseSuccess, setOpenDeactivateNurseSuccess] =
    React.useState(false);

  const [openIncorrectPassword, setIncorrectPassword] = React.useState(false);

  const [signatureImageData, setSignatureImageData] = React.useState("");

  const [openWrongPwdError, setOpenWrongPwdError] = React.useState(false);
  const [openEmptySearchTypeError, setOpenEmptySearchTypeError] =
    React.useState(false);

  const [incoming_nursesData, setIncomingNursesData] = React.useState([]);

  const [searchType, setSearchType] = React.useState("");
  const [validationFailed, setValidationFailed] = React.useState(false);

  // const [username, setUsername] = React.useState("");

  const [positions, setPositions] = React.useState([
    // {
    //   value: "Chief",
    //   label: "Chief",
    // },
    // {
    //   value: "AsstChief",
    //   label: "AsstChief",
    // },
    // {
    //   value: "ASST",
    //   label: "ASST",
    // },
  ]);

  //const { user, login } = React.useContext(AppContext);
  // const { username, logout } = React.useContext(AppContext);

  // const { userData } = React.useContext(AppContext);

  // console.log("context user name:->", userData.username);

  const storeduserData = localStorage.getItem("userName_storage");
  const userData = JSON.parse(storeduserData);

  // const [ward_options, setWardOptions] = React.useState([
  //   { value: "A", label: "A" },
  //   { value: "B", label: "B" },
  //   { value: "C", label: "C" },
  //   { value: "D", label: "D" },
  // ]);

  const [departments, setDepartments] = React.useState([]);

  const [ward_options, setWardOptions] = React.useState([]);

  const [selectedOption, setSelectedOption] = React.useState(null);

  //const contextData = React.useContext(MyContext);

  const [state, setState] = React.useState({
    gilad: true,
    jason: false,
    antoine: false,
  });

  const { gilad, jason, antoine } = state;

  const handleChangeCheckbox = (event) => {
    setState({
      ...state,
      [event.target.name]: event.target.checked,
    });
  };

  // const [signatureImageData, setSignatureImageData] = React.useState(
  // nurseData.signatureImageData
  // );

  const [searchResult, setSearchResult] = React.useState("");

  const [input, setInput] = React.useState("");
  const [default_search_name, setDefault_search_name] = React.useState(true);

  const [wardName, setWardName] = React.useState([]);

  // const { text, setText } = React.useContext(MyContext);

  const handleWardNameChange = (newWardName) => {
    setWardName(newWardName);
  };

  const fetchData = (value) => {
    console.log("fetch data is called!");
    // fetch("https://nurse-backend.onrender.com/get-nurses")
    fetch("https://nurse-backend.onrender.com/get-active-nurses")
      .then((response) => response.json())
      .then((json) => {
        //console.log("JSON:->", json);
        // setIncoming_nursesData((prevData) => [...prevData, json]);
        handleGetAllNurses();
        const results = json.filter((user) => {
          return (
            value &&
            user &&
            user.name &&
            user.name.toLowerCase().includes(value.toLowerCase())
          );
        });

        setResults(results);
      });
  };

  const isSignupInformationValid = (incoming_ref) => {
    const signupInformation = incoming_ref.current;

    console.log("incoming ref", signupInformation);

    // Check if any property is empty or falsy
    for (const key in signupInformation) {
      if (signupInformation.hasOwnProperty(key) && !signupInformation[key]) {
        return false; // Return false if any property is empty or falsy
      }
    }

    return true; // Return true if all properties are non-empty
  };

  const hasEmptyProperty = (obj) => {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (obj[key] === undefined || obj[key] === null || obj[key] === "") {
          return true; // Found an empty property
        }
      }
    }
    return false; // No empty property found
  };

  const handleChange = (value) => {
    setInput(value);

    if (searchType === "Search Name" || default_search_name) {
      fetchData(value);
    } else if (searchType === "Search Ward") {
      console.log("Search By Wards");
      console.log("incoming nues dara", incoming_nursesData);
      console.log("filtered", filterNursesByWard(incoming_nursesData, value));

      setResults(filterNursesByWard(incoming_nursesData, value));
    }
  };

  function handleSearchResultItem(result) {
    console.log("search item clicked!!!", result);
    setSearchResult(result);

    //      postLogs(incoming_username_data, inc.usr_role, `ADDED NURSE:- ${data.name}`);

    postLogs(userData.username, userData.role, `SEARCHED: ${result.name}`);

    setOpenSearchModal(true);
    setInput("");
    results.length = 0;
    console.log("search results:->", results);
  }

  // function filterNursesByWard(nurseData, targetWard) {
  //   console.log("receive nurse data:", nurseData);
  //   console.log("receive target ward:", targetWard);
  //   // const filteredNurses = nurseData
  //   //   .filter((nurse) =>
  //   //     nurse.wards.some(
  //   //       (ward) => ward.toLowerCase() === targetWard.toLowerCase()
  //   //     )
  //   //   )
  //   //   .map((nurse) => ({ name: nurse.name, matchingWard: targetWard }));
  //   const desiredWard = targetWard.toLowerCase();

  //   const filteredNurses = nurseData.filter((nurse) =>
  //     nurse.wards.includes(targetWard)
  //   );
  //   console.log("filtered by ward", filteredNurses)

  //   return filteredNurses;
  //   //return filteredNurses;
  // }
  function filterNursesByWard(nurseData, targetWard) {
    console.log("receive nurse data:", nurseData);
    console.log("receive target ward:", targetWard);

    const desiredWard = targetWard.toLowerCase();

    const resultss = nurseData.filter((user_) => {
      //const valuez = "Oliver Hansen"; // Replace "your_search_value" with the actual value you want to search for
      const valuez = targetWard;
      return (
        valuez &&
        user_ &&
        user_.wards &&
        user_.wards.some((role) =>
          role.toLowerCase().includes(valuez.toLowerCase())
        )
      );
    });

    // console.log("results search by name", results);
    console.log("results2", resultss);

    const filteredNurses = nurseData.filter((nurse) =>
      nurse.wards.includes(desiredWard)
    );

    const filteredNames = nurseData
      .filter((user) => user.wards.includes(targetWard))
      .map((user) => user.name);

    const nurseNames = filteredNurses.map((nurse) => nurse.name);
    console.log("filtered by ward", filteredNames);

    return resultss;
  }

  //   const desiredWard = targetWard.toLowerCase();

  // const filteredNurseNames = _.chain(nurseData)
  //   .filter((nurse) =>
  //     _.includes(
  //       _.map(nurse.ward, (ward) => ward.toLowerCase()),
  //       desiredWard
  //     )
  //   )
  //   .map("name")
  //   .value();

  //console.log("filtered by ward", filteredNurseNames);

  // function handleGetAllNurses() {
  //   // console.log("handle get all nurses called!");
  //   fetch("https://nurse-backend.onrender.com/get-nurses")
  //     .then((response) => response.json())
  //     .then((json) => {
  //       console.log("rec", json);
  //       setIncomingNursesData(json);

  //       filterNurses(json);

  //       //   console.log("incoming", incoming_nursesData);

  //       //  console.log("filtere:->", filteredNurseNames(json, "Ward A"));
  //     });
  // }

  function handleGetAllNurses() {
    setLoading(true); // Set loading to true when making the request

    fetch("https://nurse-backend.onrender.com/get-nurses")
      .then((response) => response.json())
      .then((json) => {
        console.log("rec", json);
        setIncomingNursesData(json);
        filterNurses(json);
      })
      .finally(() => {
        setLoading(false); // Set loading to false after the request is complete (success or error)
      });
  }

  const handleRadioChange = (event) => {
    setSearchType(event.target.value);
    console.log("Selected search type:", event.target.value);

    if (event.target.value === "Search Ward") {
      setDefault_search_name(false);
    } else if (event.target.value === "Search Name") {
      setDefault_search_name(true);
      setSearchType("Search Name");
    }
  };

  const filterNurses = (nurseList) => {
    const currentDateTime = new Date();

    const filteredNurses = nurseList.filter((nurse) =>
      isOldNurse(nurse, currentDateTime)
    );

    // Do something with filteredNurses, e.g., update state, display in UI, etc.
    // console.log("old nurses:->", filteredNurses);
  };

  const isOldNurse = (nurse, currentDateTime) => {
    const createdDateTime = new Date(nurse.created_at);
    const sixtyDaysAgo = new Date(currentDateTime);
    sixtyDaysAgo.setDate(currentDateTime.getDate() - 60);

    return createdDateTime <= sixtyDaysAgo;
  };

  //setUsername(incoming_username_data)

  const fetchSettings = async () => {
    try {
      const response = await axios.get(
        "https://nurse-backend.onrender.com/get-settings"
      );

      let positionsArray = [];
      let wardsArray = [];
      let departmentsArray = [];

      const wards_state_arr = [];
      const positions_arr = [];
      const dept_arr = [];

      response.data.forEach((item) => {
        // console.log("settings", item);
        // setSettingsId(item._id);
        positionsArray = positionsArray.concat(item.position_options);
        departmentsArray = departmentsArray.concat(item.department_options);
        wardsArray = wardsArray.concat(item.ward_options);
      });

      wardsArray.forEach((ward_item) => {
        wards_state_arr.push(ward_item);
      });

      positionsArray.forEach((ward_item) => {
        positions_arr.push({ value: ward_item, label: ward_item });
      });

      departmentsArray.forEach((dept_item) => {
        // depet_arr.push(dept_item);
        dept_arr.push({ value: dept_item, label: dept_item });
      });

      //console.log("constructed wards array:", wards_state_arr);

      setWardOptions(wards_state_arr);
      setPositions(positions_arr);

      // setPositions(positionsArray);
      setDepartments(dept_arr);
      // setWards(wardsArray);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  React.useEffect(() => {
    // This code will run when the component mounts
    setSearchInput(""); // Set searchInput to an empty string
    //console.log("nurseData", nurseData);
    //console.log("incoming context:", contextData);

    handleGetAllNurses();

    fetchSettings();

    //filterNurses();
  }, [openSearchModal]); // The empty dependency array ensures the effect runs only once when the component mounts

  const navigate = useNavigate();

  const handleNavigate1 = () => {
    navigate("/");
  };

  const sigCanvasRef = React.useRef(null);
  const sigCanvasRef2 = React.useRef(null);
  const sigCanvasRef3 = React.useRef(null);

  const sigCanvasRefEdit = React.useRef(null);
  const sigCanvasRefEdit2 = React.useRef(null);
  const sigCanvasRefEdit3 = React.useRef(null);

  const clearSignature = () => {
    sigCanvasRef.current.clear();
    sigCanvasRef2.current.clear();
    sigCanvasRef3.current.clear();
  };

  const postData = async (data_) => {
    console.log("data to be sent", data_);

    const data_sending = {
      name: data_.name,
      department: data_.department,
      wards: data_.wards,
      position: data_.position,
      status: "ACTIVE",
      signatureImageData_1: data_.signatureImageData,
      signatureImageData_2: data_.signatureImageData2,
      signatureImageData_3: data_.signatureImageData3,
    };
    try {
      const response = await axios.post(
        // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
        "https://nurse-backend.onrender.com/add-nurse",
        // "https://nurse-backend.onrender.com/create",
        //"http://localhost:8080/add-nurse",
        data_sending
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
      postLogs(userData.username, userData.role, `ADDED NURSE:- ${data_.name}`);

      // postLogs(data.name, )
      setOpenAddNurseSuccess(true);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    }
    // alert("nurse saved successfully...");

    handleCloseAddNurse();
  };

  const signupInformationRef = React.useRef({
    name: "",
    // last_name: "",
    position: "",
    // email: "",
    // disabled: false,
    department: "",
    // role: "Admin",
    // hashed_pwd: "",
    wards: [],
    signatureImageData: "",
    signatureImageData2: "",
    signatureImageData3: "",
  });

  const editValues = {
    name: "jesse",
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      department: "",
      position: "",
      wards: [],
      signatureImageData: "",
      signatureImageData2: "",
      signatureImageData3: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("wards", values.wards);
      console.log(" add form data:-", values.wards.length);
      if (values.name == "" || undefined || null) {
        console.log("name field is empty");
        setValidationFailed(true);
      } else if (values.department == "" || undefined || null) {
        console.log("department is empty");
        setValidationFailed(true);
      } else if (values.position == "" || undefined || null) {
        console.log("position is empty");
        setValidationFailed(true);
      } else if (wardName.length == 0) {
        console.log("ward is empty");
        setValidationFailed(true);
      } else {
        console.log("sig ref", sigCanvasRef.current.toDataURL());

        const binaryImageData = atob(
          sigCanvasRef.current.toDataURL().split(",")[1]
        );
        const binaryImageData2 = atob(
          sigCanvasRef2.current.toDataURL().split(",")[1]
        );
        const binaryImageData3 = atob(
          sigCanvasRef3.current.toDataURL().split(",")[1]
        );

        const imageSizeBytes = binaryImageData.length;
        const imageSizeBytes2 = binaryImageData2.length;
        const imageSizeBytes3 = binaryImageData3.length;

        console.log("signature size", imageSizeBytes);

        if (
          imageSizeBytes <= 2000 ||
          imageSizeBytes2 <= 2000 ||
          imageSizeBytes3 <= 2000
        ) {
          setValidationFailed(true);
        } else if (
          imageSizeBytes >= 2000 ||
          imageSizeBytes2 >= 2000 ||
          imageSizeBytes3 >= 2000
        ) {
          const signatureImageURL = sigCanvasRef.current.toDataURL();
          const signatureImageURL2 = sigCanvasRef2.current.toDataURL();
          const signatureImageURL3 = sigCanvasRef3.current.toDataURL();
          signupInformationRef.current.name = values.name;
          signupInformationRef.current.position =
            values.position !== undefined ? values.position : "empty";
          signupInformationRef.current.department =
            values.department !== undefined ? values.department : "empty";
          signupInformationRef.current.wards = wardName; // Add selected wards to signupInformationRef
          signupInformationRef.current.signatureImageData = signatureImageURL;
          signupInformationRef.current.signatureImageData2 = signatureImageURL2;
          signupInformationRef.current.signatureImageData3 = signatureImageURL3;

          console.log("sign up info:-", signupInformationRef.current);
          postData(signupInformationRef.current, incoming_username_data);
        } else {
          console.log("something is wrong");
          setValidationFailed(true);
        }

        // if (isSignupInformationValid(signupInformationRef)) {
        //   console.log("everything is okay");
        // } else if (!isSignupInformationValid(signupInformationRef)) {
        //   console.log("something is wrong");
        // }

        //postData(signupInformationRef.current, incoming_username_data);

        // setAuthAdd(false);
      }

      // setTimeout(postData(signupInformationRef.current), 3000)

      // sendMessage("facility_data", values);
      // navigate("/facilities");
    },
  });

  const formikEdit = useFormik({
    initialValues: {
      name: "",
      department: "",
      position: "",
      wards: [],
      signatureImageData: "",
    },
    //validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      // console.log("form data for edit:-", values);
      console.log("passed id", searchResult.ID);
      console.log("on submit edit", sigCanvasRefEdit.current.toDataURL());
      const newEdit = sigCanvasRefEdit.current.toDataURL();
      const newEdit2 = sigCanvasRefEdit2.current.toDataURL();
      const newEdit3 = sigCanvasRefEdit3.current.toDataURL();

      const binaryImageData = atob(newEdit.split(",")[1]);
      const binaryImageData2 = atob(newEdit2.split(",")[1]);
      const binaryImageData3 = atob(newEdit3.split(",")[1]);

      const imageSizeBytes = binaryImageData.length;
      const imageSizeBytes2 = binaryImageData2.length;
      const imageSizeBytes3 = binaryImageData3.length;

      console.log("signature size1", imageSizeBytes);
      console.log("signature size2", imageSizeBytes2);
      console.log("signature size3", imageSizeBytes3);

      if (
        imageSizeBytes <= 2000 ||
        imageSizeBytes2 <= 2000 ||
        imageSizeBytes3 <= 2000
      ) {
        console.log("something is wrong");
        setValidationFailed(true);
      } else if (
        imageSizeBytes >= 2000 ||
        imageSizeBytes2 >= 2000 ||
        imageSizeBytes3 >= 2000
      ) {
        handleEditNurse(values, newEdit, newEdit2, newEdit3, searchResult.ID);
      } else {
        setValidationFailed(true);
      }
    },
  });

  //const signatureImageURL = sigCanvasRef.current.toDataURL();

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 6,
    p: 4,
    borderRadius: 2,
  };
  const style_Paper = {
    position: "absolute",
    top: "18%",
    left: "94%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 0,
    p: 4,
    borderRadius: 2,
  };

  const style_picture = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 6,
    p: 4,
    borderRadius: 2,
    overflow: "auto",
    maxHeight: "80vh",
  };
  const style_box = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 6,
    p: 4,
    borderRadius: 2,
    overflowY: "auto",
    maxHeight: "80vh",
  };

  const style_Search = {
    Width: "full",
    // border: "1px solid #ccc",

    // backgroundColor: "white"
  };

  const handleSubmit = (values) => {
    // Handle form submission logic here
    console.log(values);
  };

  function deactivateNurse(id) {
    console.log("deactivated id", id);
    // Replace with your server's endpoint
    const apiUrl = `https://nurse-backend.onrender.com/deactivate-nurse/${id}`;

    // Make a POST request to the server
    return fetch(apiUrl, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        // Handle the response from the server
        console.log(data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }
  const handleDeactivate = () => {
    deactivateNurse(searchResult._id);
    setOpenDeactivateNurseSuccess(true);
    handleClose();
    handlCloseConfirm();
  };

  const handleSearchChange = (event) => {
    setSearchInput(event.target.value);
  };

  function handleSearchModal() {
    console.log("search modal activated...");
    setOpenSearchModal(true);
  }

  function aboutToSearch() {
    //console.log("about to search...", searchType, typeof default_search_name);

    if (!default_search_name) {
      console.log("-->set to true!!!!!");
    }

    if (searchType.length === 0 && !default_search_name) {
      console.log("search type is empty!");
      setOpenWrongPwdError(true);
      setOpenEmptySearchTypeError(true);
    }
  }

  const handleClose = () => {
    setOpenSearchModal(false);
    console.log("search modal deactivated...");
  };

  const handleCloseErrorModal = () => {
    setOpenWrongPwdError(false);
    console.log("search modal deactivated...");
  };

  function handleSearch() {
    console.log("Search submitted..", searchInput);
    getNurseByName(searchInput);

    // setNurseId(nurseData._id)
    // setName(nurseData.name)
  }

  function handleAddNurse() {
    console.log("handle Add nurse....");
    //setAuthAdd(true);
    setAddNurse(true);
  }

  function handleOpenEditModal() {
    console.log("nurseEditData", searchResult);
    formikEdit.setValues({
      id: searchResult._id,
      name: searchResult.name || "", // Set default value to an empty string if nurseData.name is undefined
      department: searchResult.department || "Department", // Set a default department value or adjust as needed
      position: searchResult.position,

      wards: searchResult.wards || [], // Set default wards value to an empty array if nurseData.wards is undefined
      // ... other form fields
    });
    setEditNurse(true);
    setOpenSearchModal(false);
  }

  function handleNavigateLogs() {
    navigate("/logs");
  }
  function handeleSettingsNav() {
    navigate("/settings");
  }
  const handleNavigate = () => {
    navigate("/");
  };

  async function handleOpenDeleteModal(search_res) {
    console.log("nurseDeleteData", search_res);

    try {
      const response = await axios.delete(
        `https://nurse-backend.onrender.com/delete-nurse/${search_res._id}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      // Handle success
      console.log("deleted successfully");
      postLogs(incoming_username_data, `DELETED:${search_res.name}`);
      setTimeout(window.location.reload(), 4000);

      //window.location.reload();
      console.log("should be navigatiing...");
    } catch (error) {
      // Handle error
      // toast({
      // title: "Cannot be deleted",
      // position: "top",
      // isClosable: true,
      // duration: 3000,
      // status: "error",
      // });
    }
    setOpenSuccessAlert(true);
  }

  function handleOpenAuthEdit() {
    // setAuthEdit(true);
    handleOpenEditModal();
  }

  function handleOpenAuthDelete() {
    // setAuthDelete(true);

    handleOpenDeleteModal();
  }

  function handleCloseEditModal() {
    setEditNurse(false);
  }

  function handleCloseDeleteAuth() {
    setAuthDelete(false);
  }

  function handleCloseAddAuth() {
    setAuthAdd(false);
  }

  function handleCloseAddNurse() {
    setAddNurse(false);
  }

  function handleCloseEditAuth() {
    setAuthEdit(false);
  }

  const handleChange_dept = (event) => {
    setDept(event.target.value);
  };

  const handleChange_pos = (event) => {
    setPos(event.target.value);
  };

  function handleSubmit_Edit_form() {}

  function handleCloseFacility() {}

  function handleNavigateNursePage() {
    console.log("nurse clicked");
    navigate("/nurses");
  }

  function handleNavigateSettingsPage() {
    console.log("setings clicked");
    navigate("/nurses");
  }

  const handlePUTrequest = async (nurseId, updated_nurse_info) => {
    console.log("updated nurse info:", updated_nurse_info);
    try {
      const response = await fetch(
        `https://nurse-backend.onrender.com/update-nurse${updated_nurse_info.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: updated_nurse_info.Name,
            department: updated_nurse_info.department,
            wards: updated_nurse_info.wards,
            position: updated_nurse_info.position,
            signatureImageData: signatureImageData,
          }),
        }
      );

      if (response.ok) {
        console.log("Nurse information updated successfully");
        postLogs(incoming_username_data, `edited: ${updated_nurse_info.name}`);
        setOpenSuccessAlert(true);
        // Handle further actions or UI updates
        //console.log("body", signatureImageData);
      } else {
        console.error("Failed to update nurse information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const handlePUTrequest_for_edited_signature = async (
    nurseId,
    new_sig,
    new_sig2,
    new_sig3,
    updated_nurse_info,
    ward
  ) => {
    console.log("new nurse infor", updated_nurse_info);

    if (hasEmptyProperty(updated_nurse_info)) {
      console.log("Object has at least one empty property.");
    } else {
      console.log("Object does not have any empty properties.");

      // console.log("new signature being updated", updated_nurse_info);
      try {
        const response = await fetch(
          `https://nurse-backend.onrender.com/edit-nurse/${updated_nurse_info.id}`,
          //`http://localhost:8080/edit-nurse/${updated_nurse_info.id}`,
          {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              name: updated_nurse_info.name,
              department: updated_nurse_info.department,
              wards: ward,
              status: "ACTIVE",
              position: updated_nurse_info.position,
              signatureImageData_1: new_sig,
              signatureImageData_2: new_sig2,
              signatureImageData_3: new_sig3,
            }),
          }
        );

        if (response.ok) {
          console.log("Nurse information updated successfully");
          postLogs(
            incoming_username_data,
            `edited: ${updated_nurse_info.name}`
          );
          setOpenSuccessAlert(true);

          setEditNurse(false);
          // Handle further actions or UI updates
          //console.log("body", signatureImageData);
        } else {
          console.error("Failed to update nurse information");
        }
      } catch (error) {
        console.error("An error occurred:", error);
      }
    }
  };

  const handleEditNurse = async (values, sig, sig2, sig3, id) => {
    console.log("incoming id", id);
    console.log("edited value passed", values);

    // console.log("image url:", sigCanvasRef )

    // const signatureImageURL = sigCanvasRef.current.toDataURL();
    //console.log("image url", sigCanvasRef )
    //const signatureImageURL2 = sigCanvasRef2.current.toDataURL();
    // const signatureImageURL3 = sigCanvasRef3.current.toDataURL();
    //
    //console.log("signature image uRl:", signatureImageURL);
    const binaryImageData = atob(sig.split(",")[1]);
    const binaryImageData2 = atob(sig2.split(",")[1]);
    const binaryImageData3 = atob(sig3.split(",")[1]);

    const imageSizeBytes = binaryImageData.length;
    // const imageSizeBytes2 = binaryImageData2.length;
    //const imageSizeBytes3 = binaryImageData3.length;

    console.log("signature size:", imageSizeBytes);

    if (
      imageSizeBytes < 3000
      // imageSizeBytes2 < 3000 &&
      // imageSizeBytes3 < 3000
    ) {
      console.log("image size bytes less than 3000");
      //signatureImageURL = signatureImageData
      setSignatureImageData(nurseData.signatureImageData_1);
      handlePUTrequest(nurseId, values);

      //console.log("less than 3000", nurseData.signatureImageData);
    } else if (
      imageSizeBytes > 3000
      // imageSizeBytes2 > 3000 &&
      //imageSizeBytes3 > 3000
    ) {
      console.log("image size bytes greater than 3000");
      //console.log("edited signature of the nurse");
      //setSignatureImageData(toString(signatureImageURL));
      // console.log("type", typeof signatureImageURL);
      handlePUTrequest_for_edited_signature(
        id,
        sig,
        sig2,
        sig3,
        values,
        wardName
      );
    }

    <Alert severity="success" color="info">
      Nurse information edited successfully...
    </Alert>;
  };

  // const handleDelete = async (nurse_data) => {
  //   handleOpenAuthDelete();
  // };

  const [isSideNavOpen, setIsSideNavOpen] = React.useState(false);
  const [openConfirmDelete, setOPenCornfirmDelete] = React.useState(false);
  const handlOpenConfirm = () => {
    console.log("delete clicked!");
    setOPenCornfirmDelete(true);
  };
  const handlCloseConfirm = () => {
    setOPenCornfirmDelete(false);
  };

  const handleToggleSideNav = () => {
    console.log("opened");
    setIsSideNavOpen(!isSideNavOpen);
  };

  return (
    <>
      <BasicNavbar />
    <div>{loading && <LinearProgress />}</div>
      {/* <p>incoming:--{username}</p> */}
      <ErrorSnackbar
        open={openWrongPwdError}
        autoHideDuration={3000}
        onClose={handleCloseErrorModal}
      />
      <ValidationFailed
        open={validationFailed}
        autoHideDuration={5000}
        onClose={() => {
          setValidationFailed(false);
        }}
      />
      <ErrorSnackbar_SearchType
        open={openEmptySearchTypeError}
        autoHideDuration={6000}
        onClose={() => setOpenEmptySearchTypeError(false)}
      />
      <NurseAddSucces
        open={openAddNurseSuccess}
        autoHideDuration={6000}
        onClose={() => setOpenAddNurseSuccess(false)}
      />
      <SuccessSnackbar
        open={openSuccessAlert}
        autoHideDuration={3000}
        onClose={() => setOpenSuccessAlert(false)}
      />
      {searchResult && (
        <Modal
          open={openSearchModal}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style_box}>
            {/* <div
              className="absolute  top-2 right-2 m-2 cursor-pointer hover:text-red-500 duration-200 ease-in"
              onClick={() => setOpenSearchModal(false)}
            >
              <IoCloseSharp />
            </div> */}
            <div className="flex flex-row justify-between">
              <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
                View Nurse Details
              </Typography>

              <Button onClick={handleClose} color="error" variant="contained">
                <CloseIcon fontSize="40" />
              </Button>
            </div>

            <div className="flex flex-col gap-4 m-3">
              <div className="flex flex-row gap-3">
                <TextField
                  label="Name"
                  value={searchResult.name}
                  InputProps={{
                    readOnly: true,
                    style: { fontWeight: "bold", fontSize: "18px" },
                  }}
                />
                <TextField
                  label="Department"
                  value={searchResult.department}
                  InputProps={{
                    readOnly: true,
                  }}
                />
              </div>
              <div className="flex flex-row gap-3">
                <TextField
                  label="Position"
                  value={searchResult.position}
                  InputProps={{
                    readOnly: true,
                  }}
                />
                <TextField
                  label="Wards"
                  value={searchResult.wards}
                  InputProps={{
                    readOnly: true,
                  }}
                />
              </div>
            </div>

            <Box className="border border-1 border-gray-400 p-2  m-4 rounded-md">
              <Typography id="modal-modal-title" sx={{ m: 2 }}>
                Signature 1:
              </Typography>
              <img
                src={searchResult.signatureImageData_1}
                // src={LOGO}
                alt="NOT FOUND"
                style={{ width: "300px", height: "auto" }}
              />
            </Box>

            <Box className="border border-1 border-gray-400 p-2  m-4 rounded-md">
              <Typography id="modal-modal-title" sx={{ m: 2 }}>
                Signature 2:
              </Typography>
              <img
                src={searchResult.signatureImageData_2}
                // src={LOGO}
                alt="NOT FOUND"
                style={{ width: "300px", height: "auto" }}
              />
            </Box>

            <Box className="border border-1 border-gray-400 p-2  m-4 rounded-md">
              <Typography id="modal-modal-title" sx={{ m: 2 }}>
                Signature 3:
              </Typography>
              <img
                src={searchResult.signatureImageData_3}
                // src={LOGO}
                alt="NOT FOUND"
                style={{ width: "300px", height: "auto" }}
              />
            </Box>
            <div className="flex flex-row gap-6 items-center justify-center mt-2">
              {/* <Button variant="contained" onClick={handleOpenEditModal}> */}
           
            </div>
          </Box>
        </Modal>
      )}
      <NurseDeactivate
        open={openDeactivateNurseSuccess}
        autoHideDuration={6000}
        onClose={() => setOpenDeactivateNurseSuccess(false)}
      />
      <Modal
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        open={openConfirmDelete}
      >
        <Box sx={style}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{ fontWeight: "bold" }}
          >
            {/* handlOpenConfirm */}
            Confirm Deactivate
          </Typography>{" "}
          <Box className="flex m-2 gap-4">
            <Button
              type="submit"
              variant="contained"
              color="success"
              // onClick={() => handleOpenDeleteModal(searchResult)}
              onClick={handleDeactivate}
            >
              yes
            </Button>
            <Button
              onClick={handlCloseConfirm}
              variant="contained"
              color="error"
            >
              No
            </Button>
          </Box>
        </Box>
      </Modal>
      <Modal
        open={openEditNurse}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_picture}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{ fontWeight: "bold" }}
          >
            Edit Nurse Details
          </Typography>
          <form onSubmit={formikEdit.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-row gap-3">
                <TextField
                  id="name"
                  name="name"
                  label="Name:"
                  placeholder="Name"
                  maxRows={4}
                  value={formikEdit.values.name}
                  onChange={formikEdit.handleChange}
                  onBlur={formikEdit.handleBlur}
                />
                <TextField
                  id="department"
                  select
                  name="department"
                  label="Department"
                  defaultValue="Department"
                  helperText="Please select Department"
                  value={formikEdit.values.department}
                  onChange={formikEdit.handleChange}
                  onBlur={formikEdit.handleBlur}
                >
                  {departments.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </div>
              <div className="flex flex-row  gap-3">
                <TextField
                  id="position"
                  select
                  name="position"
                  label="Position"
                  defaultValue="Position"
                  helperText="Please select Position"
                  value={formikEdit.values.position}
                  onChange={formikEdit.handleChange}
                  onBlur={formikEdit.handleBlur}
                >
                  {positions.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>

                <FormGroup
                  row
                  // className="border border-gray-400 p-0 mb-6 rounded"
                >
                  <TextField
                    label="Current Stored Wards"
                    value={searchResult.wards}
                    InputProps={{
                      readOnly: true,
                    }}
                  />
                  <MultipleSelectCheckmarks
                    wardName={wardName}
                    onWardNameChange={handleWardNameChange}
                    incoming_wards={ward_options}
                    className="mb-2"
                  />
                </FormGroup>
              </div>

              {/* imgages */}
              <div className="flex flex-col gap-2 ">
                <Typography id="modal-modal-title">
                  Current Signature 1:
                </Typography>
                <div className="border border-gray-600 rounded-md">
                  <img
                    src={searchResult.signatureImageData_1}
                    alt="NOT FOUND"
                  />
                </div>
                <Typography id="modal-modal-title">
                  Current Signature 2:
                </Typography>
                <div className="border border-gray-600 rounded-md">
                  <img
                    src={searchResult.signatureImageData_2}
                    alt="NOT FOUND"
                  />
                </div>
                <Typography id="modal-modal-title">
                  Current Signature 3 :
                </Typography>
                <div className="border border-gray-600 rounded-md">
                  <img
                    src={searchResult.signatureImageData_3}
                    alt="NOT FOUND"
                  />
                </div>

                <Box sx={{ marginTop: 8 }} />
                <Typography id="modal-modal-title">New Signature 1:</Typography>
                <div className="flex flex-col gap-2 mt-2">
                  <div className="border border-gray-600 rounded-md">
                    <ReactSignatureCanvas
                      ref={sigCanvasRefEdit}
                      penColor="blue"
                      canvasProps={{
                        className: "w-full h-auto",
                      }}
                    />
                  </div>
                  <Typography id="modal-modal-title">
                    New Signature 2:
                  </Typography>
                  <div className="border border-gray-600 rounded-md">
                    <ReactSignatureCanvas
                      ref={sigCanvasRefEdit2}
                      penColor="blue"
                      canvasProps={{
                        className: "w-full h-auto",
                      }}
                    />
                  </div>
                  <Typography id="modal-modal-title">
                    New Signature 3:
                  </Typography>
                  <div className="border border-gray-600 rounded-md">
                    <ReactSignatureCanvas
                      ref={sigCanvasRefEdit3}
                      penColor="blue"
                      canvasProps={{
                        className: "w-full h-auto",
                      }}
                    />
                  </div>
                </div>
              </div>

              {/* end of image */}
              <div>
                <Button
                  onClick={clearSignature}
                  variant="outlined"
                  color="error"
                >
                  Clear Signature
                </Button>
              </div>

              <Box className="flex m-2 gap-4">
                <Button type="submit" variant="contained" color="success">
                  Save
                </Button>
                <Button
                  onClick={handleCloseEditModal}
                  variant="contained"
                  color="error"
                >
                  Close
                </Button>
              </Box>
            </div>
          </form>
        </Box>
      </Modal>
      {/* Add new Nurse Modal */}
      <Modal
        open={openaddNurseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style_box}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add New Nurse
          </Typography>

          <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4 m-4">
              <div className="flex flex-row gap-3">
                <TextField
                  id="name"
                  name="name"
                  label="Name:"
                  placeholder="Name"
                  maxRows={4}
                  size="normal"
                  fullWidth
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.name && Boolean(formik.errors.name)}
                  helperText={formik.touched.name && formik.errors.name}
                />

                <TextField
                  id="department"
                  select
                  name="department"
                  label="Department"
                  defaultValue="Chief"
                  size="normal"
                  fullWidth
                  value={formik.values.department}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.department &&
                    Boolean(formik.errors.department)
                  }
                >
                  {departments.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </div>
              <div className="">
                <TextField
                  id="position"
                  select
                  name="position"
                  label="Position"
                  size="normal"
                  fullWidth
                  value={formik.values.position}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  sx={{ marginTop: "8px" }}
                  error={
                    formik.touched.position && Boolean(formik.errors.position)
                  }
                >
                  {positions.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                <div className="flex flex-row mt-4">
                  <MultipleSelectCheckmarks
                    wardName={wardName}
                    onWardNameChange={handleWardNameChange}
                    incoming_wards={ward_options}
                  />
                  {wardName.length}
                </div>
              </div>
              <Typography>Signature 1:</Typography>
              <div className="border border-gray-600 rounded-md">
                <ReactSignatureCanvas
                  ref={sigCanvasRef}
                  penColor="blue"
                  canvasProps={{
                    className: "w-full h-auto",
                  }}
                />
              </div>
              <Typography>Signature 2:</Typography>
              <div className="border border-gray-600 rounded-md">
                <ReactSignatureCanvas
                  ref={sigCanvasRef2}
                  penColor="blue"
                  canvasProps={{
                    className: "w-full h-auto",
                  }}
                />
              </div>
              <Typography>Signature 3:</Typography>
              <div className="border border-gray-600 rounded-md">
                <ReactSignatureCanvas
                  ref={sigCanvasRef3}
                  penColor="blue"
                  canvasProps={{
                    className: "w-full h-auto",
                  }}
                />
              </div>
            </div>

            <div>
              <Button onClick={clearSignature} variant="outlined" color="error">
                Clear Signature
              </Button>
            </div>

            <div className="flex m-2 gap-4">
              <Button variant="contained" color="success" type="submit">
                Save
              </Button>
              <Button
                onClick={handleCloseAddNurse}
                variant="contained"
                color="error"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal open={openAccountInfo} onClose={handleCloseAccountInfo}>
        <Box sx={style_Paper}>
          <div className=" cursor-pointer ">
            <AccountCircleIcon />
            {incoming_username_data}
            <div
              className="flex flex-row gap-4 cursor-pointer border mt-3"
              style={text}
            >
              <LoginIcon />
              logout
            </div>
          </div>
          {/* <div className="flex flex-row gap-4 cursor-pointer border hover:bg-blue-500">
            <span className="text-gray-600">
              <LoginIcon size={20} />
            </span>
            Add User
          </div> */}
        </Box>
      </Modal>
      {/* <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <div className="mr-6">
              <IconButton
                size="large"
                edge="end"
                aria-label="account of current user"
                aria-haspopup="true"
                onClick={handleToggleSideNav}
                color="inherit"
              >
                <LightTooltip title="menu" placement="bottom">
                  <ListIcon className="cursor-pointer" />
                </LightTooltip>
              </IconButton>
            </div>

            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}
            >
              Nurses CD Signature System
            </Typography>

            <div
              onClick={handleNavigateNursePage}
              className="border border-spacing-2  hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-400"
            >
              Nurses
            </div>

            <div
              onClick={handleNavigateSettingsPage}
              className="border border-spacing-2  hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-400"
            >
              Settings
            </div>

            <Box sx={{ flexGrow: 1 }} />
            <Box sx={{ display: { xs: "none", md: "flex" } }}>
              <IconButton
                size="large"
                aria-label="show 4 new mails"
                color="inherit"
              >
                <LightTooltip title="notifications" placement="bottom">
                  <Badge badgeContent={14} color="error">
                    <NotificationsIcon />
                  </Badge>
                </LightTooltip>
              </IconButton>

              <LightTooltip title="Account" placement="bottom">
                <div
                  className="cursor-pointer mt-3 ml-8"
                  onClick={handleOpenAccountInfo}
                >
                  <AccountCircleIcon /> {incoming_username_data}
                </div>
              </LightTooltip>
            </Box>
            <Box sx={{ display: { xs: "flex", md: "none" } }}>
              <IconButton onClick={handleToggleSideNav}>
                <LightTooltip title="more" placement="bottom">
                  <MoreIcon />
                </LightTooltip>
              </IconButton>
            </Box>
          </Toolbar>
        </AppBar>

        {/* Your side nav component */}
      {/* {isSideNavOpen && ( */}
      {/* <Sidenav
            handleAddNurse={handleAddNurse}
            user_data={incoming_usr_data}
          /> */}
      {/* )}  */}
      {/* </Box> */}
      <Box className="flex justify-center text-center w-full mt-[12%]">
        <img
          src={LOGO2}
          alt="NOT FOUND"
          className="w-[200px] rounded-full shadow-xl"
        />
      </Box>
      <div className="mt-12">
        <div className="flex text-center justify-center ">
          <Box className="flex flex-row bg-gray-100 w-[550px]  rounded-lg shadow-xl gap-[300px]">
            <Search>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase
                //onClick={handleSearchModal}
                onClick={aboutToSearch}
                //placeholder="Search…"
                placeholder={searchType || "Search Name..."}
                inputProps={{ "aria-label": "search" }}
                value={input}
                onChange={(e) => handleChange(e.target.value)}
              />
            </Search>
            {/* 
            <Button variant="contained" onClick={handleSearchModal}>
              Search
            </Button> */}
          </Box>
        </div>

        {/* <div className="border rounded-md shadow-lg ml-[650px] w-[550px] "> */}
        <div
          className="border rounded-md shadow-lg mx-auto max-w-[350px] md:max-w-[400px] lg:max-w-[550px]"
          style={scroll}
        >
          {results.map((result, id) => {
            // return <SearchResult result={result.name} key={id} />;
            return (
              <div
                className="flex flex-row gap-2 hover:bg-blue-400 p-2 cursor-pointer"
                key={id}
                onClick={() => handleSearchResultItem(result)}
              >
                {result.name}
              </div>
            );
          })}
        </div>

        <div className="flex text-center justify-center">
          <div className="flex flex-col">
            <div className="flex flex-row gap-2">
              {/* <p>Search By:</p>
            <p>Nurse Name</p> */}

              <FormControl>
                {/* <FormLabel id="demo-row-radio-buttons-group-label">
                Gender
              </FormLabel> */}
                <RadioGroup
                  row
                  aria-labelledby="demo-row-radio-buttons-group-label"
                  name="row-radio-buttons-group"
                >
                  <LightTooltip title="search nurse by name" placement="bottom">
                    <FormControlLabel
                      value="Search Name"
                      checked={default_search_name}
                      control={<Radio />}
                      label="Search By Name"
                      onChange={handleRadioChange}
                      //    onChange={setSearch_by_name(true)}
                      style={{ color: "#1976D2", fontWeight: "bold" }}
                    />
                  </LightTooltip>
                  <LightTooltip title="search nurse by ward" placement="bottom">
                    <FormControlLabel
                      value="Search Ward"
                      control={<Radio />}
                      label="Search By Ward"
                      onChange={handleRadioChange}
                      //   onChange={setSearch_by_ward(true)}
                      style={{ color: "#1976D2", fontWeight: "bold" }}
                    />
                  </LightTooltip>
                </RadioGroup>
              </FormControl>
            </div>
            <div className="mt-8">
              {/* <Button
                onClick={handleAddNurse}
                variant="contained"
                color="success"
              >
                Add Nurse
              </Button> */}
            </div>
          </div>
        </div>
        {renderMobileMenu}
        {renderMenu}
      </div>
      {/* {results && results.length > 0 && <SearchResultsList results={results} />} */}
    </>
  );
}
const text = {
  fontSize: "14px",
  padding: "5px",
  borderRadius: "6px",
};
const scroll = {
  overflowY: " auto",
  maxHeight: "40vh",
};
const text_field = {
  fontWeight: "bold",
};
