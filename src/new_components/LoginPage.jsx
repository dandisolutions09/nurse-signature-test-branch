import { AccountCircle } from "@mui/icons-material";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import IconButton from "@mui/material/IconButton";
import React, { useContext, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { useNavigate } from "react-router-dom";
import LOGO from "../assets/logo56.png";
import SuccessSnackbar from "./SuccessSnackbar";
import ErrorSnackbar from "./ErrorSnackbar";
import { postLogs } from "./PostLogFunc";
import axios from "axios";

import { Button, LinearProgress } from "@mui/material";
import { AppContext } from "./Pages/MyContext";
import UserInactiveError from "../components/UserInactiveError";
// import MyContext from "./Pages/MyContext";

const validationSchema = Yup.object({
  username: Yup.string().required("username is required"),
  password: Yup.string().required("password is required"),
});

export default function LoginPage() {
  const navigate = useNavigate();
  //  const { text, setText } = useContext(MyContext);
  //const { username, login } = useContext(AppContext);
  const { setUserData } = useContext(AppContext);
  const [loading, setLoading] = useState(false);

  //const { setUsername } = useContext(AppContext);
  // const [inputUsername, setInputUsername] = useState('DON CONTEXT');

  // setUserData(userData);

  const handleNavigate_AdminPage = (passed_username, role) => {
    console.log("Connecting...");
    console.log("username", passed_username, role);

    const userData = {
      username: passed_username,
      role: role,
      // Add other properties as needed
    };
    setUserData(userData);

    const userDataJSON = JSON.stringify(userData);

    localStorage.setItem("userName_storage", userDataJSON);

    // setText("mycontext username");

    const usr_data = { usr_name: passed_username, usr_role: role };
    // login({ username: 'exampleUser' });
    navigate("/home", { state: usr_data });

    // Introduce a delay using setTimeout (e.g., 2000 milliseconds or 2 seconds)
    setTimeout(() => {
      console.log("Connected!!");
      // Navigate to the "/home" route after the delay
    }, 2000);
    formik.resetForm();
  };

  const handleNavigate_IntermediatePage = (passed_username, role) => {
    console.log("Connecting...");
    const usr_data = { usr_name: passed_username, usr_role: role };

    const userData = {
      username: passed_username,
      role: role,
      // Add other properties as needed
    };
    setUserData(userData);

    const userDataJSON = JSON.stringify(userData);

    localStorage.setItem("userName_storage", userDataJSON);

    // Introduce a delay using setTimeout (e.g., 2000 milliseconds or 2 seconds)
    setTimeout(() => {
      console.log("Connected!!");
      // Navigate to the "/home" route after the delay
      navigate("/inter", { state: usr_data });
    }, 2000);
    formik.resetForm();
  };
  const handleNavigate_BasicPage = (passed_username, role) => {
    console.log("Connecting...");
    const usr_data = { usr_name: passed_username, usr_role: role };
    const userData = {
      username: passed_username,
      role: role,
      // Add other properties as needed
    };
    setUserData(userData);

    const userDataJSON = JSON.stringify(userData);

    localStorage.setItem("userName_storage", userDataJSON);

    // Introduce a delay using setTimeout (e.g., 2000 milliseconds or 2 seconds)
    setTimeout(() => {
      console.log("Connected!!");
      // Navigate to the "/home" route after the delay
      navigate("/basic", { state: usr_data });
    }, 2000);
    formik.resetForm();
  };

 

  const postLoginCreds = async (usrname, pwd) => {
    console.log("data to be sent", usrname, pwd);

    const LoginData = {
      username: usrname,
      password: pwd,
    };

    try {
      setLoading(true); // Set loading to true when making the request
      const response = await axios.post(
        "https://nurse-backend.onrender.com/login",
        LoginData
      );

      console.log("Response from the backend:", response.data);

      var arrayResult = response.data.split(",");

      if (
        arrayResult[0] === "Success" &&
        arrayResult[1] === "Admin" &&
        arrayResult[2] === "Active"
      ) {
        postLogs(LoginData.username, "admin", "LOGGED IN");
        console.log("success!!!!, admin");
        setOpenSuccessAlert(true);
        handleNavigate_AdminPage(LoginData.username, "admin");
      } else if (
        arrayResult[0] === "Success" &&
        arrayResult[1] === "Intermediate" &&
        arrayResult[2] === "Active"
      ) {
        postLogs(LoginData.username, "intermediate", "LOGGED IN");
        setOpenSuccessAlert(true);
        console.log("success!!!!, intermediate");
        handleNavigate_IntermediatePage(LoginData.username, "intermediate");
      } else if (
        arrayResult[0] === "Success" &&
        arrayResult[1] === "Basic" &&
        arrayResult[2] === "Active"
      ) {
        postLogs(LoginData.username, "basic", "LOGGED IN");
        setOpenSuccessAlert(true);
        console.log("success!!!!, basic");
        handleNavigate_BasicPage(LoginData.username, "intermediate");
      } else if (response.data === "AuthFailed") {
        postLogs(LoginData.username, "", "LOGIN FAILED");
        console.log("Password incorrect!!!!");
        setOpenWrongPwdError(true);
      }else if(arrayResult[2] === "Inactive"){
        console.log("current user is not active")
        setOpenInactiveError(true);
      }
    } catch (error) {
      console.error("Error sending data to the backend:", error);
    } finally {
      setLoading(false); // Set loading to false after the request is complete (success or error)
    }
  };

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log("form data:-", values);

      postLoginCreds(values.username, values.password);
    },
  });

  const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);
  const [showPassword, setShowPassword] = React.useState(false);
  const [openWrongPwdError, setOpenWrongPwdError] = React.useState(false);
  const [openInactiveError, setOpenInactiveError] = React.useState(false)

  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handleCloseErrorModal = () => {
    setOpenWrongPwdError(false);
    console.log("error modal...");
  };
  

  return (
    <>
      <div>
        {loading && <LinearProgress />}

        {/* Your existing component content */}
      </div>
      <UserInactiveError
        open={openInactiveError}
        autoHideDuration={3000}
        onClose={() => setOpenInactiveError(false)}
      />
      <SuccessSnackbar
        open={openSuccessAlert}
        autoHideDuration={6000}
        onClose={() => setOpenSuccessAlert(false)}
      />
      <ErrorSnackbar
        open={openWrongPwdError}
        autoHideDuration={3000}
        onClose={handleCloseErrorModal}
      />
      <div style={myComponentStyle} className="shadow-xl rounded-lg border ">
        <img src={LOGO} alt="componay_logo" style={company_logo} />

        <div className="header" style={Header}>
          <div style={text}>Lyteware</div>
          <div className="underline" style={underline}></div>
        </div>
        <form onSubmit={formik.handleSubmit}>
          <div className="inputs" style={inputs}>
            <div className="input" style={input}>
              <TextField
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="start">
                      <AccountCircle />
                    </InputAdornment>
                  ),
                }}
                type="text"
                style={inputfield}
                placeholder="username"
                name="username"
                value={formik.values.username}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={
                  formik.touched.username && Boolean(formik.errors.username)
                }
                helperText={formik.touched.username && formik.errors.username}
              />
            </div>
            <div className="input" style={input}>
              <TextField
                type={showPassword ? "text" : "password"}
                name="password"
                style={inputfield}
                placeholder="password"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                autoComplete="none"
                error={
                  formik.touched.password && Boolean(formik.errors.password)
                }
                helperText={formik.touched.password && formik.errors.password}
              />
            </div>
          </div>
          <div className="forgot-password " style={forgot_password}>
            Input your login details
          </div>
          <div style={submit_container}>
            <button
              type="submit"
              style={submit}
              className="bg-blue-500 text-white"
            >
              Login
            </button>
          </div>
        </form>
      </div>
    </>
  );
}

const myComponentStyle = {
  background: "",
  height: "700px",
  alignItems: "center",
  display: "flex",
  padding: "10px",
  margin: "auto",
  marginTop: "150px",
  width: "600px",
  flexDirection: "column",
  padingBottom: "20px",
};
const Header = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  gap: "9px",
  width: "100%",
  marginTop: "30px",
};

const text = {
  color: "#1e90ff",

  fontSize: "48px",
  fontWeight: "700",
};
const underline = {
  width: "61px",
  height: "6px",
  background: "#1e90ff",
  borderRadius: "9px",
};
const inputs = {
  marginTop: "55px",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  flexDirection: "column",
  gap: "25px",
};
const input = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  width: "480px",
  height: "80px",
};
const icons = {
  marginRight: "30px",
};
const inputfield = {
  height: "50px",
  width: "400px",
  background: "transparent",
  border: "none",
  outline: "none",
  color: "#797979",
  fontSize: "19px",
};
const forgot_password = {
  paddingLeft: "70px",
  marginTop: "27px",
  color: "#797979",
  fontSize: "15px",
};
const spn = {
  color: "#4c00b4",
};
const submit_container = {
  // display: "flex",
  // gap: "30px",
  margin: "60px 130px",
};
const submit = {
  display: "flex",
  background: "rgb(59 130 246)",
  justifyContent: "center",
  alignItems: "center",
  width: "220px",
  height: "59px",
  color: "#fff",

  borderRadius: "50px",
  fontSize: "19px",
  fontWeight: "700",
  cursor: "pointer",
};
const company_logo = {
  width: "100px",
  height: "100px",
  marginTop: "20px",

  borderRadius: 50,
  // padingBottom: "30px",
  // boxShadow: "0px 0px 10px 0px rgba(0, 0, 0, 0.1)",
};
