import * as React from "react";
import { styled } from "@mui/material/styles";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import NativeSelect from "@mui/material/NativeSelect";
import InputBase from "@mui/material/InputBase";
import Button from "@mui/material/Button";
import { TextField, Typography } from "@mui/material";
import Checkbox from "@mui/material/Checkbox";
import Tooltip from "@mui/material/Tooltip";
import FormControlLabel from "@mui/material/FormControlLabel";
import TableCell from "@mui/material/TableCell";

const BootstrapInput = styled(InputBase)(({ theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
}));
const CustomCheckbox = styled(Checkbox)(({ theme }) => ({
  color: theme.palette.primary.main,
  "&.Mui-checked": {
    color: theme.palette.primary.dark,
  },
}));

export default function Textfield() {
  const [checked, setChecked] = React.useState({});
  const [newOption, setNewOption] = React.useState("");
  const [age, setAge] = React.useState("");
  const [agePos, setAgePos] = React.useState("");
  const [options, setOptions] = React.useState([]);

  const handleChange2 = (option) => (event) => {
    setChecked({ ...checked, [option]: event.target.checked });
  };
  const handleAddOption = () => {
    if (newOption.trim() !== "" && !checked[newOption]) {
      setChecked({ ...checked, [newOption]: false });
      setNewOption("");
    }
  };

  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const handleAddOptionDep = () => {
    if (age && !options.includes(age)) {
      setOptions([...options, age]);
      setAge("");
    }
  };
  const handleAddOptionPos = () => {
    if (agePos && !options.includes(agePos)) {
      setOptions([...options, agePos]);
      setAgePos("");
    }
  };
  const handleRemoveOptionDep = () => {
    if (age && options.includes(age)) {
      setOptions(options.filter((option) => option !== age));
      setAge("");
    }
  };
  const handleRemoveOptionPos = () => {
    if (age && options.includes(age)) {
      setOptions(options.filter((option) => option !== age));
      setAge("");
    }
  };
  const handleRemoveOption2 = (option) => {
    const updatedChecked = { ...checked };
    delete updatedChecked[option];
    setChecked(updatedChecked);
  };

  return (
    <div className="flex flex-col gap-4">
      <div className="border border-gray-400 rounded-md p-6">
        <FormControl sx={{ m: 1 }} variant="standard">
          <Typography id="modal-modal-title" sx={{ mb: 1 }}>
            Department fields management
          </Typography>
          <BootstrapInput
            id="demo-customized-textbox"
            value={age}
            placeholder="Input Department"
            label="de"
            onChange={handleChange}
          />
          <div className="flex flex-row gap-3 mt-2">
            <div className="flex gap-3">
              <Button variant="contained" onClick={handleAddOptionDep}>
                Add Dep
              </Button>
              <Button variant="contained" onClick={handleRemoveOptionDep}>
                Remove dep
              </Button>
            </div>
            <div>
              <Button variant="contained">SUBMIT</Button>
            </div>
          </div>
        </FormControl>

        <FormControl sx={{ m: 1, mt: 2 }} variant="standard">
          <InputLabel htmlFor="demo-customized-select-native">
            Available
          </InputLabel>
          <NativeSelect
            id="demo-customized-select-native"
            value={age}
            onChange={handleChange}
            input={<BootstrapInput />}
          >
            {options.map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </NativeSelect>
        </FormControl>
      </div>
      <div className="border border-gray-400 rounded-md p-6">
        <FormControl sx={{ m: 1 }} variant="standard">
          <Typography id="modal-modal-title" sx={{ mb: 1 }}>
            Position fields management
          </Typography>
          <BootstrapInput
            id="demo-customized-textbox"
            value={age}
            placeholder="Input Position"
            label="de"
            onChange={handleChange}
          />

          <div className="flex flex-row gap-3 mt-2">
            <div className="flex gap-3">
              <Button variant="contained" onClick={handleAddOptionPos}>
                Add Pos
              </Button>
              <Button variant="contained" onClick={handleRemoveOptionPos}>
                Remove Pos
              </Button>
            </div>
            <div>
              <Button variant="contained">SUBMIT</Button>
            </div>
          </div>
        </FormControl>
        <FormControl sx={{ m: 1, mt: 2 }} variant="standard">
          <InputLabel htmlFor="demo-customized-select-native">
            Available
          </InputLabel>
          <NativeSelect
            id="demo-customized-select-native"
            value={age}
            onChange={handleChange}
            input={<BootstrapInput />}
          >
            {options.map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </NativeSelect>
        </FormControl>
      </div>
      <div className="border border-gray-400 rounded-md p-6">
        <FormControl sx={{ m: 1 }} variant="standard">
          <Typography id="modal-modal-title" sx={{ mb: 1 }}>
            Ward fields management
          </Typography>
          <BootstrapInput
            id="demo-customized-textbox"
            value={age}
            placeholder="Input Ward"
            label="de"
            onChange={handleChange}
            
          />

          <div className="flex flex-row gap-3 mt-2">
            <div className="flex gap-3">
              <Button variant="contained" onClick={handleAddOptionPos}>
                Add Pos
              </Button>
              <Button variant="contained" onClick={handleRemoveOptionPos}>
                Remove Pos
              </Button>
            </div>
            <div>
              <Button variant="contained">SUBMIT</Button>
            </div>
          </div>
        </FormControl>
        <FormControl sx={{ m: 1, mt: 2 }} variant="standard">
          <InputLabel htmlFor="demo-customized-select-native">
            Available
          </InputLabel>
          <NativeSelect
            id="demo-customized-select-native"
            value={age}
            onChange={handleChange}
            input={<BootstrapInput />}
          >
            {options.map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </NativeSelect>
        </FormControl>
      </div>
    </div>
  );
}
