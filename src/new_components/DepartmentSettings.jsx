import SettingsNav from "./SettingsNav";
import TestingTable from "./TestingTable";
import DepartmentTable from "./DepartmentTable";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import React from "react";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import ListIcon from "@mui/icons-material/List";
import { NavLink, useLocation } from "react-router-dom";



const DepartmentSettings = ({ user_data }) => {
  const [isSideNavOpen, setIsSideNavOpen] = React.useState(false);
   const location = useLocation();
  const incoming_usr_data = location.state;
  const handleToggleSideNav = () => {
    console.log("opened");
    setIsSideNavOpen(!isSideNavOpen);
  };
  return (
    <>
      <div style={header}>
        <div>
          {isSideNavOpen && <SettingsNav usr_data={incoming_usr_data} />}
        </div>

        <div>
          <div
            className="flex justify-between shadow-xl"
            style={{ height: "50px" }}
          >
            <div className="flex flex-row gap-2 ">
              <p className="cursor-pointer text-[#000] underline decoration-sky-600 m-4">
                <ListIcon
                  onClick={handleToggleSideNav}
                  sx={{ fontSize: "30px" }}
                />
              </p>
            </div>
            <div className="flex flex-row gap-2 text-center justify-center m-4">
              <NotificationsNoneIcon />
              <button variant="contained">Add+</button>
            </div>
          </div>
          <TestingTable />
        </div>
      </div>
    </>
  );
};
const header = {
  display: "flex",
  flexDirection: "row",
};


export default DepartmentSettings;
