import React, { useState } from 'react'
import Textfield from './Textfield'
import { Dropdown } from "antd";
import DescriptionOutlinedIcon from "@mui/icons-material/DescriptionOutlined";
import CallSplitOutlinedIcon from "@mui/icons-material/CallSplitOutlined";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import { Person } from "@mui/icons-material";
import { AppBar, Box, Button, IconButton, InputAdornment, MenuItem, Modal, TextField, Toolbar, Typography } from '@mui/material';
import { NavLink, useLocation } from 'react-router-dom';
import SettingsNav from './SettingsNav';
import TestingTable from "./TestingTable";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import ListIcon from "@mui/icons-material/List";
import CloseIcon from "@mui/icons-material/Close";
import * as Yup from "yup";
import { useFormik } from "formik";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import axios from 'axios';


const validationSchema = Yup.object({
  name: Yup.string().required("name is required"),
  password: Yup.string().required("password is required"),
  role: Yup.string().required("role is required"),
});

export default function SettingsPage({ user_data }) {
  const [openAddUser, setOpenAddUser] = useState(false);
  const location = useLocation();
   const handleCloseAddUser = () => {
    //  formik.resetForm();
     setOpenAddUser(false);
   };
     const handleOpenAddUser = () => {
       setOpenAddUser(true);
     };
  

   const role = [
     {
       value: "Admin",
       label: "Admin",
     },
     {
       value: "Basic",
       label: "Intermediate",
     },
     {
       value: "Basic",
       label: "Basic",
     },
   ];

  const incoming_usr_data = location.state;

  const items = [
    {
      key: "1",
      label: "view info",
      icon: <ShareOutlinedIcon />,
    },
    {
      key: "2",
      label: "send invoice",
      icon: <BorderColorOutlinedIcon />,
    },
    {
      key: "3",
      label: "send invoice",
      icon: <DescriptionOutlinedIcon />,
    },
    {
      key: "4",
      label: "Split load",
      icon: <CallSplitOutlinedIcon />,
    },
    {
      key: "5",
      label: "Email load",
      icon: <MailOutlineIcon />,
    },
    {
      key: "6",
      label: "copy",
      icon: <Person />,
    },
    {
      key: "7",
      label: "Delete load",
      icon: <DeleteOutlineIcon />,
    },
  ];
    const [isSideNavOpen, setIsSideNavOpen] = React.useState(false);
    const [showPassword, setShowPassword] = React.useState(false);

    const handleToggleSideNav = () => {
      console.log("opened");
      setIsSideNavOpen(!isSideNavOpen);
    };
     const style = {
       position: "absolute",
       top: "50%",
       left: "50%",
       transform: "translate(-50%, -50%)",
       bgcolor: "background.paper",
       boxShadow: 6,
       p: 4,
       borderRadius: 2,
       // overflowY: "auto",
       // height: "80vh",
     };
      const postData = async (data) => {
        console.log("user to be registered", data);

        const user_json = {
          username: data.name,
          password: data.password,
          status: "active",
          role: data.role,
        };

        console.log("user json", user_json);
        try {
          const response = await axios.post(
            // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
            // "https://nurse-backend.onrender.com/add-nurse",
            "https://nurse-backend.onrender.com/register",
            user_json
          );

          // Handle the response from the backend if needed
          console.log("Response from the backend:", response.data);
          //  setOpenSuccessAlert(true);
        } catch (error) {
          // Handle any errors that occur during the request
          console.error("Error sending data to the backend:", error);
        }
        // alert("nurse saved successfully...");

        handleCloseAddUser();
      };

      const formik = useFormik({
        initialValues: {
          name: "",
          password: "",
          role: "",
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
          // Handle form submission here
          console.log("form data:-", values);

          postData(values);
        },
      });
        const handleClickShowPassword = () => setShowPassword((show) => !show);
        const handleMouseDownPassword = (event) => {
          event.preventDefault();
        };
  return (
    <>
      <Modal
        open={openAddUser}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="flex flex-row justify-between">
            <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add New User
            </Typography>

            <Button
              onClick={handleCloseAddUser}
              color="error"
              variant="contained"
            >
              <CloseIcon fontSize="40" />
            </Button>
          </div>
          <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <TextField
                  type="text"
                  id="name"
                  name="name"
                  label="name"
                  helperText="Please enter full name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.name}
                />

                <TextField
                  id="role"
                  select
                  label="Role"
                  name="role"
                  value={formik.values.role}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  helperText="Please select role"
                >
                  {role.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  type={showPassword ? "text" : "password"}
                  id="password"
                  name="password"
                  label="password:"
                  placeholder="password"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  maxRows={4}
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  helperText={formik.touched.password && formik.errors.password}
                />
              </div>
            </div>
            <div className="flex m-2 gap-4">
              <Button type="submit" variant="contained" color="success">
                Submit
              </Button>
              <Button
                onClick={handleCloseAddUser}
                variant="contained"
                color="error"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <div style={header}>
        <div>
          {isSideNavOpen && <SettingsNav usr_data={incoming_usr_data} />}
        </div>

        <div style={{ flex: 1, display: "flex", flexDirection: "column" }}>
          <div
            className="flex justify-between shadow-xl"
            style={{ height: "50px", backgroundColor: "#2196f3" }}
          >
            <div className="flex flex-row gap-2 ">
              <p className="cursor-pointer text-[#000] underline decoration-sky-600 p- m-4">
                <ListIcon
                  onClick={handleToggleSideNav}
                  sx={{ fontSize: "30px", color:"white" }}
                />
              </p>
            </div>
            <div className="flex flex-row gap-2 text-center justify-center m-2">
              <NotificationsNoneIcon sx={{color:"white"}}/>
              <Button variant="contained" onClick={handleOpenAddUser}>
                Add+
              </Button>
            </div>
          </div>
          <div className="m-4">
            <p className="text-gray-400" style={text2}>
              View Users and manage users and their roles{" "}
            </p>
          </div>

          <div style={{ flex: 1, overflow: "auto" }}>
            <TestingTable />
          </div>
        </div>
      </div>
    </>
  );
}
const header = {
  display: "flex",
  // minHeight:"90%"
};
const text1 = {
  background: "#2196f3",
  padding: "2px",
  color:"white",
  fontSize: "14px",
  borderRadius: "4px",
};
const text2 = {
  fontSize: "16px",
  padding: "2px",
  fontWight:"normal",
  color:"#333",
  borderRadius: "6px",
};
const ret = {
  background:"red"
};