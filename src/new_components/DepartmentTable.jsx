import * as React from "react";
import { useEffect, useState } from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import axios from 'axios';
import CloseIcon from "@mui/icons-material/Close";
import { Dropdown, Typography } from "antd";
import DescriptionOutlinedIcon from "@mui/icons-material/DescriptionOutlined";
import CallSplitOutlinedIcon from "@mui/icons-material/CallSplitOutlined";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";
import { useNavigate } from "react-router-dom";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import { Box, Button, MenuItem, Modal, TextField } from "@mui/material";
import { useFormik } from "formik";
import * as Yup from "yup";



const validationSchema = Yup.object({
  first_name: Yup.string(),
  phone: Yup.string(),
  last_name: Yup.string(),
  dob: Yup.string(),
  email: Yup.string(),
  city: Yup.string(),
  state: Yup.string(),
  zip: Yup.string(),
  license_no: Yup.string(),

});

export default function DepartmentTable() {
  //   const { data, loading, error } = useGetData(urls.get_drivers);
  // useEffect(() => {
  //   console.log("DATA...", data);
  // console.log("URL", urls.get_drivers)
  //   setRowsD(data);
  // }, [data]);
   const [rowsD, setRowsD] = useState([]);

  const items = [
    {
      key: "1",
      label: "View driver",
      icon: <DescriptionOutlinedIcon />,
      // onClick: () => handleRowDoubleClick(),
    },
    {
      key: "2",
      label: "Edit driver",
      icon: <BorderColorOutlinedIcon />,
      // onClick: () => handleOpenFicility(rowT),
    },
    {
      key: "3",
      label: "Email driver",
      icon: <MailOutlineIcon />,
    },
    {
      key: "4",
      label: "Delete driver",
      icon: <DeleteOutlineIcon color="danger" />,
      onClick: () => setOpenDeleteModal(true),
    },
  ];

  const jesse = [
    {
      id: 1,
      first_name: "Jesse",
      phone: "+893 4378673",
      last_name: "Otieno",
      dob: "20/12/1980",
      email: "informjesse@gmail.com",
      city: "Chicago",
      state: "NYC",
      zip: "4353",
      lincense_no: "43368321",
      category: "Owner",
    },
    {
      id: 2,
      first_name: "Don",
      phone: "+113 4378673",
      last_name: "Collins",
      dob: "20/12/1980",
      email: "informjesse@gmail.com",
      city: "Lansece",
      state: "YC",
      zip: "2253",
      license_no: "56368321",
      category: "Company",
    },
    {
      id: 3,
      first_name: "Francis",
      phone: "+893 4378673",
      last_name: "Otieno",
      dob: "20/12/1980",
      email: "informjesse@gmail.com",
      city: "Chicago",
      state: "NYC",
      zip: "4353",
      license_no: "43368321",
      category: "owner",
    },
    {
      id: 4,
      first_name: "Kennedy",
      phone: "+433 2368673",
      last_name: "Branco",
      dob: "20/12/1980",
      email: "informjesse@gmail.com",
      city: "Chicago",
      state: "NYC",
      zip: "4353",
      lincense_no: "43368321",
      category: "owner",
    },
    {
      id: 5,
      first_name: "Amos",
      phone: "+345 65267",
      last_name: "Adem",
      dob: "20/12/1980",
      email: "informjesse@gmail.com",
      city: "Nevada",
      state: "MW",
      zip: "4353",
      lincense_no: "43368321",
      category: "owner",
    },
  ];
  const rates = [
    {
      value: "per mile",
      label: "m/hr",
    },
    {
      value: "percentage",
      label: "%",
    },
  ];
  const Ration = [
    {
      value: "0.750",
      label: "0.750",
    },
    {
      value: "0.003",
      label: "0.003",
    },
    {
      value: "0.055",
      label: "0.055",
    },
  ];
  const driver_category = [
    {
      value: "Owner Operator",
      label: "Owner Operator",
      color: "#FF5733",
    },
    {
      value: "Company Driver",
      label: "Company Driver",
      color: "#3399FF",
    },
  ];
   const [rows, setRows] = useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handleRowDoubleClick = () => {
    // Navigate to another page on double-click
    navigate("/viewdriver");
  };
  const navigate = useNavigate();
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 10,

    p: 4,
    borderRadius: 2,
  };

  const [openFacilityModal, setOpenFacilityModal] = React.useState(false);
  const [openFilterModal, setOpenFilterModal] = React.useState(false);
  const [openDeleteModal, setOpenDeleteModal] = React.useState(false);
  const handleOpenDelete = () => setOpenDeleteModal(true);
  const handleCloseDelete = () => setOpenDeleteModal(false);

  const handleOpenFicility = (row) => {
    setSelectedRow(row);
    setOpenFacilityModal(true);
  };
  const handleCloseFacility = () => {
    setOpenFacilityModal(false);
    setSelectedRow(null);
  };
  const handleOpenFilter = () => setOpenFilterModal(true);
  const handleCloseFilter = () => setOpenFilterModal(false);

  const formikEdit = useFormik({
    initialValues: {
      first_name: "",
      phone: "",
      email: "",
      last_name: "",
      dob: "",
      city: "",
      state: "",
      zip: "",
      license_no: "",
      // category: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("form data for edit:-", values);
    },
  });
  const [selectedRow, setSelectedRow] = useState(null);
  const [formData, setFormData] = useState({
    first_name: "",
    phone: "",
    email: "",
    last_name: "",
    dob: "",
    city: "",
    state: "",
    zip: "",
    license_no: "",
    category: "",
  });

  // useEffect(() => {

  //   if (selectedRow) {
  //     setFormData(selectedRow);
  //   } else {
  //     setFormData(jesse[0]);
  //   }
  // }, [selectedRow ]);
  // const handleSubmit = (e) => {
  //   e.preventDefault();
  //   // Handle form submission based on the selected row
  //   console.log("Updating data for row:", selectedRow);
  //   console.log(formData);
  // };
  // const handleChange = (e) => {
  //   // Update form data on user input
  //   const { name, value } = e.target;
  //   setFormData({ ...formData, [name]: value });
  // };

    // formikEdit.setValues({
    //   id: rowT._id,
    //   facility_name: rowD.facility_name || "empty", // Set default value to an empty string if nurseData.name is undefined
    //   phone_number: rowD.phone_number || "empty",
    //   email_addr: rowD.email_addr || "empty",
    //   city: rowD.city || "empty", // Set a default department value or adjust as needed
    //   state: rowD.state || "empty", // Set a default department value or adjust as needed
    //   zip: rowD.zip || "empty", // Set a default department value or adjust as needed

    //   // ... other form fields
    // });

  return (
    <>
      <Modal
        open={openDeleteModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} className="">
          <Typography id="modal-modal-title">
            <DeleteOutlineIcon />
            Are you sure you want to delete this Driver?
          </Typography>
          <div className="flex justify-between m-3">
            <Button type="submit" variant="contained" color="success">
              Yes
            </Button>
            <Button
              onClick={handleCloseDelete}
              variant="contained"
              color="error"
            >
              No
            </Button>
          </div>
        </Box>
      </Modal>
      <Modal
        open={openFacilityModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} className="w-[800px]">
          <div className="flex flex-row justify-between">
            <Typography id="modal-modal-title">Edit Driver</Typography>

            <Button onClick={handleCloseFacility} color="error">
              <CloseIcon className="text-gray-700" fontSize="40" />
            </Button>
          </div>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <form onSubmit={formikEdit.handleSubmit}>
              <div className="flex flex-col gap-4 m-4 ">
                <div className="flex felx-row gap-3">
                  <TextField
                    id="first_name"
                    name="first_name"
                    placeholder="first name"
                    value={formikEdit.values.first_name}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.first_name &&
                      Boolean(formikEdit.errors.first_name)
                    }
                    helperText={
                      formikEdit.touched.first_name &&
                      formikEdit.errors.first_name
                    }
                  />

                  <TextField
                    id="last_name"
                    name="last_name"
                    placeholder="last_name"
                    value={formikEdit.values.last_name}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.last_name &&
                      Boolean(formikEdit.errors.last_name)
                    }
                    helperText={
                      formikEdit.touched.last_name &&
                      formikEdit.errors.last_name
                    }
                  />

                  <TextField
                    id="email"
                    name="email"
                    label="email"

                    placeholder="email"
                    maxRows={4}
                    value={formikEdit.values.email}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.email &&
                      Boolean(formikEdit.errors.email)
                    }
                    helperText={
                      formikEdit.touched.email && formikEdit.errors.email
                    }
                  />
                </div>

                <div className="flex felx-row gap-3">
                  <TextField
                    id="phone"
                    name="phone"
                    label="phone"

                    placeholder="phone"
                    maxRows={4}
                    value={formikEdit.values.phone}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.phone &&
                      Boolean(formikEdit.errors.phone)
                    }
                    helperText={
                      formikEdit.touched.phone && formikEdit.errors.phone
                    }
                  />
                  <TextField
                    id="dob"
                    name="dob"
                    label="dob"

                    placeholder="dob"
                    maxRows={4}
                    value={formikEdit.values.dob}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.dob && Boolean(formikEdit.errors.dob)
                    }
                    helperText={formikEdit.touched.dob && formikEdit.errors.dob}
                  />
                  <TextField
                    id="city"
                    name="city"
                    label="city"

                    placeholder="city"
                    maxRows={4}
                    value={formikEdit.values.city}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.city && Boolean(formikEdit.errors.city)
                    }
                    helperText={
                      formikEdit.touched.city && formikEdit.errors.city
                    }
                  />
                </div>
                <div className="flex felx-row gap-3">
                  <TextField
                    id="state"
                    name="state"
                    label="state"

                    placeholder="state"
                    maxRows={4}
                    value={formikEdit.values.state}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.state &&
                      Boolean(formikEdit.errors.state)
                    }
                    helperText={
                      formikEdit.touched.state && formikEdit.errors.state
                    }
                  />
                  <TextField
                    id="zip"
                    name="zip"
                    label="zip"

                    placeholder="zip"
                    maxRows={4}
                    value={formikEdit.values.zip}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.zip && Boolean(formikEdit.errors.zip)
                    }
                    helperText={formikEdit.touched.zip && formikEdit.errors.zip}
                  />
                  <TextField
                    id="zip"
                    name="zip"
                    label="zip"

                    placeholder="zip"
                    maxRows={4}
                    value={formikEdit.values.zip}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.zip && Boolean(formikEdit.errors.zip)
                    }
                    helperText={formikEdit.touched.zip && formikEdit.errors.zip}
                  />
                </div>
                <div className="flex felx-row gap-3">
                  <TextField
                    id="license_no"
                    name="license_no"
                    label="license_no"

                    placeholder="license_no"
                    maxRows={4}
                    value={formikEdit.values.license_no}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.license_no &&
                      Boolean(formikEdit.errors.license_no)
                    }
                    helperText={
                      formikEdit.touched.license_no &&
                      formikEdit.errors.license_no
                    }
                  />
                  <TextField
                    id="category"
                    select
                    name="category"
                    label="Driver Category"
                    defaultValue="Driver Category"
                    helperText="Please select category"
                    value={formikEdit.values.category}
                    onChange={formikEdit.handleChange}
                    onBlur={formikEdit.handleBlur}
                    error={
                      formikEdit.touched.category &&
                      Boolean(formikEdit.errors.lincense_no)
                    }
                  >
                    {driver_category.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                  <TextField
                    id="rate"
                    select
                    label="Rate"
                    name="rate"
                    defaultValue="Rate"
                    helperText="Please select Rate"
                  >
                    {rates.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                  <TextField
                    id="ratio"
                    select
                    label="Ratio"
                    name="ratio"
                    defaultValue="EUR"
                    helperText="Please select Rate"
                  >
                    {Ration.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </div>
              </div>
              <div className="flex gap-2 m-3">
                <Button type="submit" variant="contained" color="success">
                  save
                </Button>
                <Button
                  onClick={handleCloseFacility}
                  variant="contained"
                  color="error"
                >
                  Cancel
                </Button>
              </div>
            </form>
          </Typography>
        </Box>
      </Modal>
      <Paper sx={{ width: "100%", overflow: "hidden", padding: 3 }}>
        <TableContainer>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow className="select-none">
                <TableCell>Name</TableCell>
                <TableCell>Phone</TableCell>
                <TableCell>D.O.B</TableCell>
                <TableCell>Address</TableCell>
                <TableCell>License No.</TableCell>
                <TableCell>Category</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {jesse.map((row, index) => (
                <Dropdown
                  key={row.id}
                  menu={{ items }}
                  trigger={["contextMenu"]}
                  placement="bottomLeft"
                >
                  <TableRow
                    key={row.id}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    className="hover:bg-gray-300 select-none"
                    onDoubleClick={handleOpenFicility(row)}
                  >
                    <TableCell align="">
                      {row.first_name} {row.last_name}
                    </TableCell>
                    <TableCell align="">{row.phone}</TableCell>
                    <TableCell align="">{row.dob}</TableCell>
                    <TableCell align="">
                      {row.city} {row.state} {row.zip}
                    </TableCell>
                    <TableCell align="">{row.lincense_no}</TableCell>
                    <TableCell align="center">
                      <p>{row.category}</p>
                    </TableCell>
                  </TableRow>
                </Dropdown>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          className="select-none"
          count={jesse.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
