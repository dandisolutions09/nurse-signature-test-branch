import React from "react";

import { Dropdown } from "antd";
import DescriptionOutlinedIcon from "@mui/icons-material/DescriptionOutlined";
import CallSplitOutlinedIcon from "@mui/icons-material/CallSplitOutlined";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import ShareOutlinedIcon from "@mui/icons-material/ShareOutlined";
import BorderColorOutlinedIcon from "@mui/icons-material/BorderColorOutlined";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import { Person } from "@mui/icons-material";
import { AppBar, Box, Button, Toolbar, Typography } from "@mui/material";
import { NavLink, useLocation } from "react-router-dom";

import ListIcon from "@mui/icons-material/List";

import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import SettingsNav from "../SettingsNav";
import AnaliticsPage from "./AnaliticsPage";
import NavigationBar from "../NavigationBar";

export default function AnaliticsIndex({ user_data }) {
  const location = useLocation();

  //  const incoming_username_data= location.state;

  const incoming_usr_data = location.state;

  const items = [
    {
      key: "1",
      label: "view info",
      icon: <ShareOutlinedIcon />,
    },
    {
      key: "2",
      label: "send invoice",
      icon: <BorderColorOutlinedIcon />,
    },
    {
      key: "3",
      label: "send invoice",
      icon: <DescriptionOutlinedIcon />,
    },
    {
      key: "4",
      label: "Split load",
      icon: <CallSplitOutlinedIcon />,
    },
    {
      key: "5",
      label: "Email load",
      icon: <MailOutlineIcon />,
    },
    {
      key: "6",
      label: "copy",
      icon: <Person />,
    },
    {
      key: "7",
      label: "Delete load",
      icon: <DeleteOutlineIcon />,
    },
  ];
    const [isSideNavOpen, setIsSideNavOpen] = React.useState(false);

    const handleToggleSideNav = () => {
      console.log("opened");
      setIsSideNavOpen(!isSideNavOpen);
    };
  return (
    <>
      <NavigationBar />

      <Typography
        sx={{
          fontSize: "26px",
          margin: "20px",
          fontWeight: "bold",
          color: "rgb(31 41 55)",
        }}
      >
        Analytics
      </Typography>

      <AnaliticsPage />
    </>
  );
}
const header = {
  display: "flex",
  // minHeight:"90%"
};
const text1 = {
  background: "#2196f3",
  padding: "2px",
  color: "white",
  fontSize: "14px",
  borderRadius: "4px",
};
const text2 = {
  fontSize: "16px",
  padding: "2px",
  fontWight: "normal",
  color: "#333",
  borderRadius: "6px",
};
const ret = {
  background: "red",
};
