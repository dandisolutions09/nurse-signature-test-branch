// import React from 'react'
// import AnaliticsCards from './AnaliticsCards'

// export default function AnaliticsPage() {
//   return (
//     <>
//      <AnaliticsCards/> 
//     </>
//   )
// }

// ApiList.js
import React, { useEffect, useState } from "react";
import axios from "axios";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import NavigationBar from "../NavigationBar";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TestingTable from "../TestingTable";
import * as Yup from "yup";
import {
  Modal,
  TextField,
  Box,
  MenuItem,
  IconButton,
  InputAdornment,
} from "@mui/material";
import { useFormik } from "formik";
import CloseIcon from "@mui/icons-material/Close";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import LightTooltip from "../../components/LightTool";



const validationSchema = Yup.object({
  name: Yup.string().required("name is required"),
  password: Yup.string().required("password is required"),
  role: Yup.string().required("role is required"),
});


const ApiList = () => {
  const [data, setData] = useState([]);
  const [positions, setPositions] = useState([]);
  const [wards, setWards] = useState([]);
  const [departments, setDepartments] = useState([]);
  const [newItemText, setNewItemText] = useState(""); // For storing the text entered by the user
  const [settingsId, setSettingsId] = useState([]);
  const [isUpdated, setIsupdated] = useState(true);
 

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "https://nurse-backend.onrender.com/get-settings"
        );

        let positionsArray = [];
        let wardsArray = [];
        let departmentsArray = [];

        response.data.forEach((item) => {
          console.log("ID", item._id);
          setSettingsId(item._id);
          positionsArray = positionsArray.concat(item.position_options);
          departmentsArray = departmentsArray.concat(item.department_options);
          wardsArray = wardsArray.concat(item.ward_options);
        });

        setPositions(positionsArray);
        setDepartments(departmentsArray);
        setWards(wardsArray);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  const handleRemove = (type, index) => {
    let updatedList = [];

    switch (type) {
      case "Wards":
        updatedList = [...wards];
        updatedList.splice(index, 1);
        setWards(updatedList);
        setIsupdated(true);
        break;
      case "Positions":
        updatedList = [...positions];
        updatedList.splice(index, 1);
        setPositions(updatedList);
        break;
      case "Departments":
        updatedList = [...departments];
        updatedList.splice(index, 1);
        setDepartments(updatedList);
        break;
      default:
        break;
    }



    console.log("handle edit function called")
    handleEdit(type, updatedList, settingsId);
  };

  const handleEdit = async (type, updatedList, id) => {
    // Perform any edit logic if needed
    console.log("type", type);
    console.log("updatedList:", updatedList);
    console.log("id:->", settingsId);

    switch (type) {
      case "Wards":
        try {
          const response = await fetch(
            `https://nurse-backend.onrender.com/update-settings/659d4630e5004b55ec6221db`,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                position_options: positions,
                department_options: departments,
                ward_options: updatedList,
              }),
            }
          );

          if (response.ok) {
            console.log("Nurse information updated successfully");
            // postLogs(
            //   incoming_username_data,
            //   `edited: ${updated_nurse_info.name}`
            // );
            // setOpenSuccessAlert(true);

            // setEditNurse(false);
            // Handle further actions or UI updates
            //console.log("body", signatureImageData);
          } else {
            console.error("Failed to update nurse information");
          }
        } catch (error) {
          console.error("An error occurred:", error);
        }

        break;

      case "Positions":
        try {
          const response = await fetch(
            `https://nurse-backend.onrender.com/update-settings/659d4630e5004b55ec6221db`,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                position_options: updatedList,
                department_options: departments,
                ward_options: wards,
              }),
            }
          );

          if (response.ok) {
            console.log("Nurse information updated successfully");
            // postLogs(
            //   incoming_username_data,
            //   `edited: ${updated_nurse_info.name}`
            // );
            // setOpenSuccessAlert(true);

            // setEditNurse(false);
            // Handle further actions or UI updates
            //console.log("body", signatureImageData);
          } else {
            console.error("Failed to update nurse information");
          }
        } catch (error) {
          console.error("An error occurred:", error);
        }
        break;

      case "Departments":
        try {
          const response = await fetch(
            `https://nurse-backend.onrender.com/update-settings/659d4630e5004b55ec6221db`,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                position_options: positions,
                department_options: updatedList,
                ward_options: wards,
              }),
            }
          );

          if (response.ok) {
            console.log("Nurse information updated successfully");
            // postLogs(
            //   incoming_username_data,
            //   `edited: ${updated_nurse_info.name}`
            // );
            // setOpenSuccessAlert(true);

            // setEditNurse(false);
            // Handle further actions or UI updates
            //console.log("body", signatureImageData);
          } else {
            console.error("Failed to update nurse information");
          }
        } catch (error) {
          console.error("An error occurred:", error);
        }

        break;
      default:
        break;
    }
  };

  const handleAdd = (type) => {
    const newItem = prompt(`Enter new ${type} text:`);
    if (newItem) {
      // Perform validation if needed

      // Create a copy of the array to avoid mutating the state directly
      let updatedList = [];

      switch (type) {
        case "Wards":
          updatedList = [...wards, newItem];
          setWards(updatedList);
          break;
        case "Positions":
          updatedList = [...positions, newItem];
          setPositions(updatedList);
          break;
        case "Departments":
          updatedList = [...departments, newItem];
          setDepartments(updatedList);
          break;
        default:
          break;
      }

      // Update the server with the new array
      console.log("handle edit function called")
      handleEdit(type, updatedList, settingsId);
    }
  };
    const style = {
      position: "absolute",
      top: "50%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      bgcolor: "background.paper",
      boxShadow: 6,
      p: 4,
      borderRadius: 2,
      // overflowY: "auto",
      // height: "80vh",
    };
     const role = [
       {
         value: "Admin",
         label: "Admin",
       },
       {
         value: "Intermediate",
         label: "Intermediate",
       },
       {
         value: "Basic",
         label: "Basic",
       },
     ];
      const [showPassword, setShowPassword] = React.useState(false);
      const [openAddUser, setOpenAddUser] = React.useState(false);
      const handleClickShowPassword = () => setShowPassword((show) => !show);
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };
      const handleOpenAddUser = () => {
        setOpenAddUser(true);
      };
      const handleCloseAddUser = () => {
        setOpenAddUser(false);
      };
       const postData = async (data) => {
         console.log("user to be registered", data);

         const user_json = {
           username: data.name,
           password: data.password,
           status: "Active",
           role: data.role,
         };

         console.log("user json", user_json);
         try {
           const response = await axios.post(
             // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
             // "https://nurse-backend.onrender.com/add-nurse",
             "https://nurse-backend.onrender.com/register",
             user_json
           );

           // Handle the response from the backend if needed
           console.log("Response from the backend:", response.data);
           //  setOpenSuccessAlert(true);
         } catch (error) {
           // Handle any errors that occur during the request
           console.error("Error sending data to the backend:", error);
         }
         // alert("nurse saved successfully...");

         handleCloseAddUser();
       };

       const formik = useFormik({
         initialValues: {
           name: "",
           password: "",
           role: "",
         },
         validationSchema: validationSchema,
         onSubmit: (values) => {
           // Handle form submission here
           console.log("form data:-", values);

           postData(values);
         },
       });

  return (
    <>
      {/* <NavigationBar userName={userData.username} /> */}
      <Typography
        sx={{
          fontSize: "26px",
          margin: "20px",

          color: "rgb(31 41 55)",
        }}
      >
        Management
      </Typography>
      <Box sx={{ flexGrow: 1 }} />
      <div className="flex flex-col gap-6 " style={header}>
        <div className="flex flex-row gap-6 justify-center" >
          {/* Wards Card */}
          <Card
            sx={{
              width: 445,
              boxShadow: 6,
            }}
          >
            <CardMedia height="540" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Wards
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <div>
                  <ul>
                    {wards.map((position, index) => (
                      <li
                        key={index}
                        style={{
                          border: "1px solid #ccc",
                          padding: "8px",
                          marginBottom: "8px",
                        }}
                      >
                        <span style={{ marginRight: "8px" }}>&#8226;</span>
                        {position}
                        <Button
                          size="small"
                          onClick={() => handleRemove("Wards", index)}
                        >
                          Remove
                        </Button>
                      </li>
                    ))}
                  </ul>
                </div>
              </Typography>
            </CardContent>
            <CardActions>
              <div className="flex flex-row gap-6">
                <Button size="small" onClick={() => handleAdd("Wards")}>
                  Add Wards
                </Button>
                {/* <Button
                  variant="contained"
                  // size="small"
                  onClick={() => handleEdit("Wards", wards)}
                >
                  save WARDS
                </Button> */}
              </div>
            </CardActions>
          </Card>

          {/* Positions Card */}
          <Card
            sx={{
              width: 445,

              boxShadow: 6,
            }}
          >
            <CardMedia height="540" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Positions
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <div>
                  <ul>
                    {positions.map((position, index) => (
                      <li
                        key={index}
                        style={{
                          border: "1px solid #ccc",
                          padding: "8px",
                          marginBottom: "8px",
                        }}
                      >
                        <span style={{ marginRight: "8px" }}>&#8226;</span>
                        {position}
                        <Button
                          size="small"
                          onClick={() => handleRemove("Positions", index)}
                        >
                          Remove
                        </Button>
                      </li>
                    ))}
                  </ul>
                </div>
              </Typography>
            </CardContent>
            <CardActions>
              {/* <Button
              size="small"
              onClick={() => handleEdit("Positions", positions)}
            >
              Edit
            </Button>
            <Button size="small" onClick={() => handleAdd("Positions")}>
              Add
            </Button> */}

              <div className="flex flex-row gap-6">
                <Button size="small" onClick={() => handleAdd("Positions")}>
                  Add Positions
                </Button>
                {/* <Button
                  variant="contained"
                  // size="small"
                  onClick={() => handleEdit("Wards", wards)}
                >
                  save WARDS
                </Button> */}
              </div>
            </CardActions>
          </Card>

          {/* Departments Card */}
          <Card
            sx={{
              width: 445,
              boxShadow: 6,
            }}
          >
            <CardMedia height="540" />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Departments
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <div>
                  <ul>
                    {departments.map((position, index) => (
                      <li
                        key={index}
                        style={{
                          border: "1px solid #ccc",
                          padding: "8px",
                          marginBottom: "8px",
                        }}
                      >
                        <span style={{ marginRight: "8px" }}>&#8226;</span>
                        {position}
                        <Button
                          size="small"
                          onClick={() => handleRemove("Departments", index)}
                        >
                          Remove
                        </Button>
                      </li>
                    ))}
                  </ul>
                </div>
              </Typography>
            </CardContent>
            <CardActions>
              <div className="flex flex-row gap-6">
                <Button size="small" onClick={() => handleAdd("Departments")}>
                  Add Dept
                </Button>
                {/* <Button
                  variant="contained"
                  // size="small"
                  onClick={() => handleEdit("Wards", wards)}
                >
                  save Dept
                </Button> */}
              </div>
            </CardActions>
          </Card>
        </div>
        {/* <Box sx={{ flexGrow: 1 }} /> */}
        {/* <Box
          component="section"
          sx={{
            p: 2,
            boxShadow: 10,

            margin: "4px",
            borderRadius: "8px",
          }}
        >
          <Box className="flex justify-between">
            <Typography variant="h5">Users</Typography>
            <LightTooltip title="Add New User">
              <Button variant="contained" onClick={handleOpenAddUser}>
                Add +
              </Button>
            </LightTooltip>
          </Box>
          <TestingTable />
        </Box> */}
        <div className="m-4">
 
          <Button
            variant="contained"
            // size="small"
            onClick={() => handleEdit("Wards", wards)}
          >
            save
          </Button>
        </div>
      </div>
    </>
  );
};
const header ={
  border:"1px solid",
  borderColor:"#000",
  margin:"100px",
  padding:"50px",
  borderRadius:"8px"
}

export default ApiList;

