import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import {
  AppBar,
  Box,
  Button,
  FormControl,
  FormLabel,
  LinearProgress,
  MenuItem,
  Modal,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import IconButton from "@mui/material/IconButton";
import { Dropdown } from "antd";
import MoreIcon from "@mui/icons-material/MoreVert";
import Tooltip from "@mui/material/Tooltip";
import Fade from "@mui/material/Fade";
import AirplanemodeActiveIcon from "@mui/icons-material/AirplanemodeActive";
import EditIcon from "@mui/icons-material/Edit";
import AccountCircle from "@mui/icons-material/AccountCircle";
import SwapVertIcon from "@mui/icons-material/SwapVert";
import PersonRemoveIcon from "@mui/icons-material/PersonRemove";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import RadioButtonCheckedIcon from "@mui/icons-material/RadioButtonChecked";
import CloseIcon from "@mui/icons-material/Close";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import InputAdornment from "@mui/material/InputAdornment";

import Radio from "@mui/joy/Radio";
import RadioGroup from "@mui/joy/RadioGroup";
import { useFormik } from "formik";
import * as Yup from "yup";
import LightTooltip from "../components/LightTool";
import SuccessSnackbar from "./SuccessSnackbar";
import NurseActivatesuccess from "../components/NursesAlerts/NurseActivatesuccess";

const validationSchema = Yup.object({
  password: Yup.string().required("Input new password"),
});

const columns = [
  { id: "username", label: "Username", minWidth: 100 },
  { id: "role", label: "Role", minWidth: 100 },
  { id: "status", label: "Status", minWidth: 100 },

  { id: "created_at", label: "Time stamp", minWidth: 100 },
];

function createData(name, code, population, size) {
  const density = population / size;
  return { name, code, population, size, density };
}

export default function TestingTable() {
  const items = [
    {
      key: "1",
      label: "Edit User",
      icon: <EditIcon sx={{ fontSize: 32 }} />,
      onClick: () => handleOpenViewUser(selectedRowData),
    },

    // {
    //   key: "2",
    //   label: "Activate User",
    //   icon: <AirplanemodeActiveIcon />,
    //   onClick: () => handleOPen_confirm(selectedRowData),
    // },
    {
      key: "3",
      label: "Reset User Password",
      icon: <VisibilityOff />,
      onClick: () => handleOpenEdit(selectedRowData),
    },
  ];

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [rows, setRows] = React.useState([]);
  const [openDelete, setOpenDelete] = React.useState(false);
  const [openEdit, setOpenEdit] = React.useState(false);
  const [openDeactivate, setOpenDeactivate] = React.useState(false);
  const [openView, setOpenView] = React.useState(false);
  const [openChangeStatus, setOpenChangeStatus] = React.useState(false);
  const [selectedRowData, setSelectedRowData] = React.useState("");
  const [openChangeRole, setChangeRole] = React.useState(false);
  const [showPassword, setShowPassword] = React.useState(false);
  const [password, setPassword] = React.useState("");
  const [incomingLogs, setIncomingLogs] = React.useState([]);
  const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [openNurseActivatesuccess, setOpenNurseActivatesuccess] = React.useState(false)

  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handle_setempty = () => {
    setPassword("");
  };
  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const handleOpenEdit = () => {
    setOpenEdit(true);
  };
  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const handleOpenChangeRole = () => {
    setChangeRole(true);
  };
  const handleCloseChangeRole = () => {
    setChangeRole(false);
  };
   const [openConfirm, setOpenConfirm] = React.useState(false);
   const handleOPen_confirm = () => {
     setOpenConfirm(true);
   };
   const handleClose_confirm = () => {
     setOpenConfirm(false);
   };

  const handlePUTrequest = async (updated_user_info) => {
    console.log("new nurse infor", updated_user_info);
    setLoading(true)
    // console.log("new signature being updated", updated_nurse_info);
    try {
      const response = await fetch(
        `https://nurse-backend.onrender.com/update-user/${updated_user_info.id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            username: updated_user_info.name,
            password: updated_user_info.password,
            status: updated_user_info.status,
            role: updated_user_info.role,
          }),
        }
      );

      if (response.ok) {
        console.log("user information updated successfully");
        // postLogs(
        //   incoming_username_data,
        //   `edited: ${updated_nurse_info.name}`
        // );
        // setOpenSuccessAlert(true);

        // setEditNurse(false);
        // Handle further actions or UI updates
        //console.log("body", signatureImageData);
      } else {
        console.error("Failed to update nurse information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
    handleCloseViewUser();
    setOpenSuccessAlert(true);
  };

  const handleResetPassword = async (updated_user_info, pwd) => {
    console.log("new nurse infor", updated_user_info);
    console.log("password", pwd.password);
    // console.log("new signature being updated", updated_nurse_info);
    try {
      const response = await fetch(
        `https://nurse-backend.onrender.com/reset-password/${updated_user_info._id}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            username: updated_user_info.username,
            password: pwd.password,
            status: updated_user_info.status,
            role: updated_user_info.role,
          }),
        }
      );

      if (response.ok) {
        console.log("user information updated successfully");
        // postLogs(
        //   incoming_username_data,
        //   `edited: ${updated_nurse_info.name}`
        // );
        // setOpenSuccessAlert(true);

        // setEditNurse(false);
        // Handle further actions or UI updates
        //console.log("body", signatureImageData);
      } else {
        console.error("Failed to update nurse information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
    handleCloseEdit();
    setOpenSuccessAlert(true);
    formik.resetForm();
    handle_setempty();
  };

  const formik = useFormik({
    initialValues: {
      username: "",
      role: "",
      // password: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("form data for edit:-", values);
      handlePUTrequest(values);
      // console.log("passed id", searchResult.ID);
      //console.log("on submit");

      // handleEditNurse(values, searchResult.ID);
    },
  });

  const formik_Password = useFormik({
    initialValues: {
      password: "",
      name: selectedRowData.username,
    },

    //validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("form data for password:-", values);
      handleResetPassword(selectedRowData, values);
      // handlePUTrequest(values);
      // console.log("passed id", searchResult.ID);
      //console.log("on submit");
      formik.resetForm();
      // handleEditNurse(values, searchResult.ID);
    },
  });

  const handleOpenViewUser = () => {
    console.log("mous:", selectedRowData);
    formik.setValues({
      id: selectedRowData._id,
      name: selectedRowData.username || "", // Set default value to an empty string if nurseData.name is undefined
      role: selectedRowData.role || "", // Set a default department value or adjust as needed
      // password: selectedRowData.password || "empty",
      //  password:"",
      status: selectedRowData.status,

      //wards: searchResult.wards || [], // Set default wards value to an empty array if nurseData.wards is undefined
      // ... other form fields
    });
    setOpenView(true);
  };
  const handleCloseViewUser = () => {
    setOpenView(false);
  };

  const handleOpenDeactivate = () => {
    setOpenDeactivate(true);
  };
  const handleCloseDeactivate = () => {
    setOpenDeactivate(false);
  };

  const handleOpenChangeStatus = () => {
    setOpenChangeStatus(true);
  };
  const handleCloseChangeStatus = () => {
    setOpenChangeStatus(false);
  };

  function handleGetAllLogs() {
      setLoading(true);
    console.log("handle get all nurses called!");
    fetch("https://nurse-backend.onrender.com/get-users")
      .then((response) => response.json())
      .then((json) => {
        console.log("rec logs", json);
        json.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

        setRows(json);
      })
      .finally(() => {
        setLoading(false); // Set loading to false after the request is complete (success or error)
      });
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  React.useEffect(() => {
    // This code will run when the component mounts
    //setSearchInput(""); // Set searchInput to an empty string
    //console.log("nurseData", nurseData);
    // handleGetAllNurses();

    handleGetAllLogs();
  }, []); // The empty dependency array ensures the effect runs only once when the component mounts

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const role = [
    {
      key: "1",
      value: "Admin",
      label: "Admin",
    },
    {
      key: "2",
      value: "Intermediate",
      label: "Intermediate",
    },
    {
      key: "3",
      value: "Basic",
      label: "Basic",
    },
  ];
  const status = [
    {
      value: "Active",
      label: "Active",
    },
    {
      value: "Inactive",
      label: "Inactive",
    },
  ];

  return (
    <>
      <NurseActivatesuccess
        open={openNurseActivatesuccess}
        autoHideDuration={3000}
        onClose={() => setOpenNurseActivatesuccess(false)}
      />
      <SuccessSnackbar
        open={openSuccessAlert}
        autoHideDuration={3000}
        onClose={() => setOpenSuccessAlert(false)}
      />
      <Modal
        open={openConfirm}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography className="flex flex-row gap-2">
            A you sure you want to Activate
            <Typography sx={{ fontWeight: "bold" }}>
              {selectedRowData.username}
            </Typography>
            ?
          </Typography>
          <div className="flex flex-row gap-4 m-4 justify-center mt-8">
            <Button variant="contained" color="success" type="submit">
              yes
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={handleClose_confirm}
            >
              no
            </Button>
          </div>
        </Box>
      </Modal>
      <Modal
        open={openDelete}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography className="flex flex-row gap-2">
            Remove
            <Typography sx={{ fontWeight: "bold" }}>
              {selectedRowData.username}
            </Typography>
            ?
          </Typography>
          <div className="flex flex-row gap-4 m-4">
            <Button variant="contained" color="success" type="submit">
              yes
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={handleCloseDelete}
            >
              no
            </Button>
          </div>
        </Box>
      </Modal>
      <Modal
        open={openEdit}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography className="flex flex-row gap-2">
            Reset{" "}
            <Typography sx={{ fontWeight: "bold" }}>
              {" "}
              {selectedRowData.username}&#39;s
            </Typography>
            Password?
          </Typography>

          {/* */}
          <form onSubmit={formik_Password.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <TextField
                type={showPassword ? "text" : "password"}
                id="password"
                name="password"
                label="New Password:"
                placeholder="Enter password"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
                maxRows={4}
                // value={formik_Password.values.password}
                onChange={formik_Password.handleChange}
                onBlur={formik_Password.handleBlur}
                error={
                  formik_Password.touched.password &&
                  Boolean(formik_Password.errors.password)
                }
                helperText={
                  formik_Password.touched.password &&
                  formik_Password.errors.password
                }
              />
            </div>

            <div className="flex flex-row gap-4 m-4">
              <Button variant="contained" type="submit">
                Reset
              </Button>
              <Button
                variant="contained"
                color="error"
                onClick={handleCloseEdit}
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openView}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="flex flex-row justify-between">
            <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Edit User
            </Typography>

            <Button
              onClick={handleCloseViewUser}
              color="error"
              variant="contained"
            >
              <CloseIcon fontSize="40" />
            </Button>
          </div>
          <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <TextField
                  type="text"
                  id="name"
                  name="name"
                  label="name"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  helperText="Please enter full name"
                />

                <TextField
                  id="role"
                  select
                  label="Rolez"
                  name="role"
                  value={formik.values.role}
                  onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                  helperText="Please select role"
                >
                  {role.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>

                <FormControl>
                  <TextField
                    id="status"
                    select
                    label="Status"
                    name="status"
                    value={formik.values.status}
                    onChange={formik.handleChange}
                    // onBlur={formik.handleBlur}
                    helperText="Please select status"
                  >
                    {status.map((option) => (
                      <MenuItem key={option.value} value={option.value}>
                        {option.label}
                      </MenuItem>
                    ))}
                  </TextField>
                </FormControl>
              </div>
            </div>
            <div className="flex m-2 gap-4">
              <Button type="submit" variant="contained">
                Save
              </Button>
              <Button
                onClick={handleCloseViewUser}
                variant="contained"
                color="error"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openChangeStatus}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          Are you sure that you want to change Status
          <Button variant="contained" onClick={handleCloseChangeStatus}>
            close
          </Button>
        </Box>
      </Modal>
      <Modal
        open={openChangeRole}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          Are you sure that you want to change Role?
          <Button variant="contained" onClick={handleCloseChangeRole}>
            close
          </Button>
        </Box>
      </Modal>
      <div>{loading && <LinearProgress />}</div>
      <Paper
        sx={{
          width: "100%",
          overflow: "hidden",
          padding: "30px",
          margin: "20px",
        }}
      >
        <TableContainer sx={{ maxHeight: "100%" }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                    sx={{ fontWeight: "600", fontSize: "16px" }}
                  >
                    {column.label}
                  </TableCell>
                ))}
                <TableCell sx={{ fontWeight: "600", fontSize: "16px" }}>
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <Dropdown
                      key={row._id}
                      menu={{ items }}
                      trigger={["contextMenu"]}
                      placement="bottomLeft"
                    >
                      <TableRow
                        role="checkbox"
                        tabIndex={-1}
                        key={row._id}
                        sx={{
                          "&:hover": { backgroundColor: "rgb(212 212 212)" },
                          cursor: "pointer",
                        }}
                        onMouseEnter={() => {
                          setSelectedRowData(row);
                        }}
                      >
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {column.format && typeof value === "number"
                                ? column.format(value)
                                : value}
                            </TableCell>
                          );
                        })}
                        <LightTooltip
                          key={row._id}
                          title="Right Click For More Actions"
                          TransitionComponent={Fade}
                          TransitionProps={{ timeout: 200 }}
                        >
                          <TableCell>
                            <MoreIcon sx={{ color: "rgb(39 39 42)" }} />
                          </TableCell>
                        </LightTooltip>
                      </TableRow>
                    </Dropdown>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "background.paper",
  boxShadow: 6,
  p: 4,
  borderRadius: 2,
};
