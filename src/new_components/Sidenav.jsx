import React from "react";
import { useState } from "react";
import PersonAddAltIcon from "@mui/icons-material/PersonAddAlt";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import { NavLink, Outlet } from "react-router-dom";
import { FaTruckLoading, FaWarehouse } from "react-icons/fa";
import { VscGear, VscAccount } from "react-icons/vsc";
import { BsFillPersonFill } from "react-icons/bs";
import { RxActivityLog } from "react-icons/rx";
import DashboardIcon from "@mui/icons-material/Dashboard";
import Box from "@mui/material/Box";
import * as Yup from "yup";
import { useFormik } from "formik";
import { Modal, Button } from "@mui/material";
import { useLocation, useNavigate } from "react-router-dom";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import GroupAddIcon from "@mui/icons-material/GroupAdd";
import Typography from "@mui/material/Typography";
import CloseIcon from "@mui/icons-material/Close";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import axios from "axios";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import ReadMoreIcon from "@mui/icons-material/ReadMore";
import SettingsIcon from "@mui/icons-material/Settings";
import SurroundSoundIcon from "@mui/icons-material/SurroundSound";



const validationSchema = Yup.object({
  name: Yup.string().required("name is required"),
  password: Yup.string().required("password is required"),
  role: Yup.string().required("role is required"),
});

export default function Sidenav({ handleAddNurse, user_data }) {
  const [openAddUser, setOpenAddUser] = useState(false);
   const [showPassword, setShowPassword] = React.useState(false);

   const handleClickShowPassword = () => setShowPassword((show) => !show);
   const handleMouseDownPassword = (event) => {
     event.preventDefault();
   };
  const navigate = useNavigate();

  // console.log("fron sidenav:", user_data)

  const handleNavigate1 = () => {
    navigate("/");
  };

  const handleOpenAddUser = () => {
      formik.resetForm();
    setOpenAddUser(true);
  };

  const handleCloseAddUser = () => {
    setOpenAddUser(false);
  };
  function handeleSettingsNav(usr_data) {
    console.log("fron setting page-->", user_data);
    navigate("/settings", { state: user_data });
  }

   const role = [
     {
       value: "Admin",
       label: "Admin",
     },
     {
       value: "User",
       label: "Intermediate",
     },
     {
       value: "User",
       label: "Basic",
     },
   ];

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 6,
    p: 4,
    borderRadius: 2,
  };

 
  const postData = async (data) => {
    console.log("user to be registered", data);

    const user_json = {

      username:data.name,
      password:data.password,
      status:"active",
      role:data.role
    }

    console.log("user json", user_json)
    try {
      const response = await axios.post(
        // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
        // "https://nurse-backend.onrender.com/add-nurse",
         "https://nurse-backend.onrender.com/register",
        user_json
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
      //  setOpenSuccessAlert(true);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    }
    // alert("nurse saved successfully...");

    handleCloseAddUser();
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      password: "",
      role: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("form data:-", values);

      postData(values)
    },
  });

  return (
    <>
      <Modal
        open={openAddUser}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="flex flex-row justify-between">
            <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add New User
            </Typography>

            <Button
              onClick={handleCloseAddUser}
              color="error"
              variant="contained"
            >
              <CloseIcon fontSize="40" />
            </Button>
          </div>
          <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <TextField
                  type="text"
                  id="name"
                  name="name"
                  label="name"
                  helperText="Please enter full name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.name}
                />

                <TextField
                  id="role"
                  select
                  label="Role"
                  name="role"
                  value={formik.values.role}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  helperText="Please select role"
                >
                  {role.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  type={showPassword ? "text" : "password"}
                  id="password"
                  name="password"
                  label="password:"
                  placeholder="password"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  maxRows={4}
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  helperText={formik.touched.password && formik.errors.password}
                />
              </div>
            </div>
            <div className="flex m-2 gap-4">
              <Button type="submit" variant="contained" color="success">
                Submit
              </Button>
              <Button
                onClick={handleCloseAddUser}
                variant="contained"
                color="error"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>

      <div
        className=" flex flex-col gap-3 right-1 absolute h-auto shadow-xl"
        style={header}
      >
        <span className="flex flex-row gap-2 cursor-pointer">
          <SurroundSoundIcon sx={{ fontSize: "32px", color: "white" }} />
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{ fontWeight: "600" }}
          >
            Lyteware
          </Typography>
        </span>
        <NavLink to="/logs">
          <div
            className="flex flex-row gap-4 cursor-pointer hover:bg-blue-500 hover:scale-110 ease-in duration-200"
            style={text}
          >
            <span className="text-gray-600">
              <AccessTimeIcon size={20} />
            </span>
            Logs
          </div>
        </NavLink>
        <div
          className="flex flex-row gap-4 cursor-pointer hover:bg-blue-500"
          style={text}
          onClick={handleAddNurse}
        >
          <span className="text-gray-600">
            <PersonAddAltIcon size={20} />
          </span>
          Add Nurse
        </div>

        <div
          className="flex flex-row gap-4 cursor-pointer hover:bg-blue-500"
          style={text}
          // onClick={handleOpenAddmin}
        >
          <span className="text-gray-600">
            <AdminPanelSettingsIcon size={20} />
          </span>
          Activity
        </div>
        {/* <div
          className="flex flex-row gap-4 cursor-pointer hover:bg-blue-500"
          style={text}
          onClick={handleOpenAddUser}
        >
          <span className="text-gray-600">
            <GroupAddIcon size={20} />
          </span>
          Add User
        </div> */}

        <div
          className="flex flex-row gap-4 cursor-pointer hover:bg-blue-500"
          style={text}
          // onClick={handleNavigate1}
        >
          <span className="text-gray-600">
            <DashboardIcon size={20} />
          </span>
          Analytics
        </div>

        <div
          className="flex flex-row gap-4 cursor-pointer hover:bg-blue-500"
          style={text}
          onClick={() => handeleSettingsNav()}
        >
          <span className="">
            <SettingsIcon className=" text-black hover:text-white" />
          </span>
          <p className=" text-gray-300 hover:text-gray-200"> Settings</p>
        </div>
      </div>
      <main>
        <Outlet />
      </main>
    </>
  );
}
const header = {
  // borderRadius: "10px",
  width: "250px",
  height: "100%",
  padding: "20px",
  background: "rgb(229 229 229)",
  marginTop: "40px",
};
const text1 = {
  color: "#1976D2",
  fontSize: "16px",
};
const text = {
  fontSize: "14px",
  padding: "5px",
  borderRadius: "6px",
};
const activeNavLink = {
  color: "#ff0000", // Set your desired color for the active NavLink
  // Add any other styles as needed
};


