import * as React from "react";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import InputBase from "@mui/material/InputBase";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import * as Yup from "yup";
import { useFormik } from "formik";
import health_logo from "../assets/logo4.png";
import LOGO from "../assets/logo4.png";

import {
  Alert,
  AlertTitle,
  Button,
  ButtonBase,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  MenuItem,
  Modal,
  Paper,
  Select,
  Snackbar,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import useGetNurseByName from "../hooks/SearchNurseHook";
import ReactSignatureCanvas, { SignatureCanvas } from "react-signature-canvas";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Refresh } from "@mui/icons-material";
import { SearchBar } from "./SearchBar";
import { SearchResultsList } from "./SearchResultList";
// import constructWithOptions from "styled-components/dist/constructors/constructWithOptions";
import { IoCloseSharp } from "react-icons/io5";
import ErrorSnackbar from "./ErrorSnackbar";
import SuccessSnackbar from "./SuccessSnackbar";

// import { Formik, Form } from "formik";

const validationSchema = Yup.object({
  name: Yup.string().required("name is required"),
  department: Yup.string().required("department is required"),
  // email_addr: Yup.string().required("Email is required"),
  // city: Yup.string().required("City is required"),
  // state: Yup.string().required("State is required"),
  // zip: Yup.string().required("Zip is required"),
  // phone_number: Yup.string().required("Phone is required"),
});

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

export default function SearchAppBar() {
  const departments = [
    {
      value: "North",
      label: "North",
    },
    {
      value: "West",
      label: "West",
    },
  ];

  const positions = [
    {
      value: "Chief",
      label: "Chief",
    },
    {
      value: "AsstChief",
      label: "AsstChief",
    },
  ];

  const options = [
    { value: "A", label: "A" },
    { value: "B", label: "B" },
    { value: "C", label: "C" },
    // { value: "option3", label: "D" },
  ];
  const [openSearchModal, setOpenSearchModal] = React.useState(false);
  const [searchInput, setSearchInput] = React.useState("");
  const [openaddNurseModal, setAddNurse] = React.useState(false);

  const [authEdit, setAuthEdit] = React.useState(false);

  const [authDelete, setAuthDelete] = React.useState(false);
  const [openEditNurse, setEditNurse] = React.useState(false);

  const [authAddNewNurse, setAuthAdd] = React.useState(false);

  const [selectedRowData, setSelectedRowData] = React.useState(5);

  const { nurseData, loading, error, getNurseByName } = useGetNurseByName();
  const [dept, setDept] = React.useState("");
  const [pos, setPos] = React.useState("");

  const [nurseId, setNurseId] = React.useState(""); // Nurse ObjectId
  const [name, setName] = React.useState("");
  const [departmentT, setDepartment] = React.useState("");
  const [wards, setWards] = React.useState([]);
  const [grade, setGrade] = React.useState("");

  const [results, setResults] = React.useState([]);

  const [openSuccessAlert, setOpenSuccessAlert] = React.useState(false);

  const [openIncorrectPassword, setIncorrectPassword] = React.useState(false);

  const [signatureImageData, setSignatureImageData] = React.useState("");

  const [openWrongPwdError, setOpenWrongPwdError] = React.useState(false);
  const [openWrongPwdSuccess, setOpenWrongPwdSuccess] = React.useState(false);
  // const [signatureImageData, setSignatureImageData] = React.useState(
  // nurseData.signatureImageData
  // );

  const [searchResult, setSearchResult] = React.useState("");

  const [input, setInput] = React.useState("");

  const fetchData = (value) => {
    console.log("fetch data is called!");
    fetch("https://nurse-backend.onrender.com/nurses")
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        const results = json.filter((user) => {
          return (
            value &&
            user &&
            user.name &&
            user.name.toLowerCase().includes(value.toLowerCase())
          );
        });

        console.log("results", results);
        setResults(results);
      });
  };

  const handleChange = (value) => {
    setInput(value);
    fetchData(value);
  };

  function handleSearchResultItem(result) {
    console.log("search item clicked!!!", result);
    setSearchResult(result);

    setOpenSearchModal(true);
  }

  React.useEffect(() => {
    // This code will run when the component mounts
    setSearchInput(""); // Set searchInput to an empty string
    console.log("nurseData", nurseData);
    if (nurseData != null) {
      nurseData._id = "";
      nurseData.name = "";
      nurseData.department = "";
      nurseData.grade = "";
      nurseData.wards = [];
      nurseData.signatureImageData = "";
    }
  }, [openSearchModal]); // The empty dependency array ensures the effect runs only once when the component mounts

  const navigate = useNavigate();

  const handleNavigate1 = () => {
      navigate('/');

  };

  const sigCanvasRef = React.useRef(null);

  const clearSignature = () => {
    sigCanvasRef.current.clear();
  };

  const postData = async (data) => {
    console.log("data to be sent", data);
    try {
      const response = await axios.post(
        // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
        // "https://nurse-backend.onrender.com/add-nurse",
        "https://nurse-backend.onrender.com/add-nurse",
        data
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
      setOpenSuccessAlert(true);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    }
    // alert("nurse saved successfully...");

    handleCloseAddNurse();
  };
  const signupInformationRef = React.useRef({
    name: "",
    // last_name: "",
    grade: "",
    // email: "",
    // disabled: false,
    department: "",
    // role: "Admin",
    // hashed_pwd: "",
    wards: [],
    signatureImageData: "",
  });

  const editValues = {
    name: "jesse",
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      department: "",
      grade: "",
      wards: [],
      signatureImageData: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("form data:-", values);

      const signatureImageURL = sigCanvasRef.current.toDataURL();
      signupInformationRef.current.name = values.name;
      signupInformationRef.current.grade =
        values.grade !== undefined ? values.grade : "empty";
      signupInformationRef.current.department =
        values.department !== undefined ? values.department : "empty";
      signupInformationRef.current.wards = values.wards; // Add selected wards to signupInformationRef
      signupInformationRef.current.signatureImageData = signatureImageURL;
      console.log("sign up info:-", signupInformationRef.current);

      postData(signupInformationRef.current);

      setAuthAdd(false);

      // setTimeout(postData(signupInformationRef.current), 3000)

      // sendMessage("facility_data", values);
      // navigate("/facilities");
    },
  });

  const formikEdit = useFormik({
    initialValues: {
      name: "",
      department: "",
      grade: "",
      wards: [],
      signatureImageData: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      // console.log("form data for edit:-", values);
      console.log("passed id", searchResult.ID);
      console.log("on submit");

      handleEditNurse(values, searchResult.ID);
    },
  });

  const formikAuth = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values, { resetForm }) => {
      // Handle form submission here
      console.log("form data for authentication-edit:-", values);

      if (values.username === "admin" && values.password === "1234") {
        console.log("username and password is correct!");
        // handleOpenEditModal()
        // setOpenSuccessAlert(true);

        setOpenSuccessAlert(true);

        handleOpenEditModal();

        values.username = "";
        values.password = "";
        setAuthEdit(false);

        // setAuthAdd(false);
        // console.log("closing form")

        // window.location.reload()
      } else if (values.username !== "admin" && values.password !== "1234") {
        console.log("username and password is incorrect!");
        //alert("You have entered incorrect password, try again!");
        setOpenWrongPwdError(true);
      }
    },
  });

  const formikAuthDelete = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("form data for authentication-delete:-", values);

      if (values.username === "admin" && values.password === "1234") {
        console.log("usernamr and password is correct!");
        // handleOpenEditModal()
        // setOpenSuccessAlert(true);

        setOpenSuccessAlert(true);

        values.username = "";
        values.password = "";

        handleOpenDeleteModal();
      } else if (values.username !== "admin" && values.password !== "1234") {
        console.log("username and password is incorrect!");
        setOpenWrongPwdError(true);
        //alert("You have entered incorrect password, try again!");
      }
    },
  });

  const formikAuthAdd = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    // validationSchema: validationSchema,
    onSubmit: (values, { resetForm }) => {
      // Handle form submission here
      console.log("form data for authentication-add:-", values);

      if (values.username === "admin" && values.password === "1234") {
        console.log("username and password is correct!");
        handleOpenEditModal()
        // setOpenSuccessAlert(true);

        setOpenSuccessAlert(true);

        setAddNurse(true);
        values.username = "";
        values.password = "";

        setAuthAdd(false);
      } else if (values.username !== "admin" || values.password !== "1234") {
        console.log("username and password is incorrect!");
        //  alert("You have entered incorrect password, try again!");

        setOpenWrongPwdError(true);
        // <Snackbar
        //   open={openWrongPwdError}
        //   autoHideDuration={4000}
        //   onClose={handleClose}
        // >
        //   <Alert
        //     onClose={handleClose}
        //     severity="success"
        //     sx={{ width: "100%" }}
        //   >
        //     username or password is incorrect!
        //   </Alert>
        // </Snackbar>;
      }else{
        console.log("fdfdfdf")
        setOpenWrongPwdError(true);
      }
    },
  });

  //const signatureImageURL = sigCanvasRef.current.toDataURL();

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 6,
    p: 4,
    borderRadius: 2,
    width: "800px",
  };
  const style_Paper = {
    backgroundColor: "", // Add your desired background color
    padding: "20px", // Adjust padding as needed
    maxWidth: "600px", // Set a max width if desired
    margin: "auto", // Center the modal horizontally
    position: "relative",
    top: "30%",
    height: "300px",

    // left: "50%",
  };

  const style_picture = {
    // position: "absolute",
    // marginBottom:"70px",
    // top: "20%",
    // left: "25%",
    // height:"200px",
    // width:"200px"
  };
  const style_box = {
    borderBottom: "1px solid #ccc", // Add bottom border
  };

  const style_Search = {
    Width: "full",
    // border: "1px solid #ccc",

    // backgroundColor: "white"
  };

  const handleSubmit = (values) => {
    // Handle form submission logic here
    console.log(values);
  };

  const handleSearchChange = (event) => {
    setSearchInput(event.target.value);
  };

  function handleSearchModal() {
    console.log("search modal activated...");
    setOpenSearchModal(true);
  }

  const handleClose = () => {
    setOpenSearchModal(false);
    console.log("search modal deactivated...");
  };

  const handleCloseErrorModal = () => {
    setOpenWrongPwdError(false);
    console.log("search modal deactivated...");
  };

  function handleSearch() {
    console.log("Search submitted..", searchInput);
    getNurseByName(searchInput);

    // setNurseId(nurseData._id)
    // setName(nurseData.name)
  }

  function handleAddNurse() {
    console.log("handle Add nurse....");

    setAuthAdd(true);
    //setAddNurse(true);
  }

  function handleOpenEditModal() {
    console.log("nurseEditData", searchResult);
    formikEdit.setValues({
      name: searchResult.name || "", // Set default value to an empty string if nurseData.name is undefined
      department: searchResult.department || "Department", // Set a default department value or adjust as needed
      grade: searchResult.grade,

      wards: searchResult.wards || [], // Set default wards value to an empty array if nurseData.wards is undefined
      // ... other form fields
    });
    setEditNurse(true);
  }

  async function handleOpenDeleteModal() {
    console.log("nurseEditData", nurseData);

    try {
      const response = await axios.delete(
        `https://nurse-backend.onrender.com/remove-nurse/${searchResult.ID}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      // Handle success
      console.log("deleted successfully");
      setTimeout(window.location.reload(), 4000);

      setOpenSuccessAlert(true)

      //window.location.reload();
      console.log("should be navigatiing...");
    } catch (error) {
      // Handle error
      // toast({
      // title: "Cannot be deleted",
      // position: "top",
      // isClosable: true,
      // duration: 3000,
      // status: "error",
      // });
    }
  }

  function handleOpenAuthEdit() {
    setAuthEdit(true);
  }

  function handleOpenAuthDelete() {
    setAuthDelete(true);
  }

  function handleCloseEditModal() {
    setEditNurse(false);
  }

  function handleCloseDeleteAuth() {
    setAuthDelete(false);
  }

  function handleCloseAddAuth() {
    setAuthAdd(false);
  }

  function handleCloseAddNurse() {
    setAddNurse(false);
  }

  function handleCloseEditAuth() {
    setAuthEdit(false);
  }

  const handleChange_dept = (event) => {
    setDept(event.target.value);
  };

  const handleChange_pos = (event) => {
    setPos(event.target.value);
  };

  function handleSubmit_Edit_form() {}

  function handleCloseFacility() {}

  const handlePUTrequest = async (nurseId, updated_nurse_info) => {
    console.log("updated nurse info:", updated_nurse_info);
    try {
      const response = await fetch(
        `https://nurse-backend.onrender.com/update-nurse${nurseId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: updated_nurse_info.Name,
            department: updated_nurse_info.department,
            wards: updated_nurse_info.wards,
            grade: updated_nurse_info.grade,
            signatureImageData: signatureImageData,
          }),
        }
      );

      if (response.ok) {
        console.log("Nurse information updated successfully");
        setOpenSuccessAlert(true)
        // Handle further actions or UI updates
        //console.log("body", signatureImageData);
      } else {
        console.error("Failed to update nurse information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const handlePUTrequest_for_edited_signature = async (
    nurseId,
    new_sig,
    updated_nurse_info
  ) => {
    console.log("new nurse infor", updated_nurse_info);
    // console.log("new signature being updated", updated_nurse_info);
    try {
      const response = await fetch(
        `https://nurse-backend.onrender.com/update-nurse/${nurseId}`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            name: updated_nurse_info.name,
            department: updated_nurse_info.department,
            wards: updated_nurse_info.wards,
            grade: updated_nurse_info.grade,
            signatureImageData: new_sig,
          }),
        }
      );

      if (response.ok) {
        console.log("Nurse information updated successfully");
        setOpenSuccessAlert(true)

        setEditNurse(false);
        // Handle further actions or UI updates
        //console.log("body", signatureImageData);
      } else {
        console.error("Failed to update nurse information");
      }
    } catch (error) {
      console.error("An error occurred:", error);
    }
  };

  const handleEditNurse = async (values, id) => {
    console.log("incoming id", id);
    console.log("edited value passed", values);

    const signatureImageURL = sigCanvasRef.current.toDataURL();
    //console.log("signature image uRl:", signatureImageURL);
    const binaryImageData = atob(signatureImageURL.split(",")[1]);
    const imageSizeBytes = binaryImageData.length;

    //console.log("signature size", imageSizeBytes);

    if (imageSizeBytes < 3000) {
      console.log("image size bytes less than 3000");
      //signatureImageURL = signatureImageData
      setSignatureImageData(nurseData.signatureImageData);
      handlePUTrequest(nurseId, values);

      //console.log("less than 3000", nurseData.signatureImageData);
    } else if (imageSizeBytes > 3000) {
      console.log("image size bytes greater than 3000");
      //console.log("edited signature of the nurse");
      //setSignatureImageData(toString(signatureImageURL));
      console.log("type", typeof signatureImageURL);
      handlePUTrequest_for_edited_signature(id, signatureImageURL, values);
    }

    <Alert severity="success" color="info">
      Nurse information edited successfully...
    </Alert>;
    // toast({
    // title: "Nurse information edited successfully...",
    // position: "top",
    // isClosable: true,
    // duration: 2000,
    // status: "success",
    // });

    // navigate("/");
  };

  const handleDelete = async (nurse_data) => {
    handleOpenAuthDelete();
  };


  return (
    <>
      {/* Search Modal */}

      {/* <Snackbar
        open={openWrongPwdError}
        autoHideDuration={3000}
        onClose={handleCloseErrorModal}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <Alert
          onClose={handleCloseErrorModal}
          severity="error"
          sx={{ width: "100%" }}
        >
          Incorrect username or password!
        </Alert>
      </Snackbar> */}

      <ErrorSnackbar
        open={openWrongPwdError}
        autoHideDuration={3000}
        onClose={handleCloseErrorModal}
      />

      <SuccessSnackbar
        open={openSuccessAlert}
        autoHideDuration={3000}
        onClose={() => setOpenSuccessAlert(false)}
      />

      <Modal
        open={openSearchModal}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Paper sx={style_Paper}>
          {searchResult && (
            <Box sx={style}>
                {/* Render your data here */}
                {/* <p>ERROR:-{searchResult.error}</p> */}
                <div
               className="absolute  top-2 right-2 m-2"
                  onClick={() => setOpenSearchModal(false)}
                >
                  <IoCloseSharp />
                </div>

             
                  <div className="flex flex-col gap-4 m-3">
                    <p>
                      {" "}
                      <span className="text-xs">Name: </span>{" "}
                      <span className="text-xl "> {searchResult.name}</span>
                    </p>

                    <p>
                      {" "}
                      <span className="text-xs">Dept: </span>{" "}
                      <span className="text-xl ">
                        {" "}
                        {searchResult.department}
                      </span>
                    </p>

                    <p>

                      <span className="text-xs">position: </span>{" "}
                      <span className=""> {searchResult.grade}</span>
                    </p>

                    <p>
            
                      <span className="text-xs">wards: </span>{" "}
                      <span className=""> {searchResult.wards}</span>
                    </p>
                  </div>

                  <div className="border p-2  border-1 m-3">
                    Signature:
                    <img
                      // src={searchResult.signatureImageData}
                      src={LOGO}
                      alt="NOT FOUND"
                      style={{ width: "300px", height: "auto" }}
                    />
                  </div>
                  <div className="flex flex-row gap-6 items-center justify-center mt-2">
                  {/* <Button variant="contained" onClick={handleOpenEditModal}> */}
                  <Button variant="contained" onClick={handleOpenAuthEdit}>
                    Edit
                  </Button>
                  <Button
                    onClick={handleOpenAuthDelete}
                    variant="contained"
                    color="error"
                  >
                    Delete
                  </Button>
                </div>
                
               
            
             

                <Modal
                  open={openEditNurse}
                  aria-labelledby="modal-modal-title"
                  aria-describedby="modal-modal-description"
                >
                  <Box sx={style}>
                    <Typography
                      id="modal-modal-title"
                      variant="h6"
                      component="h2"
                    >
                      Edit Nurse
                    </Typography>

                    <form className="m-4" onSubmit={formikEdit.handleSubmit}>
                      <div className="flex flex-col gap-4">
                        <TextField
                          id="name"
                          name="name"
                          label="Name:"
                          multiline
                          placeholder="name"
                          maxRows={4}
                          value={formikEdit.values.name}
                          onChange={formikEdit.handleChange}
                          onBlur={formikEdit.handleBlur}
                          error={
                            formikEdit.touched.name &&
                            Boolean(formikEdit.errors.name)
                          }
                          helperText={
                            formikEdit.touched.name && formikEdit.errors.name
                          }
                        />

                        <TextField
                          id="department"
                          select
                          name="department"
                          label="Department"
                          defaultValue="Department"
                          helperText="Please select Department"
                          value={formikEdit.values.department}
                          onChange={formikEdit.handleChange}
                          onBlur={formikEdit.handleBlur}
                        >
                          {departments.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                              {option.label}
                            </MenuItem>
                          ))}
                        </TextField>

                        <TextField
                          id="position"
                          select
                          name="grade"
                          label="Position"
                          defaultValue="Position"
                          helperText="Please select Position"
                          value={formikEdit.values.grade}
                          onChange={formikEdit.handleChange}
                          onBlur={formikEdit.handleBlur}
                        >
                          {positions.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                              {option.label}
                            </MenuItem>
                          ))}
                        </TextField>

                        <div className="border-2 p-2">
                          <p className="font-light">Wards</p>
                          <FormGroup row>
                            {options.map((option) => (
                              <FormControlLabel
                                key={option.value}
                                control={
                                  <Checkbox
                                    name="wards"
                                    value={option.value}
                                    checked={formikEdit.values.wards.includes(
                                      option.value
                                    )}
                                    onChange={formikEdit.handleChange}
                                  />
                                }
                                label={option.label}
                              />
                            ))}
                          </FormGroup>
                        </div>

                        <div className="border-2">
                          Current Signature:{" "}
                          <img
                            src={searchResult.signatureImageData}
                            alt="NOT FOUND"
                          />
                        </div>

                        <div className="flex flex-col gap-2 mt-2">
                          <div>New Signature:</div>
                          <div>
                            <ReactSignatureCanvas
                              ref={sigCanvasRef}
                              penColor="blue"
                              canvasProps={{
                                width: 670,
                                height: 130,
                                className: "signature-canvas",
                                className: "image-container",
                              }}
                            />
                          </div>
                        </div>

                        {/* <Button onClick={clearSignature}>Clear Signature</Button> */}

                        <div>
                          <Button
                            onClick={clearSignature}
                            variant="outlined"
                            color="error"
                          >
                            Clear Signature
                          </Button>
                        </div>
          
                      </div>

                      <div className="flex m-2 gap-4">
                        <button
                          type="submit"
                          className="bg-blue-500 p-2 text-white px-4 rounded-md hover:bg-blue-400 ease-linear duration-200"
                          // onClick={handleEditNurse}
                        >
                          Edit
                        </button>
                        <button
                          onClick={handleCloseEditModal}
                          className="bg-blue-500 p-2 text-white px-4 rounded-md hover:bg-blue-400 ease-linear duration-200"
                        >
                          Close
                        </button>
                      </div>
                    </form>
                  </Box>
                </Modal>
           
              </Box>
          )}
        </Paper>
      </Modal>

      {/* Add new Nurse Modal */}
      <Modal
        open={openaddNurseModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add New Nurse
          </Typography>

          <form className="m-4" onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4">
              <TextField
                id="name"
                name="name"
                label="Name:"
                multiline
                placeholder="name"
                maxRows={4}
                value={formik.values.nmr}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.name && Boolean(formik.errors.name)}
                helperText={formik.touched.name && formik.errors.name}
              />

              <TextField
                id="department"
                select
                name="department"
                label="Department"
                defaultValue="Department"
                helperText="Please select Department"
                value={formik.values.departments}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              >
                {departments.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>

              <TextField
                id="position"
                select
                name="grade"
                label="Position"
                defaultValue="Position"
                helperText="Please select Position"
                value={formik.values.positions}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              >
                {positions.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>

              <div className="border-2 p-2">
                <p className="font-light">Wards</p>
                <FormGroup row>
                  {options.map((option) => (
                    <FormControlLabel
                      key={option.value}
                      control={
                        <Checkbox
                          name="wards"
                          value={option.value}
                          checked={formik.values.wards.includes(option.value)}
                          onChange={formik.handleChange}
                        />
                      }
                      label={option.label}
                    />
                  ))}
                </FormGroup>
              </div>

              <div>
                <ReactSignatureCanvas
                  ref={sigCanvasRef}
                  penColor="blue"
                  canvasProps={{
                    width: 400,
                    height: 130,
                    className: "signature-canvas",
                    className: "image-container",
                  }}
                />
              </div>
            </div>

            <div>
              <Button onClick={clearSignature} variant="outlined" color="error">
                Clear Signature
              </Button>
            </div>

            <div className="flex m-2 gap-4">
              <button
                type="submit"
                className="bg-blue-500 p-2 text-white px-4 rounded-md hover:bg-blue-400 ease-linear duration-200"
              >
                Submit
              </button>
              <button
                onClick={handleCloseAddNurse}
                className="bg-blue-500 p-2 text-white px-4 rounded-md hover:bg-blue-400 ease-linear duration-200"
              >
                Close
              </button>
            </div>
          </form>
        </Box>
      </Modal>

      {/* AUTH EDIT */}
      <Modal
        open={authEdit}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <div className="flex text-center justify-center w-full mt-10">
          <Box className="justify-center text-center flex flex-col bg-gray-100 w-[600px] m-4 p-6 rounded-lg shadow-xl">
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Enter username and password -Edit
            </Typography>

            <form className="m-4" onSubmit={formikAuth.handleSubmit}>
              <div className="flex flex-col gap-4">
                <TextField
                  id="username"
                  name="username"
                  label="Username:"
                  multiline
                  placeholder="username"
                  maxRows={4}
                  value={formikAuth.values.username}
                  onChange={formikAuth.handleChange}
                  onBlur={formikAuth.handleBlur}
                  error={
                    formikAuth.touched.name && Boolean(formikAuth.errors.name)
                  }
                  helperText={formikAuth.touched.name && formikAuth.errors.name}
                />

                <TextField
                  name="password"
                  id="outlined-password-input"
                  label="Password"
                  type="password"
                  placeholder="password"
                  // autoComplete="current-password"
                  value={formikAuth.values.password}
                  onChange={formikAuth.handleChange}
                  onBlur={formikAuth.handleBlur}
                  error={
                    formikAuth.touched.name && Boolean(formikAuth.errors.name)
                  }
                  helperText={formikAuth.touched.name && formikAuth.errors.name}
                />
              </div>

              <div className="flex m-2 gap-4">
                <Button type="submit" variant="contained" color="success">
                  Submit
                </Button>

                <Button
                  onClick={handleCloseEditAuth}
                  variant="contained"
                  color="error"
                >
                  Close
                </Button>
              </div>
            </form>
          </Box>
        </div>
      </Modal>

      {/* AUTH DELETE */}
      <Modal
        open={authDelete}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            name and password -delete
          </Typography>

          <form className="m-4" onSubmit={formikAuthDelete.handleSubmit}>
            <div className="flex flex-col gap-4">
              <TextField
                id="username"
                name="username"
                label="Username:"
                multiline
                placeholder="username"
                maxRows={4}
                value={formikAuthDelete.values.username}
                onChange={formikAuthDelete.handleChange}
                onBlur={formikAuthDelete.handleBlur}
                error={
                  formikAuthDelete.touched.name &&
                  Boolean(formikAuthDelete.errors.name)
                }
                helperText={
                  formikAuthDelete.touched.name && formikAuthDelete.errors.name
                }
              />

              <TextField
                name="password"
                id="outlined-password-input"
                label="Password"
                type="password"
                placeholder="password"
                autoComplete="current-password"
                value={formikAuthDelete.values.password}
                onChange={formikAuthDelete.handleChange}
                onBlur={formikAuthDelete.handleBlur}
                error={
                  formikAuthDelete.touched.name &&
                  Boolean(formikAuthDelete.errors.name)
                }
                helperText={
                  formikAuthDelete.touched.name && formikAuthDelete.errors.name
                }
              />
            </div>

            <div className="flex m-2 gap-4">
              <Button type="submit" variant="contained" color="success">
                Submit
              </Button>

              <Button
                onClick={handleCloseDeleteAuth}
                variant="contained"
                color="error"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>

      {/* AUTH ADD NEW NURSE */}
      <Modal
        open={authAddNewNurse}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <div className="flex text-center justify-center w-full mt-10">
          <Box className="justify-center text-center flex flex-col bg-gray-100 w-[600px] m-4 p-6 rounded-lg shadow-xl">
            <Typography id="modal-modal-title" variant="h6" component="h2">
              Enter username and password
            </Typography>

            <form className="m-4" onSubmit={formikAuthAdd.handleSubmit}>
              <div className="flex flex-col gap-4">
                <TextField
                  id="username"
                  name="username"
                  label="Username:"
                  multiline
                  placeholder="username"
                  maxRows={4}
                  value={formikAuthAdd.values.username}
                  onChange={formikAuthAdd.handleChange}
                  onBlur={formikAuthAdd.handleBlur}
                  error={
                    formikAuthAdd.touched.name &&
                    Boolean(formikAuthAdd.errors.name)
                  }
                  helperText={
                    formikAuthAdd.touched.name && formikAuthAdd.errors.name
                  }
                />

                <TextField
                  name="password"
                  id="outlined-password-input"
                  label="Password"
                  type="password"
                  placeholder="password"
                  autoComplete="current-password"
                  value={formikAuthAdd.values.password}
                  onChange={formikAuthAdd.handleChange}
                  onBlur={formikAuthAdd.handleBlur}
                  error={
                    formikAuthAdd.touched.name &&
                    Boolean(formikAuthAdd.errors.name)
                  }
                  helperText={
                    formikAuthAdd.touched.name && formikAuthAdd.errors.name
                  }
                />
              </div>

              <div className="flex m-2 gap-4">
                {/* <button
 type="submit"
 className="bg-blue-500 p-2 text-white px-4 rounded-md hover:bg-blue-400 ease-linear duration-200"
 >
 Submit
 </button> */}

                <Button type="submit" variant="contained" color="success">
                  Submit
                </Button>

                <Button
                  onClick={handleCloseAddAuth}
                  variant="contained"
                  color="error"
                >
                  Close
                </Button>
                {/* <button
 onClick={handleCloseAddAuth}
 className="bg-blue-500 p-2 text-white px-4 rounded-md hover:bg-blue-400 ease-linear duration-200"
 >
 Close
 </button> */}
              </div>
            </form>
          </Box>
        </div>
      </Modal>

      {/* Edit Nurse Modal */}

      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              // color="inherit"
              aria-label="open drawer"
              sx={{ mr: 2 }}
              onClick={handleNavigate1}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ flexGrow: 1, display: { xs: "none", sm: "block" } }}
            >
              Nurses CD Signature System
            </Typography>

            <Button
              variant="contained"
              color="success"
              onClick={handleAddNurse}
            >
              Add Nurse +
            </Button>
          </Toolbar>
        </AppBar>
      </Box>

      {/* <Box>
 <img src={health_logo} alt="Dan Abramov" />
 </Box> */}

      <Box className="flex justify-center text-center w-full mt-[12%]">
        <img
          src={health_logo}
          alt="NOT FOUND"
          className="w-[200px]  rounded-full  "
        />
      </Box>

      <div className="">
        <div className="flex text-center justify-center ">
          <Box className="flex flex-row bg-gray-100 w-[550px]  rounded-lg shadow-xl gap-[300px]">
            <Search>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase
                //onClick={handleSearchModal}
                placeholder="Search…"
                inputProps={{ "aria-label": "search" }}
                value={input}
                onChange={(e) => handleChange(e.target.value)}
              />
            </Search>
{/* 
            <Button variant="contained" onClick={handleSearchModal}>
              Search
            </Button> */}
          </Box>
        </div>

        {/* <div className="border rounded-md shadow-lg ml-[650px] w-[550px] "> */}
        <div className="border rounded-md shadow-lg mx-auto max-w-[350px] md:max-w-[400px] lg:max-w-[550px]">
          {results.map((result, id) => {
            // return <SearchResult result={result.name} key={id} />;
            return (
              <div
                className="flex flex-row gap-2 hover:bg-blue-500 h-[40px] p-2 cursor-pointer"
                key={id}
                onClick={() => handleSearchResultItem(result)}
              >
                {result.name}
              </div>
            );
          })}
        </div>
      </div>

      {/* {results && results.length > 0 && <SearchResultsList results={results} />} */}
    </>
  );
}









// edit
<form onSubmit={formikEdit.handleSubmit}>
  <div className="flex flex-col gap-4 m-4">
    <div className="flex felx-row gap-3">
      <TextField
        id="name"
        name="name"
        label="Name:"
        multiline
        placeholder="name"
        maxRows={4}
        value={formikEdit.values.name}
        onChange={formikEdit.handleChange}
        onBlur={formikEdit.handleBlur}
        error={formikEdit.touched.name && Boolean(formikEdit.errors.name)}
        helperText={formikEdit.touched.name && formikEdit.errors.name}
      />
      <TextField
        id="department"
        select
        name="department"
        label="Department"
        defaultValue="Department"
        helperText="Please select Department"
        value={formikEdit.values.department}
        onChange={formikEdit.handleChange}
        onBlur={formikEdit.handleBlur}
      >
        {departments.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
      <TextField
        id="position"
        select
        name="grade"
        label="Position"
        defaultValue="Position"
        helperText="Please select Position"
        value={formikEdit.values.grade}
        onChange={formikEdit.handleChange}
        onBlur={formikEdit.handleBlur}
      >
        {positions.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
    </div>

    <Typography id="modal-modal-title">Wards</Typography>
    <div className="border-2 p-2">
      <FormGroup row>
        {options.map((option) => (
          <FormControlLabel
            key={option.value}
            control={
              <Checkbox
                name="wards"
                value={option.value}
                checked={formikEdit.values.wards.includes(option.value)}
                onChange={formikEdit.handleChange}
              />
            }
            label={option.label}
          />
        ))}
      </FormGroup>
    </div>

    <Typography id="modal-modal-title">Current Signature:</Typography>
    <div className="border-2">
      <img src={searchResult.signatureImageData} alt="NOT FOUND" />
    </div>
    <Typography id="modal-modal-title">New Signature:</Typography>
    <div className="flex flex-col gap-2 mt-2">
      <div>
        <ReactSignatureCanvas
          ref={sigCanvasRef}
          penColor="blue"
          canvasProps={{
            width: 670,
            height: 130,
            className: "signature-canvas",
            className: "image-container",
          }}
        />
      </div>
    </div>

    {/* <Button onClick={clearSignature}>Clear Signature</Button> */}

    <div>
      <Button onClick={clearSignature} variant="outlined" color="error">
        Clear Signature
      </Button>
    </div>

    <Box className="flex m-2 gap-4">
      <button
        type="submit"
        className="bg-blue-500 p-2 text-white px-4 rounded-md hover:bg-blue-400 ease-linear duration-200"
        // onClick={handleEditNurse}
      >
        Edit
      </button>
      <button
        onClick={handleCloseEditModal}
        className="bg-blue-500 p-2 text-white px-4 rounded-md hover:bg-blue-400 ease-linear duration-200"
      >
        Close
      </button>
    </Box>
  </div>
</form>; 