import React, { useState } from "react";
import {
  AiOutlineMenu,
  AiOutlineHome,
  AiOutlineProject,
  AiOutlineMail,
} from "react-icons/ai";
import GroupAddIcon from "@mui/icons-material/GroupAdd";
import { GrProjects } from "react-icons/gr";
import { BsPerson } from "react-icons/bs";
import LOGO from "../assets/avatar.jpg";
import { NavLink, Outlet } from "react-router-dom";
import DashboardIcon from "@mui/icons-material/Dashboard";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import SurroundSoundIcon from "@mui/icons-material/SurroundSound";
import { MdManageAccounts } from "react-icons/md";
import {
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  MenuItem,
  Modal,
  TextField,
  Typography,
} from "@mui/material";
import * as Yup from "yup";
import { useFormik } from "formik";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Visibility from "@mui/icons-material/Visibility";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import { useLocation, useNavigate } from "react-router-dom";
import Tooltip from "@mui/material/Tooltip";
import { styled } from "@mui/material/styles";
import ReactSignatureCanvas from "react-signature-canvas";
import CloseIcon from "@mui/icons-material/Close";
import MiscellaneousServicesIcon from "@mui/icons-material/MiscellaneousServices";
import Textfield from "./Textfield";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import axios from "axios";

const validationSchema = Yup.object({
  name: Yup.string().required("name is required"),
  password: Yup.string().required("password is required"),
  role: Yup.string().required("role is required"),
});

const SettingsNav = (usr_data) => {
  const [openAddNurse, setOpenAddNurse] = useState(false);
  const [openAddUser, setOpenAddUser] = useState(false);
  const [openFields, setOpenFields] = useState(false);
  const [showPassword, setShowPassword] = React.useState(false);

 

  console.log("settings nav:", usr_data.usr_data);

  const handleOpenAddNurse = () => {
    console.log("side-nave", usr_data.usr_data);
    setOpenAddNurse(true);
  };
  const handleOpenAddUser = () => {
    setOpenAddUser(true);
  };
  const handleOpenField = () => {
    setOpenFields(true);
  };
  const handleCloseAddUser = () => {
    formik.resetForm();
    setOpenAddUser(false);
  };
  const handleCloseAddNurse = () => {
    setOpenAddNurse(false);
  };
  const handleCloseFields = () => {
    setOpenFields(false);
  };

  const navigate = useNavigate();
  const handleNavigate1 = () => {
    navigate("/home", { state: usr_data.usr_data });
  };
  const handleNavigateDash = () => {
    navigate("/analitics", { state: usr_data.usr_data });
  };
  const handeleNavigateSettings = () => {
    navigate("/settings", { state: usr_data.usr_data });
  };
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const [nav, setNav] = useState(false);
  function handleNav() {
    setNav(!nav);
    console.log("set changed");
  }
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    boxShadow: 6,
    p: 4,
    borderRadius: 2,
    // overflowY: "auto",
    // height: "80vh",
  };
  const LightTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: theme.palette.common.white,
      color: "rgba(0, 0, 0, 0.87)",
      boxShadow: theme.shadows[1],
      fontSize: 11,
    },
  }));
  const departments = [
    {
      value: "North",
      label: "North",
    },
    {
      value: "West",
      label: "West",
    },
  ];
  const role = [
    {
      value: "Admin",
      label: "Admin",
    },
    {
      value: "User",
      label: "Intermediate",
    },
    {
      value: "User",
      label: "Basic",
    },
  ];

  const positions = [
    {
      value: "Chief",
      label: "Chief",
    },
    {
      value: "AsstChief",
      label: "AsstChief",
    },
  ];

  const options = [
    { value: "A", label: "A" },
    { value: "B", label: "B" },
    { value: "C", label: "C" },
    // { value: "option3", label: "D" },
  ];
  const postData = async (data) => {
    console.log("user to be registered", data);

    const user_json = {
      username: data.name,
      password: data.password,
      status: "active",
      role: data.role,
    };

    console.log("user json", user_json);
    try {
      const response = await axios.post(
        // "https://7297-41-80-118-236.ngrok-free.app/your-fastapi-endpoint2",
        // "https://nurse-backend.onrender.com/add-nurse",
        "https://nurse-backend.onrender.com/register",
        user_json
      );

      // Handle the response from the backend if needed
      console.log("Response from the backend:", response.data);
      //  setOpenSuccessAlert(true);
    } catch (error) {
      // Handle any errors that occur during the request
      console.error("Error sending data to the backend:", error);
    }
    // alert("nurse saved successfully...");

    handleCloseAddUser();
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      password: "",
      role: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      // Handle form submission here
      console.log("form data:-", values);

      postData(values);
    },
  });

  return (
    <>
      <Modal
        open={openFields}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{ ...style, height: "80vh", overflowY: "auto", border: "none" }}
        >
          <div className="flex flex-row justify-between">
            <Typography
              id="modal-modal-title"
              sx={{ fontWeight: "bold", select: "none" }}
              className="select-none"
            >
              Fields management
            </Typography>
            <Button
              onClick={handleCloseFields}
              color="error"
              variant="contained"
            >
              <CloseIcon fontSize="40" />
            </Button>
          </div>
          <form>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <Textfield />
              </div>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openAddUser}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="flex flex-row justify-between">
            <Typography id="modal-modal-title" sx={{ fontWeight: "bold" }}>
              Add New User
            </Typography>

            <Button
              onClick={handleCloseAddUser}
              color="error"
              variant="contained"
            >
              <CloseIcon fontSize="40" />
            </Button>
          </div>
          <form onSubmit={formik.handleSubmit}>
            <div className="flex flex-col gap-4 m-4 ">
              <div className="flex flex-col gap-3">
                <TextField
                  type="text"
                  id="name"
                  name="name"
                  label="name"
                  helperText="Please enter full name"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.name}
                />

                <TextField
                  id="role"
                  select
                  label="Role"
                  name="role"
                  value={formik.values.role}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  helperText="Please select role"
                >
                  {role.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  type={showPassword ? "text" : "password"}
                  id="password"
                  name="password"
                  label="password:"
                  placeholder="password"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  maxRows={4}
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.touched.password && Boolean(formik.errors.password)
                  }
                  helperText={formik.touched.password && formik.errors.password}
                />
              </div>
            </div>
            <div className="flex m-2 gap-4">
              <Button type="submit" variant="contained" color="success">
                Submit
              </Button>
              <Button
                onClick={handleCloseAddUser}
                variant="contained"
                color="error"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <Modal
        open={openAddNurse}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add New Nurse
          </Typography>

          <form>
            <div className="flex flex-col gap-4 m-4">
              <div className="flex flex-row gap-3">
                <TextField
                  id="name"
                  name="name"
                  label="Name:"
                  placeholder="Name"
                  maxRows={4}
                  // value={formik.values.name}
                  // onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                  // error={formik.touched.name && Boolean(formik.errors.name)}
                  // helperText={formik.touched.name && formik.errors.name}
                />

                <TextField
                  id="department"
                  select
                  name="department"
                  label="Department"
                  defaultValue="Chief"
                  helperText="Please select Department"
                  // value={formik.values.department}
                  // onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                >
                  {departments.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </div>
              <div className="flex flex-row justify-between">
                <TextField
                  id="position"
                  select
                  label="Position"
                  defaultValue="Chief"
                  helperText="Please select Position"
                  // value={formik.values.positions}
                  // onChange={formik.handleChange}
                  // onBlur={formik.handleBlur}
                >
                  {positions.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                <FormGroup row className="border border-gray-400  mb-6 rounded">
                  <label className="text-gray-600 text-sm ml-2 ">ward</label>
                  {options.map((option) => (
                    <FormControlLabel
                      key={option.value}
                      control={
                        <Checkbox
                          name="wards"
                          value={option.value}
                          // checked={formik.values.wards.includes(option.value)}
                          // onChange={formik.handleChange}
                        />
                      }
                      label={option.label}
                    />
                  ))}
                </FormGroup>
              </div>

              <div className="border border-gray-600 rounded-md">
                <ReactSignatureCanvas
                  penColor="blue"
                  canvasProps={{
                    className: "w-full h-auto",
                  }}
                />
              </div>
            </div>

            <div>
              <Button variant="outlined" color="error">
                Clear Signature
              </Button>
            </div>

            <div className="flex m-2 gap-4">
              <Button variant="contained" color="success" type="submit">
                Submit
              </Button>
              <Button
                onClick={handleCloseAddNurse}
                variant="contained"
                color="error"
              >
                Close
              </Button>
            </div>
          </form>
        </Box>
      </Modal>
      <div className="flex flex-col shadow-xl" style={header}>
        <div className="flex flex-col gap-6 justify-between">
          <span className="flex flex-row gap-2 cursor-pointer">
            <SurroundSoundIcon sx={{ fontSize: "32px", color: "white" }} />
            <Typography
              id="modal-modal-title"
              variant="h6"
              component="h2"
              sx={{ fontWeight: "600" }}
            >
              Lyteware
            </Typography>
          </span>
          <div className="flex flex-row text-center gap-2 mt-2">
            <LightTooltip title="Back Home" placement="bottom">
              <ArrowBackIosNewIcon
                className="cursor-pointer mt-2 text-lg"
                onClick={handleNavigate1}
                sx={{ color: "#000" }}
              />
            </LightTooltip>

            {/* <NavLink to="/settings"> */}
            <div className="cursor-pointer bg-blue-400 hover:bg-sky-200 shadow-xl rounded-md text-white">
              <span
                className="flex flex-row justify-center gap-2 p-2 text-white"
                style={text1}
              >
                <AiOutlineProject
                  size={20}
                  className="text-white"
                  style={hText}
                />
                <p className="text-white" style={hText}>
                  Management
                </p>
              </span>
            </div>
            {/* </NavLink> */}
          </div>

          <LightTooltip title="change user settings..." placement="bottom">
            <div
              className="flex flex-row gap-4 cursor-pointer"
              style={text}
              onClick={handeleNavigateSettings}
            >
              <span className="">
                <MdManageAccounts size={26} className="text-gary-100" />
              </span>

              <button class="bg-violet-500 hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-300 ...">
                <p className=""> User settings</p>
              </button>
            </div>
          </LightTooltip>

          <LightTooltip
            title="change department settings..."
            placement="bottom"
          >
            <div
              className="flex flex-row gap-4 cursor-pointer bg-sky-600 hover:bg-blue-400"
              style={text}
            >
              <span className="">
                <AiOutlineProject size={20} />
              </span>
              Department Settings
            </div>
          </LightTooltip>

          {/* <LightTooltip title="Add New Nurse..." placement="bottom">
            <div
              className="flex flex-row gap-4 cursor-pointer bg-sky-600 hover:bg-blue-400 "
              style={text}
              onClick={handleOpenAddNurse}
            >
              <span className="">
                <GroupAddIcon size={20} />
              </span>
              Add Nurse
            </div>
          </LightTooltip> */}
          {/* <LightTooltip title="Add New User..." placement="bottom">
            <div
              className="flex flex-row gap-4 cursor-pointer bg-sky-600 hover:bg-blue-400"
              style={text}
              onClick={handleOpenAddUser}
            >
              <span className="">
                <AiOutlineProject size={20} />
              </span>
              Add User
            </div>
          </LightTooltip> */}

          <LightTooltip
            title="View dashboard,... progress..."
            placement="bottom"
          >
            <div
              className="flex flex-row gap-4 cursor-pointer bg-sky-600 hover:bg-blue-400"
              style={text}
              onClick={handleNavigateDash}
            >
              <span className="">
                <DashboardIcon size={20} />
              </span>
              Dashboard
            </div>
          </LightTooltip>

          {/* <LightTooltip title="change user settings..." placement="bottom">
            <div
              className="flex flex-row gap-4 cursor-pointer bg-sky-600 hover:bg-blue-400"
              style={text}
              onClick={handleOpenField}
            >
              <span className="">
                <MiscellaneousServicesIcon size={20} />
              </span>
              <p className="bg-transparent hover:bg-">Manage fields</p>
            </div>
          </LightTooltip> */}
        </div>
        <div
          className="flex flex-row cursor-pointer gap-4"
          style={low_div}
          // onClick={handleNavigate1}
        >
          <span className="">
            <img src={LOGO} alt="componay_logo" style={company_logo} />
          </span>
          <LightTooltip title={usr_data.usr_data.usr_name} placement="top">
            <div>
              <p style={username}>{usr_data.usr_data.usr_name}</p>
              <p className="text-sm font-light text-gray-100" style={admin}>
                {usr_data.usr_data.usr_role}
              </p>
            </div>
          </LightTooltip>
        </div>
      </div>
      <main>
        <Outlet />
      </main>
    </>
  );
};
const header = {
  // borderTopRightRadius: "10px",
  padding: "20px",
  marginTop: "0px",
  // background: "#0F172A",
  // background: "#2196f3",
  background: "rgb(229 229 229)",
  width: "250px",
  height: "100%",
  gap: "470px",
  
};

const text1 = {
  color: "#333",
  fontSize: "16px",
  // background: "#fff",
  padding: "6px",
  borderRadius: "4px",
};
const text = {
  position: "relative",
  fontSize: "14px",
  padding: "5px",
  borderRadius: "6px",
  color: "black",
  transition: "background 0.3s",
  "&:hover": {
    background: "#2B394F",
    // Show text on hover
    "& .hover-text": {
      opacity: 1,
    },
  },
};
const company_logo = {
  width: "40px",
  height: "40px",
  borderRadius: 20,
  // padingBottom: "30px",
  // boxShadow: "0px 0px 10px 0px rgba(0, 0, 0, 0.1)",
};
const username = {
  fontWeight: "light",
  fontSize: "16px",
  overflow: "hidden",
  color: "black",
};
const low_div = {
  padding: "20px",
};
const admin = {
  color: "black",
};
const hText = {
  color: "black", // Text color
};


export default SettingsNav;
